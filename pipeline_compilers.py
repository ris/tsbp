from django.template.loaders.filesystem import Loader
from django.template import Context
from django.conf import settings

from pipeline.compilers import CompilerBase

from itertools import chain

from scss import Scss

from defaults import *

class PyScssCompiler ( CompilerBase ):
	output_extension = 'css'
	
	def match_file ( self , filename ):
		return filename.endswith ( (".scss",".sass",) )

	def compile_file ( self , infile , outfile , outdated = False , force = False ):
		scss_vars = {
			"TSBP_COLOR_SCHEME" : tuple ( chain.from_iterable ( ( ( ( "%s_%s" % ( color_name , variant_name ) , color ) for j , ( variant_name , color ) in enumerate ( color_dict.items () ) ) for i , ( color_name , color_dict ) in enumerate ( TSBP_COLOR_SCHEME.items () ) ) ) ) ,
		}
		scss_vars.update ( ( ( "TSBP_COLOR_SCHEME_%s" % name , color ) for name , color in scss_vars["TSBP_COLOR_SCHEME"] ) )
		scss_vars.update ( ( ( "TSBP_COST_COLOR_%s" % variant , color ) for variant , color in TSBP_COST_COLOR.iteritems () ) )
		scss_vars.update ( ( ( "TSBP_NOCOST_COLOR_%s" % variant , color ) for variant , color in TSBP_NOCOST_COLOR.iteritems () ) )
		
		compiler = Scss ( scss_vars = dict ( ( ( "$%s" % k , v ) for k , v in scss_vars.iteritems () ) ) )
		
		outfile = open ( outfile , mode = "wb" )
		
		outfile.write ( compiler.compile ( scss_file = infile ) )

class _DummyFileTemplateLoader ( Loader ):
	# replace this with a unity function (well, generator)
	def get_template_sources ( self , template_name , template_dirs = None ):
		yield template_name

class SVGTemplateCompiler ( CompilerBase ):
	"""
	Runs a .svgtemplate through django's template compiler, supplied with our tsbp_settings context.
	
	django-pipeline has to be duped into thinking this is "css" by adding targets as entries to PIPELINE_CSS
	"""
	output_extension = 'svg'
	
	def match_file ( self , filename ):
		return filename.endswith ( (".svgtemplate",) )

	def compile_file ( self , infile , outfile , outdated = False , force = False ):
		from context_processors import tsbp_settings
		
		loader_instance = _DummyFileTemplateLoader ()
		template = loader_instance ( infile )[0]
		
		outfile = open ( outfile , mode = "wb" )
		
		outfile.write ( template.render ( Context ( tsbp_settings ( None ) ) ).encode ( settings.FILE_CHARSET ) )
