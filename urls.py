from django.conf.urls.defaults import *
from django.contrib.auth import views as authviews
from django.views.generic.base import RedirectView

urlpatterns = patterns ( "" ,
	(r"^$" , RedirectView.as_view ( url = "solve" , permanent = False ) ),
	url ( r"solve$" , "tsbp.views.initial.initiate_solution" ) ,
	url ( r"(?P<trace_id>[0-9]+)/(?P<trace_activation_index>[0-9]+)/(?P<solution_index>[0-9]+)/$" , "tsbp.views.map.solution_map" ) ,
	url ( r"(?P<trace_id>[0-9]+)/(?P<trace_activation_index>[0-9]+)/(?P<solution_index>[0-9]+)/recalculate$" , "tsbp.views.map.recalculate_solution" ) ,
	url ( r"(?P<trace_id>[0-9]+)/(?P<trace_activation_index>[0-9]+)/(?P<solution_index>[0-9]+)/track/(?P<track_index>[0-9]+)/detail.json$" , "tsbp.views.map.solutiontrack_detail_json" ) ,
	url ( r"(?P<trace_id>[0-9]+)/(?P<trace_activation_index>[0-9]+)/(?P<solution_index>[0-9]+)/track/(?P<track_index>[0-9]+)/trackpoint/(?P<point_index>[0-9]+)/detail.json$" , "tsbp.views.map.solutiontrackpoint_detail_json" ) ,
)
