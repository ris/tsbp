from django.conf import settings

import fcntl
import os
import os.path
from contextlib import contextmanager
import logging

class UniqueInstanceError ( EnvironmentError ): pass

@contextmanager
def assert_lockfile ():
	lockfile_filename = os.path.join ( settings.TSBP_REPLICATION_DIR , "tsbp.lock" )
	lockfile = os.fdopen ( os.open ( lockfile_filename , os.O_RDWR | os.O_CREAT ) , "r+" )

	try:
		fcntl.lockf ( lockfile , fcntl.LOCK_EX | fcntl.LOCK_NB , 0600 )
	except IOError:
		raise UniqueInstanceError ( "It looks a tsbp management procedure is already running (at pid %i?)" % lockfile.read ().strip () )

	lockfile.write ( str ( os.getpid () ) )
	
	try:
		yield
	finally:
		os.unlink ( lockfile_filename )

def setup_logging ():
	logging.basicConfig ( level = logging.INFO , format = "%(asctime)s:%(levelname)s:%(name)s:%(message)s" )
