# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand , CommandError
from django.db import transaction
from django.conf import settings

import os
import os.path
import re
from datetime import datetime
from shutil import copyfile
import signal
import logging
from multiprocessing.dummy import Pool
import subprocess
from time import sleep

#from eventlet.green import subprocess
#from eventlet.greenpool import GreenPool
#from eventlet.greenthread import sleep

from tsbp.defaults import TSBP_OSMOSIS_BINARY , TSBP_OSM2PGSQL_BINARY , TSBP_OSM2PGSQL_STYLE , TSBP_REPLICATION_CURRENT_SEQUENCE_NAMESPACE
from tsbp.models import PlanetRevision
from tsbp.management.commands import assert_lockfile , UniqueInstanceError , setup_logging
from tsbp.tools import open_possibly_zipped_file , cheeky_get_raw_connection
from tsbp.planet import import_planet

from startrouter import operation as startrouter_operation


class ReplicationError ( CommandError ): pass

class ProcessingError ( CommandError ): pass

def _subprocess_command ( command_args , exception_class , cwd = None ):
	command_popen = subprocess.Popen ( command_args , cwd = cwd , close_fds = True )
	command_popen.communicate ()
	
	if command_popen.returncode:
		raise exception_class ( "%s returned code %i. Not continuing." % ( command_args[0] , command_popen.returncode ) )

def _osmosis_replicate ( temp_changes_filename , replication_dir ):
	command_args = [
		TSBP_OSMOSIS_BINARY ,
		"--read-replication-interval" ,
		"workingDirectory=%s" % replication_dir ,
		"--simplify-change" ,
		"--write-xml-change" ,
		temp_changes_filename ,
	]
	
	_subprocess_command ( command_args , ReplicationError )

def _osmosis_patch ( source_planetfile_filename , target_planetfile_filename , temp_changes_filename ):
	command_args = [
		TSBP_OSMOSIS_BINARY ,
		"--read-xml-change" ,
		"file=%s" % temp_changes_filename ,
		"--read-pbf" ,
		"file=%s" % source_planetfile_filename ,
		"--apply-change" ,
	]
	
	if getattr ( settings , "TSBP_PLANET_BOUNDING_BOX" , None ):
		command_args.extend ( (
			"--bounding-box" ,
			"top=%s" % settings.TSBP_PLANET_BOUNDING_BOX[0] ,
			"right=%s" % settings.TSBP_PLANET_BOUNDING_BOX[1] ,
			"bottom=%s" % settings.TSBP_PLANET_BOUNDING_BOX[2] ,
			"left=%s" % settings.TSBP_PLANET_BOUNDING_BOX[3] ,
		) )
	
	if getattr ( settings , "TSBP_PLANET_BOUNDING_POLYFILE" , None ):
		command_args.extend ( (
			"--bounding-polygon" ,
			"file=%s" % settings.TSBP_PLANET_BOUNDING_POLYFILE ,
		) )
	
	command_args.extend ( (
		"--write-pbf" ,
		"file=%s" % target_planetfile_filename ,
		# not actually going to use this as it's useful to be able to use the resulting files to re-seed the postgres tables
		#"omitmetadata=true" ,
	) )
	
	_subprocess_command ( command_args , ReplicationError )

def _osrm_extract ( planetfile_filename , osrm_dir ):
	command_args = [
		os.path.join ( osrm_dir , "osrm-extract" ) ,
		planetfile_filename ,
	]
	
	_subprocess_command ( command_args , ProcessingError , cwd = osrm_dir )

def _osrm_prepare ( osrm_filename , restrictions_filename , osrm_dir ):
	command_args = [
		os.path.join ( osrm_dir , "osrm-prepare" ) ,
		osrm_filename ,
		restrictions_filename ,
	]
	
	_subprocess_command ( command_args , ProcessingError , cwd = osrm_dir )

class Command ( BaseCommand ):
	help = "Replicates latest planet, uses it to update pgsql and routing databases"
	args = "fullplanetupdate"
	
	def handle ( self , **options ):
		setup_logging ()
		
		pool = Pool ( 8 )
		
		with assert_lockfile ():
			# ok, now at least we know we're the only process trying to do this
			
			with transaction.commit_on_success ():
				osmosis_state_filename = os.path.join ( settings.TSBP_REPLICATION_DIR , "state.txt" )
				pgsql_state_filename = os.path.join ( settings.TSBP_REPLICATION_DIR , "tsbp_pgsql_state.txt" )
				planetfile_state_filename = os.path.join ( settings.TSBP_REPLICATION_DIR , "tsbp_planetfile_state.txt" )
				temp_changes_filename = os.path.join ( settings.TSBP_REPLICATION_DIR , "changes.osc.gz" )
				
				try:
					with open ( pgsql_state_filename , "rb" ) as f:
						initial_pgsql_state = f.read ()
				except IOError:
					# it will be assumed that state is same as osmosis state
					initial_pgsql_state = None
				
				try:
					with open ( planetfile_state_filename , "rb" ) as f:
						initial_planetfile_state = f.read ()
				except IOError:
					# it will be assumed that state is same as osmosis state
					initial_planetfile_state = None
				
				if initial_pgsql_state != initial_planetfile_state:
					raise ReplicationError ( "State of planetfile (%s) doesn't match state of pgsql (%s). Something is badly wrong." % ( initial_planetfile_state , initial_pgsql_state ) )
				
				if initial_pgsql_state is not None:
					logging.warning ( "Setting osmosis' perceived state (%s) to pgsql's supposed state (%s)" , osmosis_state_filename , pgsql_state_filename )
					copyfile ( pgsql_state_filename , osmosis_state_filename )
				
				logging.info ( "Replicating changes" )
				_osmosis_replicate ( temp_changes_filename , settings.TSBP_REPLICATION_DIR )
				
				with open ( osmosis_state_filename , "rb" ) as f:
					osmosis_state_string = f.read ()
				
				osmosis_state_sequence_number = int ( re.search ( r"^sequenceNumber=([0-9]+?)$" , osmosis_state_string , flags=re.MULTILINE ).group ( 1 ) )
				osmosis_state_timestamp = datetime.strptime ( re.search ( r"^timestamp=(.+?)Z$" , osmosis_state_string , flags=re.MULTILINE ).group ( 1 ) , "%Y-%m-%dT%H\\:%M\\:%S" )
				
				if PlanetRevision.objects.filter ( state_sequence_number = osmosis_state_sequence_number ).filter ( sequence_namespace = TSBP_REPLICATION_CURRENT_SEQUENCE_NAMESPACE ).exists ():
					raise ReplicationError ( "Already have PlanetRevision with this sequence number: not continuing" )
				
				target_path = os.path.join ( settings.TSBP_PLANETFILES_DIR , str ( osmosis_state_sequence_number ) )
				
				try:
					os.mkdir ( target_path )
				except OSError:
					# directory already exists
					# that might (?) be fine
					pass
				
				target_planetfile_filename = os.path.join ( target_path , "planet.osm.pbf" )
				
				# using a nested function so we can snoop on useful variables we already have lying around
				def osrm_build_outer ():
					logging.info ( "Applying changes to planetfile" )
					_osmosis_patch ( os.path.join ( settings.TSBP_PLANETFILES_DIR , "current" , "planet.osm.pbf" ) , target_planetfile_filename , temp_changes_filename )
					
					# using a nested function so we can snoop on useful variables we already have lying around
					def osrm_build_meta_command ( arg ):
						# Pool.map is a bit stupid about arguments
						label , transport_settings = arg
						
						transport_target_planetfile_filename = os.path.join ( target_path , label + ".osm.pbf" )
						try:
							# in case symlink already exists
							os.unlink ( transport_target_planetfile_filename )
						except OSError:
							pass
						os.symlink ( target_planetfile_filename , transport_target_planetfile_filename )
						
						logging.info ( """Running osrm-extract for transport "%s" """ , label )
						_osrm_extract ( transport_target_planetfile_filename , transport_settings["OSRM_DIR"] )
						
						logging.info ( """Running osrm-prepare for transport "%s" """ , label )
						_osrm_prepare ( os.path.join ( target_path , label + ".osrm" ) , os.path.join ( target_path , label + ".osrm.restrictions" ) , transport_settings["OSRM_DIR"] )
					
					pool.map ( osrm_build_meta_command , settings.TSBP_AVAILABLE_TRANSPORTS.items () )
				
				# let this get on with its business
				osrm_build_outer_async_result = pool.apply_async ( osrm_build_outer )
				
				# this must be done from the parent thread as it will use the database connection
				temp_changes_file = open_possibly_zipped_file ( temp_changes_filename )
				connection = cheeky_get_raw_connection ()
				import_planet ( connection , temp_changes_file , is_changefile = True )
				
				# we don't actually care about the return value of osrm_build_outer just that it's finished successfully
				osrm_build_outer_async_result.get ()
				
				logging.info ( "Deactivating existing PlanetRevisions & creating our new PlanetRevision" )
				# stop anything new grabbing a reference to revision we're about to terminate
				PlanetRevision.objects.filter ( active = True ).update ( active = False )
				
				# create new planet revision, but not marked active yet
				new_planet_revision = PlanetRevision ( state_sequence_number = osmosis_state_sequence_number , state_timestamp = osmosis_state_timestamp , active = False , sequence_namespace = TSBP_REPLICATION_CURRENT_SEQUENCE_NAMESPACE )
				
				logging.info ( "Committing" )
			
			# transaction has committed - we need to do the rest of the switchover as quickly as possible
			# first mark pgsql's state to that of osmosis
			copyfile ( osmosis_state_filename , pgsql_state_filename )
			
			logging.info ( "Flipping planetfile symlink" )
			# flip symlink to planetfile & routing files
			current_link_filename = os.path.join ( settings.TSBP_PLANETFILES_DIR , "current" )
			try:
				os.unlink ( current_link_filename )
			except OSError:
				pass
			os.symlink ( target_path , current_link_filename )
			# now mark planetfile's state to that of osmosis
			copyfile ( osmosis_state_filename , planetfile_state_filename )
			# TODO: terminate running solve tasks?
			
			# kill osrm instances
			for label , transport_settings in settings.TSBP_AVAILABLE_TRANSPORTS.items ():
				osrm_lockfile_filename = os.path.join ( settings.TSBP_REPLICATION_DIR , "osrm_%s.lock" % label )
				try:
					with open ( osrm_lockfile_filename , "r" ) as osrm_lockfile:
						osrm_pid = int ( osrm_lockfile.read ().strip () )
						logging.info ( """Sending OSRM for transport "%s" (@%i) SIGTERM""" , label , osrm_pid )
						os.kill ( osrm_pid , signal.SIGTERM )
				except IOError:
					# lockfile doesn't exist - let's assume that means osrm isn't running
					pass
				except OSError , e:
					# process probably doesn't exist - also probably fine
					logging.warning ( """Caught %s - ignoring""" , e )
					pass
			
			# using a nested function so we can snoop on useful variables we already have lying around
			def restart_osrm_instance ( arg ):
				# Pool.map is a bit stupid about arguments
				label , transport_settings = arg
				for _ in xrange ( 10 ):
					try:
						logging.info ( """Attempting to restart OSRM for transport "%s" """ , label )
						startrouter_operation ( label , transport_settings )
						break
					except UniqueInstanceError:
						logging.info ( """OSRM for transport "%s" still running: waiting 5 seconds...""" , label )
						sleep ( 5 )
				else:
					raise EnvironmentError ( """Couldn't seem to restart router for transport "%s" """ % label )
			
			pool.map ( restart_osrm_instance , settings.TSBP_AVAILABLE_TRANSPORTS.items () )
			
			with transaction.commit_on_success ():
				logging.info ( "Marking new PlanetRevision active" )
				# mark new_planet_revision active
				new_planet_revision.active = True
				new_planet_revision.save ()
