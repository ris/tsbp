from django.core.management.base import BaseCommand , CommandError
from django.conf import settings

import logging
from os.path import join
from os import listdir , stat
import re
from stat import S_ISDIR
from shutil import rmtree
from optparse import make_option

from tsbp.management.commands import assert_lockfile , UniqueInstanceError , setup_logging


class Command ( BaseCommand ):
	help = """Cleans up (deletes) "old" numbered directories in TSBP_PLANETFILES_DIR"""
	args = "cleanupplanetfiles"
	
	option_list = BaseCommand.option_list + (
		make_option ( "-n" , "--leave-count" , type = "int" , dest = "leave_count" , default = 4 , help = "Number of directories to leave" ) ,
	)

	def handle ( self , **options ):
		setup_logging ()
		
		digit_re = re.compile ( r"^[0-9]+?$" )
		
		# lock planet processing code to be extra safe
		with assert_lockfile ():
			dir_ints = sorted ( int ( name ) for name in listdir ( settings.TSBP_PLANETFILES_DIR ) if ( digit_re.match ( name ) and S_ISDIR ( stat ( join ( settings.TSBP_PLANETFILES_DIR , name ) ).st_mode ) ) )
			
			for dir_int in dir_ints[:-options["leave_count"]]:
				target = join ( settings.TSBP_PLANETFILES_DIR , str ( dir_int ) )
				logging.info ( "Deleting %s" , target )
				rmtree ( target )
