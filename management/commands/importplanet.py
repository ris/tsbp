# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from django.db import transaction

import logging
from optparse import make_option

from tsbp.tools import open_possibly_zipped_file , cheeky_get_raw_connection , noop_context_manager
from tsbp.planet import import_planet
from tsbp.management.commands import setup_logging

class Command ( BaseCommand ):
	help = "Imports an OSM planet file into the database"
	args = "importplanet [options] filename.osm.bz2"
	
	option_list = BaseCommand.option_list + (
		# this option may be required to work around bugs in postgres/postgis encountered with large transactions
		make_option ( "-s" , "--separate-transactions" , action = "store_true" , dest = "separate_transactions" , default = False , help = "Perform import tasks in separate transactions." ) ,
	)
	
	def handle ( self , filename , **options ):
		setup_logging ()
		
		connection = cheeky_get_raw_connection ()
		planetfile = open_possibly_zipped_file ( filename )
		
		with transaction.commit_on_success () if not options.get ( "separate_transactions" ) else noop_context_manager ():
			import_planet ( connection , planetfile , separate_transactions = options.get ( "separate_transactions" ) )
			if not options.get ( "separate_transactions" ):
				logging.info ( "Committing" )
		
		print "Now you'll probably want to do a fullplanetupdate to get a fully workable snapshot going"
