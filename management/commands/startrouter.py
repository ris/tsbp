# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand , CommandError
from django.db import transaction
from django.conf import settings

import subprocess
import os.path
from sys import stderr

from tsbp.management.commands import assert_lockfile , UniqueInstanceError
from tsbp.defaults import TSBP_DAEMONIZE_BINARY

def operation ( label , transport_settings ):
	osrm_lockfile_filename = os.path.join ( settings.TSBP_REPLICATION_DIR , "osrm_%s.lock" % label )
	
	command_args = [
		TSBP_DAEMONIZE_BINARY ,
		"-p" ,
		osrm_lockfile_filename ,
		"-l" ,
		osrm_lockfile_filename ,
		"-c" ,
		transport_settings["OSRM_DIR"] ,
		os.path.join ( transport_settings["OSRM_DIR"] , "osrm-routed" ) ,
	]
	
	command_popen = subprocess.Popen ( command_args )
	command_popen.communicate ()
	
	if command_popen.returncode:
		if command_popen.returncode == 1:
			raise UniqueInstanceError ( """Router for transport "%s" already seems to be running (at pid %s?)""" % ( label , open ( osrm_lockfile_filename , "rb" ).read ().strip () ) )
		raise EnvironmentError ( """Attempting to start router for transport "%s" returned code %i""" % ( label , command_popen.returncode ) )

class Command ( BaseCommand ):
	help = "Attepts to start a (set of) daemonized OSRM router(s)"
	args = "startrouter"

	def handle ( self , **options ):
		with assert_lockfile ():
			for label , transport_settings in settings.TSBP_AVAILABLE_TRANSPORTS.items ():
				try:
					operation ( label , transport_settings )
				except UniqueInstanceError , e:
					print >> stderr , e
