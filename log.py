# -*- coding: utf-8 -*-
import logging

class StackScrapingLogFilter ( logging.Filter ):
	def __init__ ( self , sentinel_local , gpx_localname , **kwargs ):
		"""
			@sentinel_local is a tuple of ( function , local name , value )
		"""
		from track import Track , TrackSegment , RoutableTrack , TransitionList , Transition , TransitionBucket
		from trackpoint import TrackPoint , PointOnWayList , PointOnWay
		
		self._sentinel_local = sentinel_local
		self._gpx_localname = gpx_localname
		
		self._interesting_types_seq = (
			Track ,
			TrackSegment ,
			RoutableTrack ,
			TransitionList ,
			Transition ,
			TransitionBucket ,
			TrackPoint ,
			PointOnWayList ,
			PointOnWay ,
		)
		self._interesting_types_template = dict ( ( (t,None) for t in self._interesting_types_seq ) )
	
	def filter ( self , record ):
		try:
			from track import Track , TrackSegment , RoutableTrack , TransitionList , Transition , TransitionBucket
			from trackpoint import TrackPoint , PointOnWayList , PointOnWay
			from sys import exc_info
			# learned this trick from the logging sources
			try:
				raise Exception
			except:
				t , v , tb = exc_info ()
			
			frame = tb.tb_frame
			interesting_types = self._interesting_types_template.copy ()
			
			while frame is not None:
				if frame.f_code is self._sentinel_local[0] and frame.f_locals.get ( self._sentinel_local[1] , None ) is self._sentinel_local[2]:
					# success - break from loop to begin reconstruction
					break
				
				f_self = frame.f_locals.get ( "self" )
				if f_self is not None:
					f_self_type = type ( f_self )
					if f_self_type in self._interesting_types_seq:
						if interesting_types[f_self_type]:
							interesting_types[f_self_type] = interesting_types[f_self_type][0] , f_self
						else:
							interesting_types[f_self_type] = f_self , f_self
				frame = frame.f_back
			else:
				# record was logged from outside desired function call - discard
				return False
			
			gpx = frame.f_locals.get ( self._gpx_localname , None )
			if gpx is not None:
				if interesting_types[Track]:
					try:
						i = gpx.index ( interesting_types[Track][1] )
					except ValueError:
						pass
					else:
						record.track_index = i
						record.track = gpx[i]
				elif interesting_types[RoutableTrack]:
					for i , track in enumerate ( gpx ):
						if getattr ( track , "routable_track" , None ) is interesting_types[RoutableTrack][1]:
							record.track_index = i
							record.track = track
							break
					else:
						return True
				elif interesting_types[TransitionList]:
					for i , track in enumerate ( gpx ):
						if hasattr ( track , "routable_track" ) and getattr ( track.routable_track , "transitions" , None ) is interesting_types[TransitionList][1]:
							record.track_index = i
							record.track = track
							break
					else:
						return True
				elif interesting_types[TrackSegment]:
					for i , track in enumerate ( gpx ):
						try:
							j = track.index ( interesting_types[TrackSegment][1] )
						except ValueError:
							pass
						else:
							record.track_index , record.tracksegment_index = i , j
							record.track , record.tracksegment = track , track[j]
							break
					else:
						return True
				# undesirable last resorts
				elif interesting_types[TrackPoint]:
					for i , track in enumerate ( gpx ):
						prev_trackpoints = 0
						for j , tracksegment in enumerate ( track ):
							try:
								k = tracksegment.index ( interesting_types[TrackPoint][1] )
							except ValueError:
								pass
							else:
								record.track_index , record.tracksegment_index , record.trackpoint_index = i , j , k + prev_trackpoints
								record.track , record.tracksegment , record.trackpoint = track , tracksegment , tracksegment[k]
								break
							prev_trackpoints += len ( tracksegment )
					else:
						return True
				elif interesting_types[Transition]:
					for i , track in enumerate ( gpx ):
						if not ( hasattr ( track , "routable_track" ) and hasattr ( track.routable_track , "transitions" ) ):
							continue
						
						try:
							j = track.routable_track.transitions.index ( interesting_types[Transition][1] )
						except ValueError:
							pass
						else:
							record.track_index , record.transition_index = i , j
							record.track , record.transition = track , track.routable_track.transitions[j]
							break
					else:
						return True
				else:
					return True
				
				# if we get this far we can assume we've got a track index
				
				if interesting_types[TrackSegment] and not hasattr ( record , "tracksegment_index" ):
					try:
						i = gpx[record.track_index].index ( interesting_types[TrackSegment][1] )
					except ValueError:
						pass
					else:
						record.tracksegment_index = i
						record.tracksegment = gpx[record.track_index][i]
				
				if interesting_types[Transition] and not hasattr ( record , "transition_index" ):
					track = gpx[record.track_index]
					if hasattr ( track , "routable_track" ) and hasattr ( track.routable_track , "transitions" ):
						try:
							i = track.routable_track.transitions.index ( interesting_types[Transition][1] )
						except ValueError:
							pass
						else:
							record.transition_index = i
							record.transition = track.routable_track.transitions[i]
				
				if interesting_types[RoutableTrack] and interesting_types[TrackPoint] and not hasattr ( record , "trackpoint_index" ):
					track = gpx[record.track_index]
					if hasattr ( track , "routable_track" ):
						try:
							i = track.routable_track.transitions.index ( interesting_types[TrackPoint][1] )
						except ValueError:
							prev_trackpoints = 0
							for i , tracksegment in enumerate ( track ):
								j = tracksegment.index ( interesting_types[TrackPoint][1] )
								if j != -1:
									record.trackpoint_index = j + prev_trackpoints
									break
								prev_trackpoints += len ( tracksegment )
						else:
							record.trackpoint_routable_index = i
							record.trackpoint = track.routable_track[i]
		except Exception , e:
			# i know this is bad form but i really don't want my mad logging code to cause a failure
			record.filtering_exception = e
			pass
		
		return True

class AnnotationShowingLogFormatter ( logging.Formatter ):
	def format ( self , record ):
		# logging uses old style classes? boo.
		result = logging.Formatter.format ( self , record )
		extra = []
		if hasattr ( record , "track_index" ):
			extra.append ( "trk=%s" % record.track_index )
		if hasattr ( record , "tracksegment_index" ):
			extra.append ( "trkseg=%s" % record.tracksegment_index )
		if hasattr ( record , "trackpoint_index" ):
			extra.append ( "trkpt=%s:%s" % ( record.trackpoint_index , record.trackpoint ) )
		if hasattr ( record , "transition_index" ):
			extra.append ( "trns=%s:%s" % ( record.transition_index , record.transition ) )
		
		if extra:
			result += "\n^%s" % ",".join ( extra )
		
		return result
