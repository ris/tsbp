from django.conf import settings

import simplejson as json

import defaults

_TSBP_COLORS_DICT = dict ( ( ( setting_name , getattr ( defaults , setting_name ) ) for setting_name in ("TSBP_COLOR_SCHEME","TSBP_COST_COLOR_NAME","TSBP_COST_COLOR","TSBP_NOCOST_COLOR_NAME","TSBP_NOCOST_COLOR","TSBP_TRACK_COLORS",) ) )

TSBP_COLORS_JSON = json.dumps ( _TSBP_COLORS_DICT )

def tsbp_settings ( request ):
	d = _TSBP_COLORS_DICT.copy ()
	d.update ( {
		"TSBP_COLORS" : _TSBP_COLORS_DICT ,
		"TSBP_COLORS_JSON" : TSBP_COLORS_JSON ,
		"TSBP_INTRO_BANNER_TEXT" : getattr ( settings , "TSBP_INTRO_BANNER_TEXT" , "" ) , # could use a safestring for html content
	} )
	return d
