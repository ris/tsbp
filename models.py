# -*- coding: utf-8 -*-
from django.contrib.gis.db import models
from django.core import exceptions
from django.conf import settings

import datetime
from math import sqrt
from simplejson import loads , dumps

from tools import get_mercurial_working_version_id
# this is here so it always gets executed early
initial_mercurial_working_version_id = get_mercurial_working_version_id ( __file__ )


class OSMNode ( models.Model ):
	objects = models.GeoManager ()
	
	osm_id = models.BigIntegerField ( primary_key = True )
	
	point_4326 = models.PointField ( srid = 4326 , spatial_index = False )
	point_900913 = models.PointField ( srid = 900913 , spatial_index = False , null = True )
	version = models.PositiveIntegerField ()

class OSMWay ( models.Model ):
	objects = models.GeoManager ()
	
	osm_id = models.BigIntegerField ( primary_key = True )
	
	name = models.TextField ( blank = True )
	ref = models.TextField ( blank = True )
	
	# this is here only to allow us to detect backwards oneways as it's not yet
	# easy to deduce that from OSRM. still won't cope with odd situations like
	# backwards bicycle-only oneways, but previously we didn't worry about complex
	# oneways at all, so this is at least one better.
	oneway = models.TextField ( blank = True )
	
	version = models.PositiveIntegerField ()
	
	way = models.LineStringField ( srid = 900913 , null = True )

class OSMMembership ( models.Model ):
	"""
	We have to use a special model and integer fields rather than a straight manytomany field because we have to be able to cope with lossy situations. ie - ways with nodes we do not have an entry for because it was chopped out of the bounding box.
	
	Doing it this way allows us to skip the REFERENCES constraint.
	"""
	osmway_osm_id = models.BigIntegerField ( db_index = True )
	osmnode_osm_id = models.BigIntegerField ( db_index = True )

#
# model representing planet revisions
#

class PlanetRevision ( models.Model ):
	class Meta:
		unique_together = ( ( "state_sequence_number" , "sequence_namespace" ) ,)
	
	state_sequence_number = models.PositiveIntegerField ()
	state_timestamp = models.DateTimeField ()
	
	sequence_namespace = models.TextField ( blank = True )
	
	inserted_timestamp = models.DateTimeField ( default = datetime.datetime.now )
	
	active = models.BooleanField ()

#
# models representing osm traces
#

class Trace ( models.Model ):
	"""
		Represents the immutable properties of a trace
	"""
	data = models.FileField ( upload_to = "private/tracedata/%Y%m/" , blank = True )
	content_type = models.TextField ( blank = True )
	
	# using null value to act as both a state indicator and a timestamp: null -> unfetched
	fetched = models.DateTimeField ( null = True )
	
	last_trace_activation_index = models.PositiveIntegerField ( null = False , default = 0 )
	
	def clean ( self ):
		if self.data and not self.fetched:
			raise ValidationError ( "Can't have data but no fetched timestamp" )
		
		if self.fetched:
			if not self.data:
				raise ValidationError ( "fetched instances must have data" )
			
			if not self.content_type:
				raise ValidationError ( "fetched instances must have content_type" )

class TraceActivation ( models.Model ):
	"""
		Represents the state of a trace at the time a solution was initially requested
	"""
	class Meta:
		# this unique constraint also means we don't need a separate index in index_by_trace (?)
		# because we'll almost always be looking up by both trace and index_by_trace together
		unique_together = ( ( "trace" , "index_by_trace" ) ,)
	
	VISIBILITY_CHOICES_FORFIELD = (
		("private",)*2 ,
		("public",)*2 ,
		("trackable",)*2 ,
		("identifiable",)*2 ,
	)
	
	TRANSPORT_TYPE_CHOICES_FORFIELD = tuple ( ( (k,)*2 for k in settings.TSBP_AVAILABLE_TRANSPORTS.iterkeys () ) )
	
	trace = models.ForeignKey ( Trace , null = False )
	index_by_trace = models.PositiveIntegerField ( null = False , blank = True )
	
	# using null value to act as both a state indicator and a timestamp: null -> unfetched
	fetched = models.DateTimeField ( null = True )
	
	name = models.TextField ( blank = True )
	description = models.TextField ( blank = True )
	visibility = models.TextField ( choices = VISIBILITY_CHOICES_FORFIELD , blank = True )
	
	raw_details_xml = models.TextField ( blank = True )
	
	last_solution_index = models.PositiveIntegerField ( null = False , default = 0 )
	
	transport_type = models.CharField ( max_length = 64 , choices = TRANSPORT_TYPE_CHOICES_FORFIELD , blank = False )
	
	def clean ( self ):
		if self.fetched:
			if not self.visibility:
				raise ValidationError ( "fetched instances must have visibility set" )
			
			if not self.raw_details_xml:
				raise ValidationError ( "fetched instances must have raw_details_xml" )

#
# models for tsbp solutions
#

def _validate_json ( value ):
	if value:
		try:
			loads ( value )
		except ValueError:
			raise ValidationError ( "This doesn't seem to be valid json" )

class Solution ( models.Model ):
	class Meta:
		# this unique constraint also means we don't need a separate index in index_by_trace_activation (?)
		# because we'll almost always be looking up by both trace_activation and index_by_trace_activation together
		unique_together = ( ( "trace_activation" , "index_by_trace_activation" ) ,)
	
	STATE_CHOICES_FORFIELD = (
		( 0 , "initial" ) ,
		( 1 , "queued" ) ,
		( 2 , "processing" ) ,
		( 3 , "error" ) ,
		( 4 , "success" ) ,
		( 5 , "retry" ) ,
	)
	STATE_CHOICES_INVERSE = dict ( ( (b,a) for a , b in STATE_CHOICES_FORFIELD ) )
	
	trace_activation = models.ForeignKey ( TraceActivation , null = False )
	index_by_trace_activation = models.PositiveIntegerField ( null = False , blank = True )
	parent_solution = models.ForeignKey ( "self" , null = True , blank = True )
	planet_revision = models.ForeignKey ( PlanetRevision , null = True , blank = True )
	pickled_gpx = models.FileField ( upload_to = "private/pickled_gpx/%Y%m/" , blank = True )
	log_file = models.FileField ( upload_to = "private/solution_log_file/%Y%m/" , blank = True )
	intents_json = models.TextField ( blank = True , validators = [ _validate_json ] )
	
	metadata_json = models.TextField ( blank = True )
	
	# max_length 41 for including a possible "+" at the end denoting a modified repository
	created_by_mercurial_working_version_id = models.CharField ( blank = False , max_length = 41 , default = initial_mercurial_working_version_id )
	
	@property
	def metadata ( self ):
		if not hasattr ( self , "_metadata" ):
			self._metadata = loads ( self.metadata_json ) if self.metadata_json else {}
		return self._metadata
	
	state = models.PositiveSmallIntegerField ( choices = STATE_CHOICES_FORFIELD , default = 0 )
	reason = models.TextField ( blank = True )
	
	def clean ( self ):
		if self.state == 4 and not self.pickled_gpx:
			raise exceptions.ValidationError ( "pickled_gpx required in this state" )
		
		if self.state == (2,3,4,5,) and not self.log_file:
			raise exceptions.ValidationError ( "log_file required in this state" )
		
		if self.state == (2,3,4,5,) and not self.planet_revision:
			raise exceptions.ValidationError ( "planet_revision required in this state" )
		
		if self.parent_solution and not self.planet_revision:
			raise exceptions.ValidationError ( "planet_revision required for solutions with parent_solution" )
		
		if self.parent_solution and self.parent_solution.planet_revision != self.planet_revision:
			raise exceptions.ValidationError ( "planet_revision required for solutions with parent_solution" )
		
		if self.parent_solution and self.parent_solution.trace_activation != self.trace_activation:
			raise exceptions.ValidationError ( "parent_solution's trace_activation doesn't match ours" )
		
		if self.parent_solution and not self.intents_json:
			raise exceptions.ValidationError ( "intents_json required for solutions with parent_solution" )
	
	@property
	def intents ( self ):
		if not hasattr ( self , "_intents" ):
			self._intents = loads ( self.intents_json ) if self.intents_json else None
		return self._intents
	
	@models.permalink
	def get_absolute_url ( self ):
		from tsbp.views.map import solution_map
		return ( "tsbp.views.map.solution_map" , () , { "trace_id" : self.trace_activation.trace.id , "trace_activation_index" : self.trace_activation.index_by_trace , "solution_index" : self.index_by_trace_activation } )
	
	@models.permalink
	def get_recalculate_url ( self ):
		from tsbp.views.map import recalculate_solution
		return ( "tsbp.views.map.recalculate_solution" , () , { "trace_id" : self.trace_activation.trace.id , "trace_activation_index" : self.trace_activation.index_by_trace , "solution_index" : self.index_by_trace_activation } )

class SolutionTrack ( models.Model ):
	OUTCOME_CHOICES_FORFIELD = (
		( 0 , "unsolvable" ) ,
		( 1 , "exhausted" ) ,
		( 2 , "success" ) ,
	)
	OUTCOME_CHOICES_INVERSE = dict ( ( (b,a) for a , b in OUTCOME_CHOICES_FORFIELD ) )
	
	class Meta:
		unique_together = ( ( "solution" , "track_index" ) ,)
		ordering = ( "solution" , "track_index" ,)
	
	solution = models.ForeignKey ( Solution )
	track_index = models.PositiveIntegerField ( db_index = True )
	outcome = models.PositiveSmallIntegerField ( choices = OUTCOME_CHOICES_FORFIELD , default = OUTCOME_CHOICES_INVERSE["success"] )
	
	simplified_linestring = models.LineStringField ( srid = 4326 , null = True , blank = True )
	all_points_bbox_extents = models.MultiPointField ( srid = 4326 , null = True , blank = True )
	cost_overview_cost_json = models.TextField ( blank = True )
	cost_overview_nulls_json = models.TextField ( blank = True )
	annotated_pinnings_json = models.TextField ( blank = True )
	
	costed_linestring_geojson = models.TextField ( blank = True )
	
	metadata_json = models.TextField ( blank = True )
	
	@property
	def metadata ( self ):
		if not hasattr ( self , "_metadata" ):
			self._metadata = loads ( self.metadata_json ) if self.metadata_json else {}
		return self._metadata
	
	objects = models.GeoManager()

class SolutionTrackPoint ( models.Model ):
	class Meta:
		unique_together = ( ( "solution_track" , "point_index" ) ,)
		ordering = ( "solution_track" , "point_index" ,)
	
	solution_track = models.ForeignKey ( SolutionTrack )
	point_index = models.PositiveIntegerField ( db_index = True )
	# which trkseg this point was part of, not that we really care much about trksegs
	seg_index = models.PositiveIntegerField ()
	routable = models.BooleanField ( db_index = True )
	gpx_point = models.PointField ( srid = 4326 )
	# this is the fudged timestamp accounting for shared timestamps
	timestamp = models.DateTimeField ( null = True )
	# this is the original timestamp as supplied in the gpx
	original_timestamp = models.DateTimeField ( null = True )
	
	cost = models.FloatField ( null = True , blank = True )
	
	@property
	def visual_cost ( self ):
		return sqrt ( self.cost ) if self.cost is not None else None
	
	metadata_json = models.TextField ( blank = True )
	
	@property
	def metadata ( self ):
		if not hasattr ( self , "_metadata" ):
			self._metadata = loads ( self.metadata_json ) if self.metadata_json else {}
		return self._metadata
	
	current_candidate_index = models.PositiveIntegerField ( null = True , blank = True )
	calculated_candidates_json = models.TextField ( blank = True )
	
	@property
	def calculated_candidates ( self ):
		if not hasattr ( self , "_calculated_candidates" ):
			self._calculated_candidates = loads ( self.calculated_candidates_json ) if self.calculated_candidates_json else {}
		return self._calculated_candidates
	
	def __init__ ( self , *args , **kwargs ):
		trackpoint = kwargs.pop ( "trackpoint" , None )
		if trackpoint:
			if not kwargs.get ( "gpx_point" ):
				kwargs["gpx_point"] = trackpoint.point.transform ( 4326 , clone = True )
			if not kwargs.get ( "timestamp" ):
				kwargs["timestamp"] = trackpoint.timestamp
			if not kwargs.get ( "original_timestamp" ):
				kwargs["original_timestamp"] = trackpoint._raw_timestamp
			if not kwargs.get ( "metadata_json" ):
				kwargs["metadata_json"] = dumps ( {
					"velocity_vector" : trackpoint.velocity_vector ,
					"original_hdop" : getattr ( trackpoint , "_hdop" , None ) ,
				} )
			if not kwargs.get ( "calculated_candidates_json" ):
				kwargs["calculated_candidates_json"] = dumps ( list ( trackpoint.get_calculated_candidates_summary () ) )
		super ( SolutionTrackPoint , self ).__init__ ( *args , **kwargs )
	
	objects = models.GeoManager()
