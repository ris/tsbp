# -*- coding: utf-8 -*-
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import HttpResponse , HttpResponseBadRequest , HttpResponseNotFound , HttpResponseForbidden , HttpResponseRedirect , QueryDict , Http404
from django.views.decorators.http import require_http_methods
from django.db import transaction
from django.core.validators import MaxValueValidator
from django import forms

from tsbp.models import Solution , Trace , TraceActivation
from tsbp.tasks import solve_gpx


class _InitiateSolutionForm ( forms.Form ):
	osm_trace_id = forms.IntegerField ( label = u"OSM Trace id" , validators = [ MaxValueValidator ( (1<<32)-1 ) ] )
	transport_type = forms.ChoiceField ( choices = TraceActivation.TRANSPORT_TYPE_CHOICES_FORFIELD , initial = "motorcar" )


@require_http_methods (("GET","POST"))
def initiate_solution ( request ):
	if request.method == "POST":
		form = _InitiateSolutionForm ( request.POST )
		
		if form.is_valid ():
			with transaction.commit_on_success ():
				trace , created = Trace.objects.get_or_create ( id = form.cleaned_data["osm_trace_id"] )
				trace_activation = TraceActivation ( trace = trace , transport_type = form.cleaned_data["transport_type"] )
				trace_activation.save ()
				# re-fetch to get assigned index
				trace_activation = TraceActivation.objects.get ( pk = trace_activation.pk )
				
				solution = Solution ( trace_activation = trace_activation )
				solution.save ()
				# re-fetch to get assigned index
				solution = Solution.objects.get ( pk = solution.pk )
				
				solve_gpx.delay ( solution.id )
				
				return HttpResponseRedirect ( solution.get_absolute_url () )
				
		# else fall thru to reshow form with errors
	else:
		if request.GET:
			form = _InitiateSolutionForm ( initial = request.GET )
		else:
			form = _InitiateSolutionForm ()
	
	context = {
		"form" : form ,
	}
	
	return render_to_response ( "tsbp/initiate_solution_template.xhtml" , RequestContext ( request , context ) )
