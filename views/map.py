# -*- coding: utf-8 -*-
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.template.loader import render_to_string
from django.utils import simplejson as json
from django.http import HttpResponse , HttpResponseBadRequest , HttpResponseNotFound , HttpResponseForbidden , HttpResponseRedirect , QueryDict , Http404
from django.views.decorators.http import require_http_methods
from django.db import transaction
from django.contrib.gis.geos import Point

from time import mktime
from math import hypot , atan2 , degrees

from tsbp.decorators import solution_view_by_trace_chain , solutiontrack_view , solutiontrackpoint_view
from tsbp.models import Solution

@solution_view_by_trace_chain
def solution_map ( request , solution ):
	if solution.state != Solution.STATE_CHOICES_INVERSE["success"]:
		state_line = "Solution is in state %s%s" % ( solution.get_state_display () , (": %s" % solution.reason) if solution.state in (Solution.STATE_CHOICES_INVERSE["error"],) and solution.reason else "" )
		reload_line = "Reload page to check for updated status" if solution.state in (0,1,2,5,) else ""
		
		return HttpResponse ( "%s\n%s" % ( state_line , reload_line ) , mimetype = "text/plain" )
	
	return render_to_response ( "tsbp/solution_map_template.xhtml" , RequestContext ( request , { "solution" : solution } ) )

@solution_view_by_trace_chain
@solutiontrack_view
def solutiontrack_detail_json ( request , solution , solutiontrack ):
	coords_list = []
	costprofile = []
	timestamps = []
	for gpx_point , cost , timestamp in solutiontrack.solutiontrackpoint_set.order_by ( "point_index" ).values_list ( "gpx_point" , "cost" , "timestamp" ):
		coords_list.append ( gpx_point.coords )
		costprofile.append ( cost )
		timestamps.append ( None if timestamp is None else mktime ( timestamp.timetuple () ) )
	
	gpxpoints_dict = {
		"type" : "Feature" ,
		"geometry" : {
			"type" : "MultiPoint" ,
			"coordinates" : coords_list ,
		} ,
	}
	
	return HttpResponse ( json.dumps ( {
		"linestring" : json.loads ( solutiontrack.costed_linestring_geojson ) if solutiontrack.costed_linestring_geojson else None ,
		"pinnings" : json.loads ( solutiontrack.annotated_pinnings_json ) if solutiontrack.annotated_pinnings_json else None ,
		# ^ yes, decoding then re-encoding. not ideal.
		"costprofile" : costprofile ,
		"gpxpoints" : gpxpoints_dict ,
		"timestamps" : timestamps ,
		"outcome" : solutiontrack.get_outcome_display () ,
	} ) , mimetype = "text/javascript" )

@solution_view_by_trace_chain
@solutiontrack_view
@solutiontrackpoint_view
def solutiontrackpoint_detail_json ( request , solution , solutiontrack , solutiontrackpoint ):
	context = {
		"solution" : solution ,
		"solutiontrack" : solutiontrack ,
		"solutiontrackpoint" : solutiontrackpoint ,
		"velocity_magnitude" : hypot ( *solutiontrackpoint.metadata["velocity_vector"] ) ,
		"velocity_angle_deg" : degrees ( atan2 ( *solutiontrackpoint.metadata["velocity_vector"] ) ) ,
	}
	calculated_candidates_copy = [ dict ( ( (k,( Point ( *v , srid = 900913 ).transform ( 4326 , clone = True ).coords ) if k == "c" else v ) ) for k , v in cc_dict.items () ) for cc_dict in solutiontrackpoint.calculated_candidates ]
	
	result = {
		"panel_inner_html" : render_to_string ( "tsbp/solutiontrackpoint_detail_template.xhtml" , RequestContext ( request , context ) ) ,
		"calculated_candidates" : calculated_candidates_copy ,
		"current_candidate_index" : solutiontrackpoint.current_candidate_index ,
	}
	return HttpResponse ( json.dumps ( result ) , mimetype = "text/javascript" )

@require_http_methods(["POST"])
@solution_view_by_trace_chain
def recalculate_solution ( request , solution ):
	from tsbp.tasks import solve_gpx
	import simplejson as json
	if solution.state != Solution.STATE_CHOICES_INVERSE["success"]:
		raise Http404
	
	try:
		intents = json.loads ( request.POST.get ( "intents" , "{}" ) )
	except:
		return HttpResponseBadRequest ( "intents contains undecipherable json" )
	
	# populate intents with any extra parameters we've been given
	for name , value in request.POST.iteritems ():
		if name in ("intents","csrfmiddlewaretoken",):
			continue
		
		tokens = name.split ( "__" )
		
		# create any required dicts
		enclosing_dict = intents
		for token in tokens[:-1]:
			enclosing_dict[token] = enclosing_dict.get ( token , {} )
			enclosing_dict = enclosing_dict[token]
		
		try:
			enclosing_dict[tokens[-1]] = json.loads ( value )
		except:
			return HttpResponseBadRequest ( "%s contains undecipherable json" % name )
	
	with transaction.commit_on_success ():
		new_solution = Solution ( parent_solution = solution , trace_activation = solution.trace_activation , planet_revision = solution.planet_revision , intents_json = json.dumps ( intents ) )
		new_solution.full_clean ()
		new_solution.save ()
	
	with transaction.commit_on_success ():
		# re-fetch to get assigned index
		new_solution = Solution.objects.get ( pk = new_solution.pk )
		new_solution.state = Solution.STATE_CHOICES_INVERSE["queued"]
		new_solution.full_clean ()
		new_solution.save ()
		
		solve_gpx.delay ( new_solution.id )
	
	return HttpResponseRedirect ( new_solution.get_absolute_url () )
