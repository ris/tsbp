--
-- this lot probably won't get inserted cleanly by django, but it belongs in this file so it's staying here.
--

CREATE OR REPLACE FUNCTION trace_activation_auto_index_by_trace () RETURNS TRIGGER AS $$
BEGIN
	IF NEW.index_by_trace IS NULL OR NEW.index_by_trace < 0 THEN
		UPDATE tsbp_trace SET last_trace_activation_index = last_trace_activation_index + 1 WHERE id = NEW.trace_id RETURNING last_trace_activation_index INTO STRICT NEW.index_by_trace;
	END IF;
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trace_activation_auto_index_by_trace_beforeinsert BEFORE INSERT ON tsbp_traceactivation FOR EACH ROW EXECUTE PROCEDURE trace_activation_auto_index_by_trace ();

-- this is needed because django won't repolulate itself with the generated index_by_... field after INSERTing it, and
-- possibly a following UPDATE will try to put back the initial NULL value. so just silently swallow attempts to update it
-- to an invalid value
CREATE OR REPLACE FUNCTION trace_activation_ign_upd_index_by_trace_to_null () RETURNS TRIGGER AS $$
BEGIN
	IF NEW.index_by_trace IS NULL OR NEW.index_by_trace < 0 THEN
		NEW.index_by_trace := OLD.index_by_trace;
	END IF;
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trace_activation_ign_upd_index_by_trace_to_null_beforeupdate BEFORE UPDATE ON tsbp_traceactivation FOR EACH ROW EXECUTE PROCEDURE trace_activation_ign_upd_index_by_trace_to_null ();
