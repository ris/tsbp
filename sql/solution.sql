--
-- this lot probably won't get inserted cleanly by django, but it belongs in this file so it's staying here.
--

CREATE OR REPLACE FUNCTION solution_auto_index_by_trace_activation () RETURNS TRIGGER AS $$
BEGIN
	IF NEW.index_by_trace_activation IS NULL OR NEW.index_by_trace_activation < 0 THEN
		UPDATE tsbp_traceactivation SET last_solution_index = last_solution_index + 1 WHERE id = NEW.trace_activation_id RETURNING last_solution_index INTO STRICT NEW.index_by_trace_activation;
	END IF;
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER solution_auto_index_by_trace_activation_beforeinsert BEFORE INSERT ON tsbp_solution FOR EACH ROW EXECUTE PROCEDURE solution_auto_index_by_trace_activation ();

-- this is needed because django won't repolulate itself with the generated index_by_... field after INSERTing it, and
-- possibly a following UPDATE will try to put back the initial NULL value. so just silently swallow attempts to update it
-- to an invalid value
CREATE OR REPLACE FUNCTION solution_ign_upd_index_by_trace_activation_to_null () RETURNS TRIGGER AS $$
BEGIN
	IF NEW.index_by_trace_activation IS NULL OR NEW.index_by_trace_activation < 0 THEN
		NEW.index_by_trace_activation := OLD.index_by_trace_activation;
	END IF;
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER solution_ign_upd_index_by_trace_activation_to_null_beforeupdate BEFORE UPDATE ON tsbp_solution FOR EACH ROW EXECUTE PROCEDURE solution_ign_upd_index_by_trace_activation_to_null ();
