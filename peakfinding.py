
def find_peaks_cwt_extended ( vector , widths , wavelet=None , max_distances=None , gap_thresh=None , min_length=None , min_snr=1 , noise_perc=10 , window_size = None ):
	"""
	Basically a copy of scipy.signal.find_peaks_cwt exposing more knobs
	"""
	from scipy.signal.wavelets import cwt , ricker
	from scipy.signal._peak_finding import _identify_ridge_lines , _filter_ridge_lines
	import numpy
	
	if gap_thresh is None:
		gap_thresh = numpy.ceil(widths[0])
	if max_distances is None:
		max_distances = widths / 4.0
	if wavelet is None:
		wavelet = ricker
	
	cwt_dat = cwt ( vector , wavelet , widths )
	ridge_lines = _identify_ridge_lines ( cwt_dat , max_distances , gap_thresh )
	filtered = _filter_ridge_lines ( cwt_dat , ridge_lines , window_size = window_size , min_length = min_length , min_snr = min_snr , noise_perc = noise_perc )
	max_locs = map ( lambda x: x[1][0] , filtered )
	return sorted ( max_locs )
