from collections import deque
from itertools import islice , chain
from operator import eq
from contextlib import contextmanager

def iterwindow ( it , window_size , step = 1 , partials = True ):
	"""Sliding window iterator based on http://stackoverflow.com/a/323911"""
	it = iter ( it )  # Ensure we have an iterator
	l = deque ( islice ( it , window_size ) )
	while True:  # Continue till StopIteration gets raised.
		t = tuple ( l )
		if partials or len ( t ) == window_size:
			yield t
		for i in xrange ( step ):
			l.append ( it.next () )
			l.popleft ()

def iterpad ( it , npad ):
	return chain ( [ None ] * npad , it , [ None ] * npad )

def iterrepeateach ( it , count = 2 ):
	"""repeats each value of an iterator count times"""
	for i in it:
		for j in xrange ( count ):
			yield i

def iterdedup ( it ):
	"""
	deduplicates entries in an iterator stream
	"""
	it = iter ( it )
	
	# only have to keep track of one previous entry
	previous = it.next ()
	
	# the extra None here should never get yielded, but should just push through the final
	# previous by not satisfying the equal clause
	for i in chain ( it , (None,) ):
		if i != previous:
			yield previous
		previous = i

def iterdual ( subit , superit , equality_function = eq ):
	"""
	iterates through two iterators together, the sub iterator @subit having a subset of the members of the super iterator @superit (in the same order)
	
	only advances sub iterator when it equals the current value of the super iterator according to equality_function
	"""
	for subitem in subit:
		for superitem in superit:
			if equality_function ( subitem , superitem ):
				yield subitem , superitem
				# break to fetch the next subitem
				break
			else:
				yield None , superitem
	
	# emit trailing superitems
	for superitem in superit:
		yield None , superitem

def iterfractionalsplit ( it , mean_divisor ):
	"""
	splits iterator stream into sub-iterables of mean length @mean_divisor
	"""
	it = iter ( it )
	try:
		q = deque ( (it.next (),) )
	except StopIteration:
		return
	last_j = 0
	for i , item in enumerate ( it , 1 ):
		j = int ( i * mean_divisor )
		if j != last_j:
			yield q
			# replace q with new one
			q = deque ( (item,) )
			last_j = j
		else:
			q.append ( item )
	yield q

def iterinlineexceptions ( it ):
	"""
	yields exceptions raised from iterating @it inline into the stream
	
	intended to help with handling exceptions in eventlet greenthreads
	"""
	try:
		it = iter ( it )
		
		for i in it:
			yield i
	except Exception , e:
		yield e

def get_mercurial_working_version_id ( path ):
	"""
	Goes up the path's dir structure until it finds a usable repository root.
	
	Returns the hex id of the working changeset. ( Or rather its direct parent )
	"""
	from mercurial import hg , ui , error
	import os.path
	
	path = os.path.realpath ( path )
	u = ui.ui ()
	
	while True:
		try:
			repo = hg.repository ( u , path )
			break
		except ( error.Abort , error.RepoError ) , e:
			path = os.path.dirname ( path )
			if path == "/":
				raise e
			else:
				continue
	
	revision_hash = repo[None].p1().hex ()
	modified , added , removed , deleted , unknown , ignored , clean = repo.status ()
	if modified or added or removed or deleted:
		revision_hash += "+"
	
	return revision_hash

timedelta_total_seconds = lambda td : (td.microseconds + (td.seconds + td.days * 24 * 3600) * 10**6) / 10.**6

def coroutine ( func ):
	def start ( *args , **kwargs ):
		cr = func ( *args , **kwargs )
		cr.next ()
		return cr
	return start

def open_possibly_zipped_file ( filename ):
	if filename.endswith ( ".bz2" ):
		from bz2 import BZ2File
		return BZ2File ( filename , mode = "rb" , buffering = (1<<16) )
	elif filename.endswith ( ".gz" ):
		from gzip import GzipFile
		return GzipFile ( filename , mode = "rb" )
	else:
		return open ( filename , mode = "r" )

def cheeky_get_raw_connection ():
	"""
	Does something very cheeky and quite naughty. Uses some tricks to get the raw (hopefully psycopg2) connection object so we can use it with
	all its features and none of the overhead of django's connection, whilst still automatically getting all django's db settings.
	Yet another thing that destroys our db independence and will make us vulnerable to django internal changes.
	
	From oslmusicalchairs.
	"""
	from django.db import connection
	
	return connection.cursor().cursor.connection

@contextmanager
def noop_context_manager ():
	yield
