"""
	Planet processing routines mostly ported from oslmusicalchairs
"""

from django.conf import settings
from django.db import transaction
from django.contrib.gis.geos import Point

import logging
from itertools import chain

from eventlet.greenthread import sleep as eventlet_sleep

from tools import coroutine , noop_context_manager
from geometry import parse_poly_file

_event_start = 0
_event_end = 1


_command_delete_node = 0
_command_modify_node = 1
_command_create_node = 2

_command_delete_membership_by_way = 3
_command_create_membership = 4

_command_delete_way = 5
_command_create_way = 6

_command_mark_node_modified = 7
_command_mark_way_modified = 8

_command_finish = 9

@coroutine
def planetfile_coroutine ( queue ):
	"""
	inspired by david beazley's "curious course on coroutines and concurrency"
	"""
	
	try:
		bbox_floats = (
			float ( settings.TSBP_PLANET_BOUNDING_BOX[0] ) ,
			float ( settings.TSBP_PLANET_BOUNDING_BOX[1] ) ,
			float ( settings.TSBP_PLANET_BOUNDING_BOX[2] ) ,
			float ( settings.TSBP_PLANET_BOUNDING_BOX[3] ) ,
		)
	except AttributeError:
		bbox_floats = None
	
	bounding_poly = getattr ( settings , "TSBP_PLANET_BOUNDING_POLYFILE" , None ) and parse_poly_file ( open ( settings.TSBP_PLANET_BOUNDING_POLYFILE ) )
	if bounding_poly:
		bounding_poly_orig = bounding_poly # need to keep a reference to the original poly around to workaround a django/GEOS bug
		bounding_poly = bounding_poly.prepared
	
	while True:
		node_buffer = []
		event , value = (yield)
		
		if event == _event_start and value[0] in ( "create" , "modify" , "delete" , "osm" ):
			# an actioncontext of "osm" is an initial planet import
			actioncontext = value[0]
			
			while True:
				event , value = (yield)
				
				if event == _event_start and value[0] == "node":
					floatlon = float ( value[1]["lon"] )
					floatlat = float ( value[1]["lat"] )
					
					# check this node interests us by being in the TSBP_PLANET_BOUNDING_BOX. if not, dont waste time on it.
					# note we don't properly handle nodes moving _out_ of the bbox as it means we would have to check every node for
					# whether it had previously been inside the bbox. should only cause weirdness near the border.
					if ( bbox_floats is None or (
						floatlat <= bbox_floats[0] and
						floatlon <= bbox_floats[1] and
						floatlat >= bbox_floats[2] and
						floatlon >= bbox_floats[3] ) ) and ( bounding_poly is None or bounding_poly.contains ( Point ( floatlon , floatlat ) ) ):
						if actioncontext == "delete":
							queue.put ( ( _command_delete_node , (value[1]["id"],) ) )
							#logging.warning ( "put to queue, size %s" , queue.qsize () )
						else:
							queue.put ( ( _command_modify_node if actioncontext == "modify" else _command_create_node , ( value[1]["lon"] , value[1]["lat"] , value[1]["version"] , value[1]["id"] ) ) )
							#logging.warning ( "put to queue, size %s" , queue.qsize () )
							if actioncontext == "modify":
								queue.put ( ( _command_mark_node_modified , (value[1]["id"],) ) )
								#logging.warning ( "put to queue, size %s" , queue.qsize () )
					
					# skip through uninteresting node inner elements
					while True:
						event , value = (yield)
						if event == _event_end and value == "node":
							break
				elif event == _event_start and value[0] == "way":
					way_id = value[1]["id"]
					way_version = value[1]["version"]
					way_name = ""
					way_ref = ""
					way_oneway = ""
					is_highway = False
					member_buffer = []
					
					if actioncontext in ( "modify" , "delete" ):
						# for modify actions, what we'll do is attempt to delete it and any references to it and if it now interests us, reinsert it.
						queue.put ( ( _command_delete_membership_by_way , (way_id,) ) )
						#logging.warning ( "put to queue, size %s" , queue.qsize () )
						queue.put ( ( _command_delete_way , (way_id,) ) )
						#logging.warning ( "put to queue, size %s" , queue.qsize () )
					
					# run through way inner elements
					while True:
						event , value = (yield)
						if event == _event_start and value[0] == "nd":
							queue.put ( ( _command_create_membership , ( value[1]["ref"] , way_id ) ) )
							#logging.warning ( "put to queue, size %s" , queue.qsize () )
						elif event == _event_start and value[0] == "tag":
							if value[1]["k"] == "name":
								way_name = value[1]["v"]
							elif value[1]["k"] == "ref":
								way_ref = value[1]["v"]
							elif value[1]["k"] == "oneway":
								way_oneway = value[1]["v"]
							elif value[1]["k"] == "highway":
								is_highway = True
						elif event == _event_end and value == "way":
							break
					
					if actioncontext in ( "osm" , "create" , "modify" ) and is_highway:
						queue.put ( ( _command_create_way , ( way_id , way_name , way_ref , way_oneway , way_version ) ) )
						#logging.warning ( "put to queue, size %s" , queue.qsize () )
						if actioncontext in ( "create" , "modify" ):
							queue.put ( ( _command_mark_way_modified , (way_id,) ) )
							#logging.warning ( "put to queue, size %s" , queue.qsize () )
				elif event == _event_end and value == actioncontext:
					break


def consume_command_queue ( connection , queue , modified_node_table_name = "modified_nodes" , modified_way_table_name = "modified_ways" ):
	from models import OSMNode , OSMWay , OSMMembership
	
	command_sql = (
		"DELETE FROM %s WHERE osm_id = %%s" % OSMNode._meta.db_table ,
		"UPDATE %s SET point_4326 = ST_SetSRID ( ST_MakePoint ( %%s , %%s ) , 4326 ) , version = %%s WHERE osm_id = %%s" % OSMNode._meta.db_table ,
		"INSERT INTO %s ( point_4326 , version , osm_id ) VALUES ( ST_SetSRID ( ST_MakePoint ( %%s , %%s ) , 4326 ) , %%s , %%s )" % OSMNode._meta.db_table ,
		
		"DELETE FROM %s WHERE osmway_osm_id = %%s" % OSMMembership._meta.db_table ,
		"INSERT INTO %s ( osmnode_osm_id , osmway_osm_id ) VALUES ( %%s , %%s )" % OSMMembership._meta.db_table ,
		
		"DELETE FROM %s WHERE osm_id = %%s" % OSMWay._meta.db_table ,
		"INSERT INTO %s ( osm_id , name , ref , oneway , version ) VALUES ( %%s , %%s , %%s , %%s , %%s )" % OSMWay._meta.db_table ,
		
		"INSERT INTO %s ( osmnode_osm_id ) VALUES ( %%s )" % modified_node_table_name ,
		"INSERT INTO %s ( osmway_osm_id ) VALUES ( %%s )" % modified_way_table_name ,
	)
	
	cursor = connection.cursor ()
	
	while True:
		command , command_args = queue.get ()
		#logging.warning ( "got from queue, size %s" , queue.qsize () )
		if command == _command_finish:
			break
		cursor.execute ( command_sql[command] , command_args )
	
	transaction.set_dirty ()

def import_planet ( connection , planetfile , is_changefile = False , modified_node_table_name = "modified_nodes" , modified_way_table_name = "modified_ways" , separate_transactions = False ):
	from xml.parsers import expat
	from threading import Thread
	from Queue import Queue
	
	cursor = connection.cursor ()
	
	with transaction.commit_on_success () if separate_transactions else noop_context_manager ():
		if is_changefile:
			logging.debug ( "Creating modified node/way tables" )
			cursor.execute ( "CREATE TEMPORARY TABLE %s ( osmnode_osm_id bigint NOT NULL )" % modified_node_table_name )
			cursor.execute ( "CREATE TEMPORARY TABLE %s ( osmway_osm_id bigint NOT NULL )" % modified_way_table_name )
		
		queue = Queue ( maxsize = 128 )
		
		def producer ():
			logging.debug ( "Setting up parser" )
			gen = planetfile_coroutine ( queue )
			p = expat.ParserCreate ()
			try:
				p.buffer_size = (1<<16)
			except AttributeError:
				# probably not python 2.6 - nothing to worry about
				pass
			p.buffer_text = True
			p.StartElementHandler = lambda name , attrs : gen.send ( ( _event_start , ( name , attrs ) ) )
			p.EndElementHandler = lambda name : gen.send ( ( _event_end , name ) )
		
			logging.info ( "Parsing" )
			p.ParseFile ( planetfile )
			
			# let consumer know we've reached the end
			queue.put ( ( _command_finish , () ) )
		
		producer_thread = Thread ( target = producer )
		producer_thread.daemon = True
		logging.debug ( "Spawning command queue producer" )
		producer_thread.start ()
		
		# set up consumer to submit database commands in main thread
		consume_command_queue ( connection , queue , modified_node_table_name = modified_node_table_name , modified_way_table_name = modified_way_table_name )
		
		logging.debug ( "Waiting for producer thread to terminate" )
		producer_thread.join ()
		
		_analyze_tables ( connection )
	
	with transaction.commit_on_success () if separate_transactions else noop_context_manager ():
		transform_required_nodes ( connection , modified_node_table_name = modified_node_table_name if is_changefile else None )
	
	with transaction.commit_on_success () if separate_transactions else noop_context_manager ():
		calculate_linestrings ( connection , modified_node_table_name = modified_node_table_name if is_changefile else None , modified_way_table_name = modified_way_table_name if is_changefile else None )
	
	with transaction.commit_on_success () if separate_transactions else noop_context_manager ():
		purge_geometryless_ways ( connection )
		
		_analyze_tables ( connection )

def _analyze_tables ( connection ):
	from models import OSMNode , OSMWay , OSMMembership
	
	logging.info ( "Analyzing tables" )
	cursor = connection.cursor ()
	for model in ( OSMNode , OSMWay , OSMMembership ):
		cursor.execute ( "ANALYZE %s" % model._meta.db_table )

def transform_required_nodes ( connection , modified_node_table_name = None ):
	from models import OSMNode , OSMWay , OSMMembership
	
	logging.info ( "Transforming required nodes" )
	
	cursor = connection.cursor ()
	
	extrawhere = "AND ( point_900913 IS NULL OR EXISTS ( SELECT 1 FROM %(modified_node_table_name)s WHERE %(modified_node_table_name)s.osmnode_osm_id = %(node_table_name)s.osm_id ) )" if modified_node_table_name else ""
	
	querystring = ( """UPDATE %%(node_table_name)s SET point_900913 = ST_Transform ( point_4326 , 900913 )
	FROM
		%%(membership_table_name)s
	WHERE
			%%(node_table_name)s.osm_id = %%(membership_table_name)s.osmnode_osm_id
			%s
	""" % extrawhere ) % {
		"node_table_name" : OSMNode._meta.db_table ,
		"way_table_name" : OSMWay._meta.db_table ,
		"membership_table_name" : OSMMembership._meta.db_table ,
		"modified_node_table_name" : modified_node_table_name ,
	}
	
	#cursor.execute ( "EXPLAIN " + querystring )
	#logging.info ( "EXPLAIN output:\n%s" , "\n".join ( chain.from_iterable ( cursor.fetchall () ) ) )
	cursor.execute ( querystring )
	logging.debug ( "Affected %s rows" , cursor.rowcount )
	
	transaction.set_dirty ()

def calculate_linestrings ( connection , modified_node_table_name = None , modified_way_table_name = None , joined_nodes_temp_table_name = "joined_nodes" ):
	from models import OSMNode , OSMWay , OSMMembership
	
	logging.info ( "Calculating linestrings" )
	
	cursor = connection.cursor ()

	if modified_node_table_name and modified_way_table_name:
		extrafrom = """, (
			SELECT
				osmway_osm_id
			FROM
					%(modified_node_table_name)s
				JOIN
					%(membership_table_name)s
				USING ( osmnode_osm_id )
		UNION
			SELECT
				osmway_osm_id
			FROM
				%(modified_way_table_name)s
		) AS all_modified_ways """ % {
			"modified_node_table_name" : modified_node_table_name ,
			"modified_way_table_name" : modified_way_table_name ,
			"membership_table_name" : OSMMembership._meta.db_table ,
		}
		extrawhere = " AND all_modified_ways.osmway_osm_id = outer_membership.osmway_osm_id "
	else:
		extrafrom = extrawhere = ""
	
	logging.info ( "Building temporary table %s" , joined_nodes_temp_table_name )
	cursor.execute ( """CREATE TEMPORARY TABLE %(joined_nodes_temp_table_name)s AS
	SELECT outer_membership.id AS membership_id , outer_membership.osmway_osm_id , point_900913 as point
	FROM
		%(membership_table_name)s AS outer_membership ,
		%(node_table_name)s %(extrafrom)s
	WHERE
		outer_membership.osmnode_osm_id = %(node_table_name)s.osm_id %(extrawhere)s
	-- ordering here is an attempt to lay out data conveniently on disk. don't
	-- know if this works.
	ORDER BY
		outer_membership.osmway_osm_id , outer_membership.id
	""" % {
		"membership_table_name" : OSMMembership._meta.db_table ,
		"node_table_name" : OSMNode._meta.db_table ,
		"joined_nodes_temp_table_name" : joined_nodes_temp_table_name ,
		"extrafrom" : extrafrom ,
		"extrawhere" : extrawhere ,
	} )
	
	cursor.execute ( "ANALYZE %s" % joined_nodes_temp_table_name )
	cursor.execute ( "CREATE INDEX %s_index ON %s ( osmway_osm_id )" % ((joined_nodes_temp_table_name,)*2) )
	
	querystring = """UPDATE %(way_table_name)s SET way = s.linestring
	FROM
	(
		SELECT
			osmway_osm_id ,
			-- membership_id should reflect the order of insertion of the membership and so, assuming all a way's
			-- memberships were added at once as we currently expect, the order in the way too
			CASE WHEN count(point) > 1 THEN ST_MakeLine ( point ORDER BY membership_id ) END AS linestring
		FROM
			%(joined_nodes_temp_table_name)s
		GROUP BY
			osmway_osm_id
	) AS s
	WHERE
		s.osmway_osm_id = %(way_table_name)s.osm_id""" % {
		"way_table_name" : OSMWay._meta.db_table ,
		"joined_nodes_temp_table_name" : joined_nodes_temp_table_name ,
	}
	
	#cursor.execute ( "EXPLAIN " + querystring )
	#logging.info ( "EXPLAIN output:\n%s" , "\n".join ( chain.from_iterable ( cursor.fetchall () ) ) )
	cursor.execute ( querystring )
	logging.debug ( "Affected %s rows" , cursor.rowcount )
	
	transaction.set_dirty ()

def purge_geometryless_ways ( connection ):
	"""
	A way whose nodes are all outside our TSBP_PLANET_BOUNDING_BOX will have had its way linestring calculated as null. Discard.
	What's more, a way with no spatial presence is no use to our analysis.
	"""
	from models import OSMWay , OSMMembership
	
	logging.info ( "Purging geometryless ways" )
	
	cursor = connection.cursor ()
	
	cursor.execute ( "DELETE FROM %(way_table_name)s WHERE way ISNULL" % {
		"way_table_name" : OSMWay._meta.db_table ,
	} )
	
	logging.debug ( "Affected %s rows" , cursor.rowcount )
	
	logging.info ( "Purging wayless memberships" )
	
	cursor.execute ( "DELETE FROM %(membership_table_name)s WHERE NOT EXISTS ( SELECT 1 FROM %(way_table_name)s WHERE osm_id = osmway_osm_id )" % {
		"way_table_name" : OSMWay._meta.db_table ,
		"membership_table_name" : OSMMembership._meta.db_table ,
	} )
	
	logging.debug ( "Affected %s rows" , cursor.rowcount )
	
	transaction.set_dirty ()
