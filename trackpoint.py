# -*- coding: utf-8 -*-
from django.contrib.gis.geos import GEOSGeometry , Point

from datetime import datetime
from math import cos , pi , hypot , sqrt , atan , exp , log
from itertools import chain , tee , izip
from bisect import bisect_left , insort_left , insort_right
import logging
from sys import setrecursionlimit , getrecursionlimit
import threading
from functools32 import lru_cache

from models import OSMWay
from tools import timedelta_total_seconds


_backdrift_recursion_limit = 20000

logger = logging.getLogger ( "tsbp.gpx" )

class SearchDistanceOverflow ( Exception ):
	pass

class NotAllowedOnWayError ( ValueError ):
	pass

@lru_cache ( maxsize = 1024 )
def _get_name_ref_for_osm_way_id ( osm_way_id ):
	way = OSMWay.objects.get ( osm_id = osm_way_id )
	return way.name , way.ref

class TrackPoint ( object ):
	_max_search_distance = 100.0
	_search_distance_step = 25.0
	
	_hinted_distance_bias = 3.0
	_hinted_ripeness_proportion_factor = 500.0
	
	_base_sigma = 6.0
	
	def __init__ ( self , start_element , gpxiter , router , transport , ripeness_multiplier = 1 ):
		point = Point ( float ( start_element.get ( "lon" ) ) , float ( start_element.get ( "lat" ) ) , srid = 4326 )
		point.transform ( 900913 )
		self.point = point
		self.transport = transport
		self.dirty_listeners = {} # intended to be a dict of listener:cookie pairs, preferably with a weakref'd listener. maybe this should be hidden away a bit.
		self.router = router # perhaps this should be a more general track reference. iunno.
		self._ripeness_multiplier = ripeness_multiplier
		
		self._already_searched_distance = 0.0
		self._max_calculated_intrinsic_selection_cost = 0.0
		
		for event , element in gpxiter:
			if event == "end" and element.tag.endswith ( "}hdop" ):
				self._hdop = float ( element.text )
			elif event == "end" and element.tag.endswith ( "}time" ):
				normalized_text = element.text.replace ( "-" , "" )
				for format_candidate , has_fractional_seconds in ( ("%Y%m%dT%H:%M:%S.%fZ",True) , ("%Y%m%dT%H:%M:%S.%f",True) , ("%Y%m%dT%H:%M:%SZ",False) , ("%Y%m%dT%H:%M:%S",False) ):
					try:
						self._raw_timestamp = datetime.strptime ( normalized_text , format_candidate )
					except ValueError:
						pass
					else:
						self._has_fractional_seconds = has_fractional_seconds
						break
				else:
					raise ValueError
			elif event == "end" and element is start_element:
				element.clear ()
				break
		
		self.calculated_candidates = []
		self._uncalculated_candidates = []
		self._all_candidates = {}
	
	_user_pinned = None
	
	def _get_user_pinned ( self ):
		return self._user_pinned
	
	def _set_user_pinned ( self , user_pinned ):
		"""
			a non-negative value pins trackpoint to that calculated_candidate index
			
			a negative value pins trackpoint to any calculated_candidate with index >= +ve value equivalent
		"""
		
		# let's not let anything weird happen
		assert user_pinned is None or user_pinned < len ( self.calculated_candidates )
		assert user_pinned is None or user_pinned >= -len ( self.calculated_candidates )
		
		self._user_pinned = user_pinned
		# clear calculated candidates' memos which will have depended on this setting
		for candidate in self.calculated_candidates:
			if hasattr ( candidate , "_intrinsic_cost" ):
				del candidate._intrinsic_cost
			if hasattr ( candidate , "_intrinsic_selection_cost" ):
				del candidate._intrinsic_selection_cost
		
		for listener , cookie in self.dirty_listeners.items ():
			listener.mark_dirty_trackpoint ( cookie )
	
	user_pinned = property ( _get_user_pinned , _set_user_pinned )
	
	def _fetch_candidate_lookup ( self , radius ):
		return OSMWay.objects.filter ( osm_id__gte = 0 ).filter ( way__bboverlaps = self.point.buffer ( radius / self.local_scale_factor ) )
	
	def _fetch_more_candidates ( self , backdrift_dominators ):
		while True:
			new_search_distance = self._already_searched_distance + self._search_distance_step
			if new_search_distance > self._max_search_distance:
				raise SearchDistanceOverflow
			
			new_candidates = self._fetch_candidate_lookup ( new_search_distance ).exclude ( osm_id__in = self._all_candidates.iterkeys () )
			self._already_searched_distance = new_search_distance
			
			now_has_acceptable_candidate = bool ( self._spare_candidate_count )
			neutral_intrinsic_cost = self._neutral_intrinsic_cost_already_searched_distance
			
			# we're going to need a copy of the backdrift_dominators iterator for each candidate, so let's
			# pregenerate them. shouldn't matter if one of these doesn't get used by its (discarded) candidate.
			backdrift_dominators_copies = tee ( backdrift_dominators , len ( new_candidates ) )
			
			# again, should really convert propose_osmway to be iterative, not recursive - temporary fix?
			previous_recursion_limit = getrecursionlimit ()
			setrecursionlimit ( _backdrift_recursion_limit )
			
			for candidate , backdrift_dominators_copy in izip ( new_candidates , backdrift_dominators_copies ):
				try:
					new_powlist = self._propose_osmway ( candidate , backdrift_dominators_copy )
				except NotAllowedOnWayError:
					continue
				
				# new_powlist[0] should have the lowest intrinsic_selection_cost
				if new_powlist[0].intrinsic_selection_cost < neutral_intrinsic_cost:
					now_has_acceptable_candidate = True
			
			setrecursionlimit ( previous_recursion_limit )
			
			if now_has_acceptable_candidate:
				break
	
	# a wrapper for external use of _fetch_more_candidates that will mark trackpoint dirty
	def fetch_more_candidates ( self , *args , **kwargs ):
		for listener , cookie in self.dirty_listeners.items ():
			listener.mark_dirty_trackpoint ( cookie )
		return self._fetch_more_candidates ( *args , **kwargs )
	
	def has_any_candidates ( self , backdrift_dominators ):
		if self.calculated_candidates or self._uncalculated_candidates:
			return True
		try:
			self.fetch_more_candidates ( backdrift_dominators )
		except SearchDistanceOverflow:
			return False
		
		return True
	
	def gather_impostors ( self , backdrift_dominators ):
		for osm_way_id , powlist in self._all_candidates.items ():
			if powlist is not None:
				powlist.gather_impostors ( backdrift_dominators )
	
	def determine_velocity_vector ( self , prev , next ):
		nvectors_to_average = []
		if next:
			vector_to_next = next.point[0] - self.point[0] , next.point[1] - self.point[1]
			seconds_to_next = timedelta_total_seconds ( next.timestamp - self.timestamp )
			nvector_to_next = vector_to_next[0] / max ( seconds_to_next , 0.001 ) , vector_to_next[1] / max ( seconds_to_next , 0.001 )
			nvectors_to_average.append ( nvector_to_next )
		if prev:
			vector_from_prev = self.point[0] - prev.point[0] , self.point[1] - prev.point[1]
			seconds_from_prev = timedelta_total_seconds ( self.timestamp - prev.timestamp )
			nvector_from_prev = vector_from_prev[0] / max ( seconds_from_prev , 0.001 ) , vector_from_prev[1] / max ( seconds_from_prev , 0.001 )
			nvectors_to_average.append ( nvector_from_prev )
		
		# set guessed velocity in ms-1
		nvector_average = reduce ( lambda a , b : ( a[0] + b[0] , a[1] + b[1] ) , nvectors_to_average , ( 0.0 , 0.0 ) )
		self.velocity_vector = nvector_average[0] * self.local_scale_factor / max ( len ( nvector_average ) , 1 ) , nvector_average[1] * self.local_scale_factor / max ( len ( nvector_average ) , 1 )
	
	def advance_next_candidate ( self , backdrift_dominators = None ):
		for listener , cookie in self.dirty_listeners.items ():
			listener.mark_dirty_trackpoint ( cookie )
		
		if not self._spare_candidate_count:
			if backdrift_dominators is None:
				# backdrift_dominators must be supplied if there is any chance of candidate fetch being triggered.
				raise TypeError ( "backdrift_dominators not supplied but candidates need fetching" )
			try:
				self._fetch_more_candidates ( backdrift_dominators )
			except SearchDistanceOverflow:
				# ok, let's just let the dregs through, if any
				pass
		candidate = self._uncalculated_candidates.pop ( 0 )
		self.calculated_candidates.append ( candidate )
		self._max_calculated_intrinsic_selection_cost = max ( candidate.intrinsic_selection_cost , self._max_calculated_intrinsic_selection_cost )
		return candidate
	
	def purge_impostors ( self ):
		"""
			Remove all uncalculated impostors
		"""
		for candidate in self._uncalculated_candidates[:]:
			if candidate.impostor_for is not None:
				logger.debug ( "removing impostor candidate %s" , candidate )
				self._uncalculated_candidates.remove ( candidate )
				self._all_candidates[candidate.powlist.osm_way_id].remove ( candidate )
				if not self._all_candidates[candidate.powlist.osm_way_id]:
					# powlist now empty, so let's remove it for consistency
					del self._all_candidates[candidate.powlist.osm_way_id]
				# don't think we should need a re-sort (?)
	
	def _propose_osmway ( self , osmway , backdrift_dominators , hinted = False ):
		if not self.router.allowed_on_way ( osmway ):
			# discard this way
			self._all_candidates[osmway.osm_id] = None
			raise NotAllowedOnWayError
		
		self._all_candidates[osmway.osm_id] = new_powlist = PointOnWayList ( self , osmway , backdrift_dominators , hinted = hinted )
		for new_pow in new_powlist:
			# using insort_right as we're iterating though new_powlist left-to-right and we want to
			# preserve new_powlist's ordering on same-valued pows
			insort_right ( self._uncalculated_candidates , new_pow )
		
		return new_powlist
	
	# a wrapper for external use of _propose_osmway that will mark trackpoint dirty
	def propose_osmway ( self , *args , **kwargs ):
		for listener , cookie in self.dirty_listeners.items ():
			listener.mark_dirty_trackpoint ( cookie )
		return self._propose_osmway ( *args , **kwargs )
	
	def apply_hint_set ( self , hint_set , backdrift_dominators ):
		dirty = False
		
		# shouldn't be possible for a "blacklist" _all_candidates member to be in hint_set
		all_candidates_keys = frozenset ( self._all_candidates.iterkeys () )
		known_hinted = hint_set & all_candidates_keys
		if known_hinted:
			# first check to see if we've already calculated a member of hint_set
			for candidate_pow in self.calculated_candidates:
				if candidate_pow.powlist.osm_way_id in hint_set:
					# this trackpoint is fine - give up
					return
			
			for osm_id in known_hinted:
				self._all_candidates[osm_id][0].hinted = True
			
			# this will need to be resorted
			self._uncalculated_candidates.sort ( key = PointOnWay.sort_key_function )
			
			dirty = True
		
		new_hinted_candidates = self._fetch_candidate_lookup ( self._max_search_distance * self._hinted_distance_bias ).filter ( osm_id__in = hint_set - all_candidates_keys )
		
		# again, should really convert propose_osmway to be iterative, not recursive - temporary fix?
		previous_recursion_limit = getrecursionlimit ()
		setrecursionlimit ( _backdrift_recursion_limit )
		
		for candidate in new_hinted_candidates:
			dirty = True
			try:
				new_powlist = self._propose_osmway ( candidate , backdrift_dominators , hinted = True )
			except NotAllowedOnWayError:
				continue
		
		setrecursionlimit ( previous_recursion_limit )
		
		if dirty:
			for listener , cookie in self.dirty_listeners.items ():
				listener.mark_dirty_trackpoint ( cookie )
	
	def set_shared_timestamp_info ( self , position , count ):
		self._shared_timestamp_position = position
		self._shared_timestamp_count = count
	
	@property
	def _spare_candidate_count ( self ):
		return bisect_left ( self._uncalculated_candidates , self._neutral_intrinsic_cost_already_searched_distance )
	
	@property
	def _neutral_intrinsic_cost_already_searched_distance ( self ):
		return PointOnWay.neutral_intrinsic_cost_at_distance ( self._already_searched_distance , self )
	
	@property
	def _neutral_intrinsic_cost_max_search_distance ( self ):
		return PointOnWay.neutral_intrinsic_cost_at_distance ( self._max_search_distance , self )
	
	@property
	def _intrinsic_cost_next_uncalculated_candidate ( self ):
		return self._uncalculated_candidates[0].intrinsic_cost if self._uncalculated_candidates and self._uncalculated_candidates[0].intrinsic_cost < self._neutral_intrinsic_cost_max_search_distance else None
	
	@property
	def _next_beyond_range_candidate_ripeness_factor ( self ):
		nicnuc = self._intrinsic_cost_next_uncalculated_candidate
		return self._neutral_intrinsic_cost_already_searched_distance / nicnuc if nicnuc is not None else 0
	
	def _get_ripeness_multiplier ( self ):
		return self._ripeness_multiplier
	
	def _set_ripeness_multiplier ( self , ripeness_multiplier ):
		if ripeness_multiplier < 0:
			raise ValueError ( "Can't have a negative ripeness_multiplier" )
		
		self._ripeness_multiplier = ripeness_multiplier
		for listener , cookie in self.dirty_listeners.items ():
			listener.mark_dirty_trackpoint ( cookie )
		return self._ripeness_multiplier
	
	ripeness_multiplier = property ( _get_ripeness_multiplier , _set_ripeness_multiplier )
	
	@property
	def ripeness ( self ):
		if not self.calculated_candidates:
			return None
		
		if self._ripeness_multiplier == 0:
			# save a bit of time
			return 0
		
		if self._user_pinned is not None and self._user_pinned >= 0:
			# we've been pinned to one of our existing candidates. no point in calculating anything more.
			return 0
		
		spare_candidate_count = self._spare_candidate_count
		proportional_component = ( 1.0 - ( self._already_searched_distance / self._max_search_distance ) ) + ( spare_candidate_count or self._next_beyond_range_candidate_ripeness_factor )
		if spare_candidate_count:
			next_spare_candidate_term = self._max_calculated_intrinsic_selection_cost / self._uncalculated_candidates[0].intrinsic_selection_cost
			if next_spare_candidate_term > 1.0:
				next_spare_candidate_term *= self._hinted_ripeness_proportion_factor
			proportional_component += next_spare_candidate_term
		#if self._uncalculated_candidates:
			#proportional_component += ( max ( self._max_calculated_intrinsic_selection_cost / self._uncalculated_candidates[0].intrinsic_selection_cost , 1.0 ) - 1.0 ) * self._hinted_ripeness_proportion_factor
		inverse_component = sum ( ( 0.5 if candidate.hinted else 1.0 ) for candidate in self.calculated_candidates ) ** 1.9
		return self._ripeness_multiplier * proportional_component / inverse_component
	
	@property
	def local_scale_factor ( self ):
		"""
		Approx metres / map unit in this local area
		"""
		if not hasattr ( self , "_local_scale_factor" ):
			if self.point.srid != 900913:
				raise ValueError ( "This only works for points in 900913 projection" )
		
			self._local_scale_factor = cos ( ( self.point.coords[1] * 85.0511 * pi ) / ( 20037508.34 * 90.0 * 2.0 ) )
		
		return self._local_scale_factor
	
	@property
	def timestamp ( self ):
		if not hasattr ( self , "_timestamp" ):
			if hasattr ( self , "_shared_timestamp_position" ) and hasattr ( self , "_shared_timestamp_count" ) and not self._has_fractional_seconds:
				self._timestamp = self._raw_timestamp.replace ( microsecond = int ( self._shared_timestamp_position * 1.e6 / self._shared_timestamp_count ) )
			else:
				self._timestamp = self._raw_timestamp
		return self._timestamp
	
	@property
	def hdop ( self ):
		if hasattr ( self , "_hdop" ):
			return self._hdop
		return 3.0
	
	def get_calculated_candidates_summary ( self ):
		for candidate in self.calculated_candidates:
			yield candidate.get_summary_dict ()
	
	def __repr__ ( self ):
		return "<TP@(%s,%s),uc=%s,cc=%s,r=%s,asd=%s>" % ( self.point[0] , self.point[1] , len ( self._uncalculated_candidates ) , len ( self.calculated_candidates ) , self.ripeness , self._already_searched_distance )

class PointOnWayList ( list ):
	def __init__ ( self , trackpoint , osmway , backdrift_dominators , hinted = False ):
		self.osm_way_id = osmway.osm_id
		self.osm_way_onewayness = 1 if trackpoint.router.is_oneway ( osmway ) else 0
		if osmway.oneway == "-1":
			self.osm_way_onewayness *= -1
		
		self.trackpoint = trackpoint # ideally this should be a weakref, but weakrefs have trouble being pickled
		super ( PointOnWayList , self ).__init__ ( PointOnWay.iter_from_powlist_osmway ( self , osmway ) )
		self.sort ()
		
		self.gather_impostors ( backdrift_dominators , osmway )
		
		if hinted:
			self[0].hinted = True
	
	def gather_impostors ( self , backdrift_dominators , osmway = None ):
		"""
			Consider whether to give ourselves any impostors
		"""
		if not self.osm_way_onewayness:
			return
		
		# vvvv this next test won't work with hints. problematic?
		# make sure our "best" point isn't beyond our trackpoint's _neutral_intrinsic_cost_max_search_distance
		# to slightly limit how deep a recursive propose_osmway can go needlessly
		if self[0].intrinsic_selection_cost < self.trackpoint._neutral_intrinsic_cost_max_search_distance:
			try:
				immediate_dominator = backdrift_dominators.next ()
			except StopIteration:
				pass
			else:
				if self.osm_way_id not in immediate_dominator._all_candidates:
					# hmm. recursion depth resulting from this call potentially troublesome. should probably reimplement
					# using iteration.
					immediate_dominator.propose_osmway ( osmway or OSMWay.objects.get ( osm_id = self.osm_way_id ) , backdrift_dominators )
				
				# immediate_dominator should have at least one pow by now
				# taking advantage of onewayness' value being 1 or -1 and multiplying by it to cause
				# the inequality to be valid for both situations
				immediate_dominators_target_pow = immediate_dominator._all_candidates[self.osm_way_id][0]
				if ( self[0].length_along_way - immediate_dominators_target_pow.length_along_way ) * self.osm_way_onewayness < 0:
					# provisionally looks like a backdrift situation.
					# get a PointOnWay that is almost a clone of dominator's so we can check it out some more
					new_pow = PointOnWay ( self , immediate_dominators_target_pow.nearest_point , immediate_dominators_target_pow.direction_vector , immediate_dominators_target_pow.length_along_way , impostor_for = self[0] )
					
					# TODO worry about traversal across the join of "closed" ways giving us backdrift false-positives
					
					# vvv note we're looking at intrinsic_cost here, not intrinsic_selection_cost
					if new_pow.intrinsic_cost < self.trackpoint._neutral_intrinsic_cost_max_search_distance:
						# ok - we probably want this impostor.
						# we can just shove it in the front of our list as it will at least pretend to share a lowest intrinsic_selection_cost
						# and we want it to have precedent over the non-impostor
						self.insert ( 0 , new_pow )
						logger.debug ( "inserted impostor candidate %s" , new_pow )
	
	def sort ( self , *args , **kwargs ):
		kwargs.setdefault ( "key" , PointOnWay.sort_key_function )
		super ( PointOnWayList , self ).sort ( *args , **kwargs )

class PointOnWay ( object ):
	# used by repr for thread safety of recursion-proof routine
	_local = threading.local ()
	_local.repr_called_set = set()
	
	_user_pinned_other_intrinsic_selection_cost = 1.e15
	
	def __init__ ( self , powlist , nearest_point , direction_vector , length_along_way , impostor_for = None , hinted = False ):
		# note length_along_way is in map units
		self.powlist = powlist # ideally this should be a weakref, but weakrefs have trouble being pickled
		self.nearest_point = nearest_point
		self.direction_vector = direction_vector
		self.length_along_way = length_along_way
		self.impostor_for = impostor_for
		self._hinted = hinted
		
		# don't need to divide by magnitude here as direction vector is unit vector.
		parallel_velocity = dot_product = powlist.trackpoint.velocity_vector[0] * direction_vector[0] + powlist.trackpoint.velocity_vector[1] * direction_vector[1]
		velocity_magnitude = hypot ( *powlist.trackpoint.velocity_vector )
		
		if self.powlist.osm_way_onewayness:
			if self.powlist.osm_way_onewayness == -1:
				# simply negate parallel velocity
				parallel_velocity = -parallel_velocity
			
			# effectively multiply by heaviside step function
			parallel_velocity = max ( 0.0 , parallel_velocity )
		
		## calculate directional_factor and distance_factor as probabilities
		
		# note the protection against division by zero
		directional_factor = ( (parallel_velocity**2) / max ( velocity_magnitude , 1.e-3 ) ) - 0.5*velocity_magnitude
		
		# using atan as our sigmoid function here as it has a gradient of 1 around 0
		# multiplied so that increased speed really starts to have a diminishing effect over ~directional_factor_rolloff_velocity ms-1
		directional_factor = atan ( directional_factor / self.powlist.trackpoint.transport["directional_factor_rolloff_velocity"] )
		
		# multiply & shift to end up with a number between 0 and 1 to denote directional cost factor
		self.directional_factor = ( directional_factor / pi ) + 0.5
		
		# distance from trackpoint to nearest_point in metres
		self.distance = nearest_point.distance ( powlist.trackpoint.point ) * powlist.trackpoint.local_scale_factor
	
	@classmethod
	def iter_from_powlist_osmway ( cls , powlist , osmway ):
		from geometry import iter_distance_minima_points
		for nearest_point , direction_vector , length_along_way in iter_distance_minima_points ( osmway.way , powlist.trackpoint.point ):
			yield cls ( powlist , nearest_point , direction_vector , length_along_way )
	
	def _get_hinted ( self ):
		return self._hinted
	
	def _set_hinted ( self , hinted ):
		self._hinted = hinted
		# clear memos
		if hasattr ( self , "_intrinsic_cost" ):
			del self._intrinsic_cost
		if hasattr ( self , "_intrinsic_selection_cost" ):
			del self._intrinsic_selection_cost
	
	hinted = property ( _get_hinted , _set_hinted )
	
	# a bunch of these calculations are split out into (probably quite inefficient) properties and static methods
	# so when i experiment with these heuristics and change things i don't have to remember to change things that
	# appear in several places together.
	
	@property
	def distance_factor ( self ):
		return self.distance_factor_given_distance_and_trackpoint ( self.distance , self.powlist.trackpoint )
	
	@property
	def hinted_distance_factor ( self ):
		return self.distance_factor_given_distance_and_trackpoint ( self.distance , self.powlist.trackpoint , base_sigma = self.powlist.trackpoint._base_sigma * self.powlist.trackpoint._hinted_distance_bias )
	
	@property
	def hinted_directional_factor ( self ):
		# would be nice to have a better model for directional leeway
		return self.directional_factor**(1.0/self.powlist.trackpoint._hinted_distance_bias)
	
	@staticmethod
	def _intrinsic_cost_given_factors ( directional_factor , distance_factor ):
		# cost being expressed as log inverse probability of this being the right candidate
		return -log (  max ( directional_factor * distance_factor , 1.e-35 ) )
	
	def _get_intrinsic_cost ( self , ignore_user_pinning = False ):
		if not hasattr ( self , "_intrinsic_cost" ):
			user_pinned = self.powlist.trackpoint.user_pinned
			calculated_position = None
			if user_pinned is not None and not ignore_user_pinning:
				# sadly we can't use .index () to find this due to our odd notion of equality for PointOnWays
				for i , candidate in enumerate ( self.powlist.trackpoint.calculated_candidates ):
					if self is candidate:
						calculated_position = i
						break
			
			if calculated_position is not None and not ( user_pinned == calculated_position or 0 > user_pinned >= -calculated_position ):
				self._intrinsic_cost = self._user_pinned_other_intrinsic_selection_cost
			else:
				self._intrinsic_cost = self._intrinsic_cost_given_factors ( self.directional_factor , self.distance_factor )
		return self._intrinsic_cost
	
	intrinsic_cost = property ( _get_intrinsic_cost )
	
	@property
	def intrinsic_selection_cost ( self ):
		if not hasattr ( self , "_intrinsic_selection_cost" ):
			actual_target = self if self.impostor_for is None else self.impostor_for
			self._intrinsic_selection_cost = actual_target._intrinsic_cost_given_factors ( actual_target.hinted_directional_factor , actual_target.hinted_distance_factor ) if self.hinted else actual_target._get_intrinsic_cost ( ignore_user_pinning = True )
		return self._intrinsic_selection_cost
	
	@staticmethod
	def distance_factor_given_distance_and_trackpoint ( distance , trackpoint , base_sigma = None ):
		"""
		Factored out for use from outside class
		"""
		if base_sigma is None:
			base_sigma = trackpoint._base_sigma
		
		# make a stab at estimating how likely this gps positional error is given a possible hdop.
		sigma = base_sigma * trackpoint.hdop
		
		# use the cumulative rayleigh distribution. should always return a number between 0 and 1.
		return exp ( -( (distance**2.0) / (2.0*(sigma**2.0)) ) )
	
	@classmethod
	def neutral_intrinsic_cost_at_distance ( cls , distance , trackpoint ):
		return cls._intrinsic_cost_given_factors ( 0.5 , cls.distance_factor_given_distance_and_trackpoint ( distance , trackpoint ) )
	
	@staticmethod
	def sort_key_function ( pointonway ):
		return pointonway.intrinsic_selection_cost
	
	def __cmp__ ( self , other ):
		try:
			return cmp ( self.intrinsic_selection_cost , other.intrinsic_selection_cost )
		except AttributeError:
			# hm. perhaps it's a numeric object
			return cmp ( self.intrinsic_selection_cost , other )
	
	def __repr__ ( self ):
		# idea for recursion-proof repr from http://stackoverflow.com/questions/2858921/how-do-i-handle-recursive-reprs-in-python
		repr_called_set = self._local.repr_called_set
		id_self = id ( self )
		if id_self in repr_called_set:
			# break recursive loop
			return "<pow@%r(recursively called)>" % id_self
		try:
			repr_called_set.add ( id_self )
			return ( "<pow%s@%s,ic=%s,isc=%s>" % ( self.powlist.osm_way_id , self.nearest_point.coords , self.intrinsic_cost , self.intrinsic_selection_cost ) )
		finally:
			repr_called_set.remove ( id_self )
	
	def worth_hinting_to ( self , other ):
		# this was briefly:
		# return self.powlist.osm_way_id != other.powlist.osm_way_id or self.nearest_point != other.nearest_point
		# but i now think that's probably not a good idea, but I'm leaving this mechanism in as it may be useful in future
		return True
	
	def seconds_metres_to ( self , other , with_alternatives = False ):
		return self.powlist.trackpoint.router.seconds_metres_from_to ( self.nearest_point , self.powlist.osm_way_id , other.nearest_point , other.powlist.osm_way_id , with_alternatives = with_alternatives )
	
	def geojson_dict_to ( self , other , extra_properties = {} , with_alternatives = False ):
		from geometry import encoded_polyline_to_geojson_dict
		from router import auxiliary_routers
		route_data = auxiliary_routers[self.powlist.trackpoint.transport["label"]].get_route ( self.nearest_point , self.powlist.osm_way_id , other.nearest_point , other.powlist.osm_way_id , geometry = True , with_alternatives = with_alternatives )
		geometries_iter = iter ( route_data["route_geometry"] )
		if with_alternatives:
			geometries_iter = chain ( geometries_iter , route_data["alternative_geometries"] )
		
		results = []
		for geometry in geometries_iter:
			gd = encoded_polyline_to_geojson_dict ( auxiliary_routers[self.powlist.trackpoint.transport["label"]].get_route ( self.nearest_point , self.powlist.osm_way_id , other.nearest_point , other.powlist.osm_way_id , geometry = True )["route_geometry"] )
			gd["properties"] = {
				"from_way" : self.powlist.osm_way_id ,
				"to_way" : other.powlist.osm_way_id ,
				"from_ic" : self.intrinsic_cost ,
				"to_ic" : other.intrinsic_cost ,
			}
			gd["properties"].update ( extra_properties )
			results.append ( gd )
		
		return result if with_alternatives else result[0]
	
	def get_summary_dict ( self ):
		d = {
			"id" : self.powlist.osm_way_id ,
			"c" : self.nearest_point.coords , # outputting coords in 900913 as it will likely never be used
		}
		
		name , ref = _get_name_ref_for_osm_way_id ( self.powlist.osm_way_id )
		if name:
			d["n"] = name
		if ref:
			d["r"] = ref
		
		return d
	
	def iterpoints_to ( self , other ):
		from geometry import decode_line
		from router import auxiliary_routers
		return decode_line ( auxiliary_routers[self.powlist.trackpoint.transport["label"]].get_route ( self.nearest_point , self.powlist.osm_way_id , other.nearest_point , other.powlist.osm_way_id , geometry = True )["route_geometry"] )
