# -*- coding: utf-8 -*-
from numpy import array , unravel_index , append as numpy_append

from math import floor , ceil , log , atan , pi
from bisect import bisect_left , insort_left
from itertools import chain , imap , izip
from collections import deque
import logging

from functools32 import lru_cache

from tools import iterwindow , iterpad , iterdual , timedelta_total_seconds


from eventlet import GreenPool
pool = GreenPool ()

logger = logging.getLogger ( "tsbp.gpx" )

class ZeroRipenessError ( ValueError ):
	pass

class HintingPointlessError ( ValueError ):
	pass

class UnsolvableRouteError ( ValueError ):
	pass

def _matrix_pseudo_multiplication ( a , b ):
	return ( a[:,None] + b.T[None,:] ).min ( -1 )

def _pseudo_identity_matrix ( n ):
	return array ( [ [ 0 if i == j else float("inf") for j in xrange ( n ) ] for i in xrange ( n ) ] , dtype = "f4" )

_hint_route_already_hinted_penalty = 1.e8


class Track ( list ):
	def __init__ ( self , start_element , gpxiter , router , transport , blacklist = None ):
		self._blacklist = blacklist or [] # blacklist is assumed to be sorted
		self.router = router
		self.transport = transport
		self.metadata = {}
		super ( Track , self ).__init__ ( self._gen_from_gpx ( start_element , gpxiter ) )
		self.routable_track = RoutableTrack ( self._gen_routable ( self._gen_flag_same_times ( chain.from_iterable ( self ) ) , self._blacklist ) , self.router )
	
	def _gen_from_gpx ( self , start_element , gpxiter ):
		for event , element in gpxiter:
			if event == "start" and element.tag.endswith ( "}trkseg" ):
				yield TrackSegment ( element , gpxiter , self.router , self.transport )
			elif event == "end" and element is start_element:
				element.clear ()
				break
			elif event == "end" and element.tag.split ( "}" )[-1] in ( "name" , "cmt" , "desc" , "src" , "number" , "type" ,):
				tagbasename = element.tag.split ( "}" )[-1]
				if tagbasename == "number":
					try:
						self.metadata[tagbasename] = int ( element.text )
					except ValueError:
						pass
				else:
					self.metadata[tagbasename] = element.text
			
			if event == "end":
				# clear any unhandled elements as they're ended
				element.clear ()
	
	@staticmethod
	def _gen_flag_same_times ( iterable ):
		# build up a buffer of trackpoints and only release them when we're sure the following trackpoint doesn't
		# share the same timestamp
		same_time_buffer = deque ()
		# the extra None here should never get yielded, but should just push through the final entries in the
		# same_time_buffer by not satisfying the equal clause
		for trackpoint in chain ( iterable , (None,) ):
			if not ( same_time_buffer and trackpoint is not None and trackpoint._raw_timestamp == same_time_buffer[0]._raw_timestamp ):
				len_same_time_buffer = len ( same_time_buffer )
				if len_same_time_buffer == 1:
					# a shortcut
					yield same_time_buffer.popleft ()
				else:
					# release built up buffer
					i = 0
					while same_time_buffer:
						past_trackpoint = same_time_buffer.popleft ()
						past_trackpoint.set_shared_timestamp_info ( i , len_same_time_buffer )
						yield past_trackpoint
						i += 1
				
				# buffer should now be empty
			
			same_time_buffer.append ( trackpoint )
		
	@staticmethod
	def _gen_routable ( iterable , blacklist ):
		from trackpoint import SearchDistanceOverflow
		dominators = deque ()
		for i , ( j , ( prev , current , next ) ) in iterdual ( blacklist , enumerate ( iterwindow ( iterpad ( iterable , 1 ) , 3 , partials = False ) ) , equality_function = lambda i , ( j , x ): i == j  ):
			if i is not None:
				# trackpoint is in blacklist
				continue
			
			# TODO check for back-in-time trackpoints and discard
			try:
				current.determine_velocity_vector ( prev , next )
				current.fetch_more_candidates ( reversed ( dominators ) )
				dominators.append ( current )
				yield current
			except SearchDistanceOverflow:
				# exception will have been thrown before we got a chance to yield it to the routable track or add it to the dominators. this is deliberate.
				pass
	
	def _get_blacklist ( self ):
		return self._blacklist
	
	def _set_blacklist ( self , new_blacklist ):
		# in case it's just a sequence
		new_blacklist = list ( new_blacklist )
		if self._blacklist == new_blacklist:
			return
		
		logger.info ( "Applying new blacklist to track" )
		
		last_routable_index = -1
		
		# we're going to be iterating through a copy of the routable_track, but we're going to
		# be modifying the actual thing so we need to track the offset we create between the two
		routable_index_offset = 0
		
		for blacklist_entry , ( routable_pair , ( track_index , track_tp ) ) in iterdual ( new_blacklist , iterdual ( enumerate ( self.routable_track[:] ) , enumerate ( chain.from_iterable ( self ) ) , equality_function = lambda ( j , x ) , ( k , y ): x is y ) , equality_function = lambda i , ( _ , ( k , y ) ): i == k ):
			if routable_pair is not None:
				# can only unpack these if routable_pair got through the inner iterdual
				routable_index , routable_tp = routable_pair
				last_routable_index = routable_index
			else:
				routable_index = routable_tp = None
			
			if blacklist_entry is None and routable_pair is None:
				# not blacklisted but not in routable_track. check to see whether we could put it in routable_track
				# (TODO should we do something about somehow resetting tp's calculated_candidates?)
				# using an empty dominators because we're about to purge all the impostors anyway
				if not track_tp.has_any_candidates ( () ):
					# no, we can't put this in routable_track
					continue
				
				i = last_routable_index + routable_index_offset
				self.routable_track[i+1:i+1] = [ track_tp ]
				self.routable_track.transitions[max(i,0):i+1] = ( Transition ( left_tp , right_tp ) for left_tp , right_tp in iterwindow ( self.routable_track[max(i,0):i+3] , 2 , partials = False ) )
				routable_index_offset += 1
			
			if blacklist_entry is None and routable_pair is not None:
				# track_tp is in routable_track, and it's not blacklisted anyway - that's fine
				continue
			
			if blacklist_entry is not None and routable_pair is None:
				# track_tp is not in routable_track, but it's blacklisted anyway - no action
				continue
			
			if blacklist_entry is not None and routable_pair is not None:
				# blacklisted - seems we should remove this from routable_track
				i = routable_index + routable_index_offset
				self.routable_track[i:i+1] = []
				self.routable_track.transitions[max(i-1,0):i+1] = ( Transition ( left_tp , right_tp ) for left_tp , right_tp in iterwindow ( self.routable_track[max(i-1,0):i+1] , 2 , partials = False ) )
				routable_index_offset -= 1
				
				# remove ourselves as a listener
				self.routable_track.transitions.remove_dirty_listeners_from_trackpoint ( routable_tp )
		
		# we could probably be a bit more selective over which trackpoints we need to reconsider impostors for
		# but this doesn't really seem particularly slow
		self.routable_track.refresh_impostors ()
		
		self.routable_track.transitions.refresh_structure ()
		
		self._blacklist = new_blacklist
	
	blacklist = property ( _get_blacklist , _set_blacklist )
	
	def _get_user_pinnings ( self ):
		return [ ( i , trackpoint.user_pinned ) for i , trackpoint in enumerate ( chain.from_iterable ( self ) ) if trackpoint.user_pinned is not None ]
	
	def _set_user_pinnings ( self , new_user_pinnings ):
		for new_pinning_tuple , ( j , trackpoint ) in iterdual ( sorted ( new_user_pinnings ) , enumerate ( chain.from_iterable ( self ) ) , equality_function = lambda ( i , new_pinning ) , ( j , trackpoint ): i == j ):
			if new_pinning_tuple is None:
				if trackpoint.user_pinned is not None:
					trackpoint.user_pinned = None
				continue
			
			i , new_pinning = new_pinning_tuple
			
			if new_pinning == trackpoint.user_pinned:
				continue
			
			if new_pinning is None:
				trackpoint.user_pinned = new_pinning
				continue
			
			int_new_pinning = int ( new_pinning )
			
			if int_new_pinning >= len ( trackpoint.calculated_candidates ):
				raise IndexError ( "pinning value %i for track index %i cannot be applied as trackpoint only has %i calculated_candidates" % ( new_pinning , i , len ( trackpoint.calculated_candidates ) ) )
			
			trackpoint.user_pinned = max ( int_new_pinning , -len ( trackpoint.calculated_candidates ) )
	
	user_pinnings = property ( _get_user_pinnings , _set_user_pinnings )
	
	@property
	def annotated_user_pinnings ( self ):
		# meh good enough for now
		total_track = list ( chain.from_iterable ( self ) )
		return [ (index,pinning,total_track[index].calculated_candidates[pinning].get_summary_dict () if pinning >= 0 else None) for index , pinning in self.user_pinnings ]

class TrackSegment ( list ):
	def __init__ ( self , start_element , gpxiter , router , transport ):
		self.router = router
		self.transport = transport
		super ( TrackSegment , self ).__init__ ( self._gen_from_gpx ( start_element , gpxiter ) )
	
	def _gen_from_gpx ( self , start_element , gpxiter ):
		from trackpoint import TrackPoint
		for event , element in gpxiter:
			if event == "start" and element.tag.endswith ( "}trkpt" ):
				yield TrackPoint ( element , gpxiter , self.router , self.transport )
			elif event == "end" and element is start_element:
				element.clear ()
				break

class RoutableTrack ( list ):
	def __init__ ( self , sequence , router ):
		self.router = router
		super ( RoutableTrack , self ).__init__ ( sequence )
		self.transitions = TransitionList ( self )
	
	def refresh_impostors ( self ):
		for i , tp in enumerate ( self ):
			tp.purge_impostors ()
			dominators = self[:i]
			tp.gather_impostors ( reversed ( dominators ) )
	
	def advance_trackpoints ( self , tp_indexes ):
		for tp_index in tp_indexes:
			try:
				self[tp_index].advance_next_candidate ( reversed ( self[0:tp_index] ) )
			except IndexError:
				pass
	
	def _iterate_scatter_solve ( self ):
		r = self.transitions.gather_recalculations ()
		logger.info ( "Gathered recalculations %s" , r )
		self.advance_trackpoints ( r )
		tt = self.transitions.total_transition ()
		logger.info ( "total_transition min now %s (@%s)" , tt.min () , unravel_index ( tt.argmin () , tt.shape ) )
		logger.debug ( "total_transition now %s" , tt )
		logger.info ( "request_count now %s" , self.router.request_count )
	
	def _find_explore_and_hint ( self , budget ):
		from peakfinding import find_peaks_cwt_extended
		# multiplying by the ripeness_multiplier opens us up to our peakfinding being manipulated
		# by the user, but yknow.. i think that might be what we want (?)
		augmented_transition_costs = array ( [ tr.matrix[pin_left,pin_right] * ( tr.left_trackpoint.ripeness_multiplier + tr.right_trackpoint.ripeness_multiplier ) for tr , pin_left , pin_right in self.transitions.collapse_to_transitions () ] , dtype = "f4" )
		peak_indexes = find_peaks_cwt_extended ( augmented_transition_costs , array((1,4)) , min_length = 2 , min_snr = 10 , noise_perc = 20 , window_size = 48 )
		
		peaks = [ (i,augmented_transition_costs[i]) for i in peak_indexes ]
		peaks.sort ( key = lambda (i,v) : v , reverse = True )
		
		logger.info ( "Found potential hint peaks %s" , peaks )
		
		hinted_set = set()
		
		for peak_index , peak_value in peaks:
			if peak_index in hinted_set:
				logger.info ( "Skipping peak at transition index %s as it seems to have already been covered by a hint" , peak_index )
				continue
			
			logger.info ( "Exploring hints for transition index %s" % peak_index )
			eh_result = self.transitions.explore_and_hint ( peak_index )
			if eh_result is not None:
				hinted_start_index , hinted_end_index = eh_result
			
				# make note of indexes that touched
				hinted_set.update ( xrange ( hinted_start_index , hinted_end_index ) )
			
			if self.router.request_count > budget:
				logger.info ( "Giving up early on hinting as we seem to have hit our budget allocation (request_count = %s)" , self.router.request_count )
				break
	
	def solve ( self , budget ):
		initial_request_count = self.router.request_count
		
		logger.info ( "Starting scatter solving" )
		while self.router.request_count < initial_request_count + ( budget / 3 ):
			self._iterate_scatter_solve ()
		
		logger.info ( "Looking for hints" )
		try:
			self._find_explore_and_hint ( initial_request_count + ( budget / 2 ) )
		except HintingPointlessError:
			logger.info ( "caught HintingPointlessError" )
		
		logger.info ( "Resuming scatter solving" )
		while self.router.request_count < initial_request_count + ( (budget * 2) / 3 ):
			self._iterate_scatter_solve ()
		
		logger.info ( "Looking for hints" )
		try:
			self._find_explore_and_hint ( initial_request_count + ( (budget * 5) / 6 ) )
		except HintingPointlessError:
			logger.info ( "caught HintingPointlessError" )
		
		logger.info ( "Resuming scatter solving" )
		while self.router.request_count < initial_request_count + budget:
			self._iterate_scatter_solve ()
	
class TransitionList ( list ):
	debug = False
	_hinting_score_threshold = 1.1
	
	def __init__ ( self , routable_track ):
		from random import Random
		from os import urandom
		super ( TransitionList , self ).__init__ ()
		self.routable_track = routable_track # ideally this should be a weakref, but weakrefs have trouble being pickled
		for tp_left , tp_right in iterwindow ( routable_track , 2 , partials = False ):
			self.append ( Transition ( tp_left , tp_right ) )
		
		seed = hash ( urandom ( 8 ) )
		logger.info ( "Initializing TransitionList Random instance with seed %s" , seed )
		self.random = Random ( seed )
		
		try:
			self.refresh_structure ()
		except UnsolvableRouteError:
			# we'll defer the caller finding this out till they start trying to solve
			pass
	
	def refresh_structure ( self ):
		self._pyramid_tip = TransitionBucket ( 0 , len ( self ) , self )
		self._auxiliary_hinting_pyramid_tip = AsymmetricTransitionBucket ( 0 , len ( self ) , self )
		
		for i , tp in enumerate ( self.routable_track ):
			tp.dirty_listeners[self._pyramid_tip] = i
			tp.dirty_listeners[self._auxiliary_hinting_pyramid_tip] = i
	
	def remove_dirty_listeners_from_trackpoint ( self , trackpoint ):
		trackpoint.dirty_listeners.pop ( self._pyramid_tip , None )
		trackpoint.dirty_listeners.pop ( self._auxiliary_hinting_pyramid_tip , None )
	
	def _get_pyramid_tip ( self ):
		try:
			if self.__pyramid_tip is None:
				raise UnsolvableRouteError
			
			return self.__pyramid_tip
		except AttributeError:
			raise UnsolvableRouteError
	
	def _set_pyramid_tip ( self , new_pyramid_tip ):
		self.__pyramid_tip = new_pyramid_tip
	
	def _del_pyramid_tip ( self ):
		self.__pyramid_tip = None
	
	_pyramid_tip = property ( _get_pyramid_tip , _set_pyramid_tip , _del_pyramid_tip )
	
	def _get_auxiliary_hinting_pyramid_tip ( self ):
		try:
			if self.__auxiliary_hinting_pyramid_tip is None:
				raise UnsolvableRouteError
			
			return self.__auxiliary_hinting_pyramid_tip
		except AttributeError:
			raise UnsolvableRouteError
	
	def _set_auxiliary_hinting_pyramid_tip ( self , new_auxiliary_hinting_pyramid_tip ):
		self.__auxiliary_hinting_pyramid_tip = new_auxiliary_hinting_pyramid_tip
	
	def _del_auxiliary_hinting_pyramid_tip ( self ):
		self.__auxiliary_hinting_pyramid_tip = None
	
	_auxiliary_hinting_pyramid_tip = property ( _get_auxiliary_hinting_pyramid_tip , _set_auxiliary_hinting_pyramid_tip , _del_auxiliary_hinting_pyramid_tip )
	
	# don't want this to behave *that* much like a list
	def __hash__ ( self ):
		return id ( self )
	
	def total_transition ( self ):
		return self._pyramid_tip.matrix
	
	def collapse_to_pows ( self ):
		matrix = self._pyramid_tip.matrix
		pin_left , pin_right = unravel_index ( matrix.argmin () , matrix.shape )
		
		# we're also responsible for appending the rightmost pin as all the rest will be some other transition's
		# left trackpoint
		for p in chain ( self._pyramid_tip.collapse ( pin_left , pin_right , to_transition = False ) , ( self.routable_track[-1].calculated_candidates[pin_right] ,) ):
			yield p
	
	def collapse_to_transitions ( self ):
		matrix = self._pyramid_tip.matrix
		pin_left , pin_right = unravel_index ( matrix.argmin () , matrix.shape )
		
		for p in self._pyramid_tip.collapse ( pin_left , pin_right , to_transition = True ):
			yield p
	
	def collapse_to_geojson_dict ( self ):
		from tools import iterwindow
		return { "type" : "FeatureCollection" , "features" : [ transition.left_trackpoint.calculated_candidates[left_ci].geojson_dict_to ( transition.right_trackpoint.calculated_candidates[right_ci] , extra_properties = { "atc" : float ( transition.matrix[left_ci,right_ci] ) } ) for transition , left_ci , right_ci in self.collapse_to_transitions () ] }
	
	def collapse_to_linestring_coord_quadruples ( self , simplify_precision = None ):
		"""
			returns generator yielding tuples of:
				xcoord , ycoord , (fractional) trackpoint index , connected to previous point (bool)
		"""
		from tools import iterwindow , iterinlineexceptions
		from decorators import passinlineexceptions
		from django.contrib.gis.geos import LineString
		from math import hypot
		
		for i , a in enumerate ( pool.imap ( passinlineexceptions ( lambda ( transition , left_ci , right_ci ): ( transition , left_ci , right_ci , tuple ( transition.left_trackpoint.calculated_candidates[left_ci].iterpoints_to ( transition.right_trackpoint.calculated_candidates[right_ci] ) ) ) ) , iterinlineexceptions ( self.collapse_to_transitions () ) ) ):
			if isinstance ( a , Exception ):
				# seems this was actually an exception raised in a greenthread - reraise
				raise a
			
			transition , left_ci , right_ci , point_tuple = a
			
			try:
				linestr = LineString ( point_tuple , srid = 4326 )
			except TypeError:
				# no geometry from the router it seems - route must be impossible - output a "straight" line but mark as unconnected
				
				if i == 0:
					# just in case this was the first point
					left_pow = transition.left_trackpoint.calculated_candidates[left_ci]
					yield left_pow.nearest_point.coords[0] , left_pow.nearest_point.coords[1] , 0 , True
				
				right_pow = transition.right_trackpoint.calculated_candidates[right_ci]
				yield right_pow.nearest_point.coords[0] , right_pow.nearest_point.coords[1] , i+1 , False
			else:
				linestr.transform ( 900913 )
				if simplify_precision is not None:
					linestr.simplify ( simplify_precision / transition.left_trackpoint.local_scale_factor )
				
				distances = [ 0 ]
				distance_accum = 0.0
				for coord_a , coord_b in iterwindow ( linestr.coords , 2 ):
					distance_accum += hypot ( coord_a[0] - coord_b[0] , coord_a[1] - coord_b[1] )
					distances.append ( distance_accum )
				
				point_iter = izip ( distances , linestr.coords )
				if i == 0:
					d , ( x , y ) = point_iter.next ()
					yield x , y , 0 , True
				else:
					# we just want to skip the first one as it will be included as the last point of the previous linestring
					point_iter.next ()
				
				if distance_accum == 0:
					yield linestr.coords[-1][0] , linestr.coords[-1][1] , i+1 , True
				else:
					for d , ( x , y ) in point_iter:
						yield x , y , i + (d/distances[-1]) , True
	
	def gather_recalculations ( self ):
		if self._pyramid_tip.ripeness == 0:
			raise ZeroRipenessError
		
		# visit_for_recalculations will traverse down the pyramid following highest "weight"
		# returning a sequence of tuples corresponding to each pyramid level, top first
		#
		# this should help us determine from how "high" it is best to cast most of our recalculations
		q = self._pyramid_tip.visit_for_recalculations ()
		cumulative_weights = []
		running_total = 0
		for tb , w in q:
			running_total += w
			cumulative_weights.append ( running_total )
		
		return set ( ( q[bisect_left ( cumulative_weights , self.random.random () * running_total )][0].cast_recalculation ( self.random.random () ) for i in xrange ( len ( cumulative_weights ) ) ) )
	
	def transition_matrix_from_to ( self , from_transition_index , to_transition_index ):
		if from_transition_index >= to_transition_index:
			raise ValueError
		return reduce ( _matrix_pseudo_multiplication , imap ( lambda transition : transition.matrix , self._pyramid_tip.iter_covered_transition ( from_transition_index , to_transition_index ) ) )
	
	def explore_and_hint ( self , target_transition_index ):
		ctt = tuple ( self.collapse_to_transitions () )
		
		highest_scoring_tb , highest_scoring_tb_score = self._pyramid_tip , 0.0
		for tb , score , collapsed_time , collapsed_distance in chain ( self._pyramid_tip.visit_for_hint_exploration ( target_transition_index , ctt ) , self._auxiliary_hinting_pyramid_tip.visit_for_hint_exploration ( target_transition_index , ctt ) ):
			logger.debug ( "Hint score for %s: %s" , tb , score )
			if score > highest_scoring_tb_score:
				highest_scoring_tb , highest_scoring_tb_score = tb , score
		
		if highest_scoring_tb_score > self._hinting_score_threshold:
			logger.info ( "Applying hint to %s" , highest_scoring_tb )
			highest_scoring_tb.apply_hint_osm_way_id_set ()
			return highest_scoring_tb._start_transition_index , highest_scoring_tb._end_transition_index
		else:
			logger.info ( "Swallowing hint for %s: score %s, threshold %s" , highest_scoring_tb , highest_scoring_tb_score , self._hinting_score_threshold )
			return None
	

class BaseTransition ( object ):
	matrix = array ( [[]] , dtype = "f4" )
	
	position_aware = False
	
	# i cannot for the life of me remember why this is 0.5. think it's a heuristic that just happens to work.
	_visit_for_recalculations_base_chain_weight = 0.5
	
	@property
	def cost ( self ):
		return self.matrix.min ()
	
	@property
	def matrix ( self ):
		return self.matrix_and_ripeness[0]
	
	@property
	def ripeness ( self ):
		return self.matrix_and_ripeness[1]
	
	@property
	def matrix_and_ripeness ( self ):
		raise NotImplementedError
	
	def iterlevel ( self , level ):
		if level < 0:
			raise ValueError
		elif level == 0:
			yield self
		else:
			for x in chain ( self._left_child.iterlevel ( level - 1 ) , self._right_child.iterlevel ( level - 1 ) ):
				yield x

class TransitionBucket ( BaseTransition ):
	position_aware = True
	
	def __new__ ( cls , start_transition_index , end_transition_index , transition_list , odd = False , dirty_indexes = None ):
		transition_index_range = end_transition_index - start_transition_index
		if not transition_index_range:
			# this ain't possible - don't return a TransitionBucket
			return None
		if transition_index_range == 1:
			# don't actually return a TransitionBucket at all - return the actual Transition
			return transition_list[start_transition_index]
		else:
			return BaseTransition.__new__ ( cls )
	
	def __getnewargs__ ( self ):
		# allow ourselves to be unpickled properly
		return self._start_transition_index , self._end_transition_index , self._transition_list
	
	def _get_left_span ( self ):
		"""
			Separate for easy overloading
		"""
		span = self._end_transition_index - self._start_transition_index
		minor_span = min ( span-1 , max ( 1 , span // 2 ) )
		
		return minor_span if not self._odd else (span - minor_span)
	
	def __init__ ( self , start_transition_index , end_transition_index , transition_list , odd = False , dirty_indexes = None ):
		self._start_transition_index = start_transition_index
		self._end_transition_index = end_transition_index
		self._transition_list = transition_list # ideally this should be a weakref, but weakrefs have trouble being pickled
		self._odd = odd
		# a dirty_indexes argument of None means we should start our own and refer our children to it
		self._dirty_indexes = dirty_indexes if dirty_indexes is not None else []
		
		left_span = self._get_left_span ()
		
		self._left_child = type ( self ) ( self._start_transition_index , self._start_transition_index + left_span , transition_list , odd = not self._odd , dirty_indexes = self._dirty_indexes )
		self._right_child = type ( self ) ( self._start_transition_index + left_span , self._end_transition_index , transition_list , odd = not self._odd , dirty_indexes = self._dirty_indexes )
	
	def _clear_dirty_index ( self , index ):
		candidate_index_index = bisect_left ( self._dirty_indexes , index )
		while candidate_index_index < len ( self._dirty_indexes ) and self._dirty_indexes[candidate_index_index] == index:
			del self._dirty_indexes[candidate_index_index]
	
	def mark_dirty_trackpoint ( self , tp_index ):
		if tp_index != 0:
			#if self.debug:
				#print "marking (left) transition %s dirty" % ( tp_index - 1 )
			# mark left adjacent transition
			insort_left ( self._dirty_indexes , tp_index - 1 )
		if tp_index < len ( self._transition_list ):
			#if self.debug:
				#print "marking (right) transition %s dirty" % ( tp_index )
			# mark right adjacent transition
			insort_left ( self._dirty_indexes , tp_index )
	
	def visit_for_recalculations ( self ):
		this_tuple = ( self , log ( self.cost / ( self._left_child.cost + self._right_child.cost ) ) + self._visit_for_recalculations_base_chain_weight )
		
		if logger.isEnabledFor ( logging.DEBUG ):
			logger.debug ( "visiting %s" , self )
			logger.debug ( "\tballooning is %s (%s)" , log ( self.cost / ( self._left_child.cost + self._right_child.cost ) ) , ( self.cost - ( self._left_child.cost + self._right_child.cost ) ) )
			logger.debug ( "\tleft %s" , self._left_child )
			logger.debug ( "\tright %s" , self._right_child )
		
		if self._left_child.cost * self._left_child.ripeness > self._right_child.cost * self._right_child.ripeness:
			logger.debug ( "\tgoing left" )
			return (this_tuple,) + self._left_child.visit_for_recalculations ()
		else:
			
			logger.debug ( "\tgoing right" )
			return (this_tuple,) + self._right_child.visit_for_recalculations ()
	
	def cast_recalculation ( self , fractional_point ):
		# public entry point for _cast_recalculation
		return self._start_transition_index + self._cast_recalculation ( fractional_point * self.ripeness )
	
	def _cast_recalculation ( self , cumulative_ripeness_point ):
		left_child_ripeness = self._left_child.ripeness
		if cumulative_ripeness_point < left_child_ripeness:
			return self._left_child._cast_recalculation ( cumulative_ripeness_point )
		else:
			return self._right_child._cast_recalculation ( cumulative_ripeness_point - left_child_ripeness ) + self._get_left_span ()
	
	def collapse ( self , pin_left , pin_right , to_transition = False ):
		left_collapsed_matrix = self._left_child.matrix[pin_left,:]
		right_collapsed_matrix = self._right_child.matrix[:,pin_right]
		
		pin_midpoint = ( left_collapsed_matrix + right_collapsed_matrix ).argmin ()
		
		for p in self._left_child.collapse ( pin_left , pin_midpoint , to_transition = to_transition ):
			yield p
		
		for p in self._right_child.collapse ( pin_midpoint , pin_right , to_transition = to_transition ):
			yield p
	
	@property
	def hint_transitions ( self ):
		if not hasattr ( self , "_hint_transitions" ):
			# simply allowing for 1 normal route HintTransition and 1 alternative route HintTransition here for now
			self._hint_transitions = tuple ( HintTransition ( self.piecewise_length , i , self._transition_list[self._start_transition_index].left_trackpoint , self._transition_list[self._end_transition_index-1].right_trackpoint ) for i in xrange ( 2 ) )
		return self._hint_transitions
	
	def _get_collapsed_hint_candidate_indexes ( self ):
		"""
			Calculates best hint transition path to take across this TransitionBucket given the whole TransitionList
			using standard cost functions of both Transitions and HintTransitions (intermixing the two)
		"""
		# this function is hard to cache as it relies on information from the whole TransitionList, which
		# we have no posession of and no way of knowing about updates.
		
		best_alternative = None
		
		for alternative_route_index , hint_transition in enumerate ( self.hint_transitions ):
			hint_matrix = hint_transition.matrix
			
			left_coalesced_matrix = self._transition_list.transition_matrix_from_to ( 0 , self._start_transition_index ) if self._start_transition_index != 0 else _pseudo_identity_matrix ( hint_matrix.shape[0] )
			right_coalesced_matrix = self._transition_list.transition_matrix_from_to ( self._end_transition_index , len ( self._transition_list ) ) if self._end_transition_index != len ( self._transition_list ) else _pseudo_identity_matrix ( hint_matrix.shape[1] )
			
			mid_right_matrix = _matrix_pseudo_multiplication ( hint_matrix , right_coalesced_matrix )
			total_matrix = _matrix_pseudo_multiplication ( left_coalesced_matrix , mid_right_matrix )
			total_matrix_min = total_matrix.min ()
			
			if best_alternative is None or best_alternative[0] > total_matrix_min:
				pin_absolute_left , pin_absolute_right = unravel_index ( total_matrix.argmin () , total_matrix.shape )
				
				left_collapsed_matrix = left_coalesced_matrix[pin_absolute_left,:]
				mid_right_collapsed_matrix = mid_right_matrix[:,pin_absolute_right]
				
				pin_left = ( left_collapsed_matrix + mid_right_collapsed_matrix ).argmin ()
				
				mid_collapsed_matrix = hint_matrix[pin_left,:]
				right_collapsed_matrix = right_coalesced_matrix[:,pin_absolute_right]
				
				pin_right = ( mid_collapsed_matrix + right_collapsed_matrix ).argmin ()
				
				# these are useful values to be sent back to the caller
				left_cost = left_collapsed_matrix[pin_left]
				mid_cost = hint_transition.raw_matrix[pin_left,pin_right]
				right_cost = right_collapsed_matrix[pin_right]
				
				best_alternative = total_matrix_min , pin_left , pin_right , alternative_route_index , left_cost , mid_cost , right_cost
		
		return best_alternative[1:]
	
	def get_hint_osm_way_id_set_and_indexes ( self ):
		best_left_candidate_index , best_right_candidate_index , alternative_route_index , left_cost , mid_cost , right_cost = self._get_collapsed_hint_candidate_indexes ()
		best_left_candidate , best_right_candidate = self._transition_list[self._start_transition_index].left_trackpoint.calculated_candidates[best_left_candidate_index] , self._transition_list[self._end_transition_index-1].right_trackpoint.calculated_candidates[best_right_candidate_index]
		
		return self._transition_list.routable_track.router.way_id_set_from_to ( best_left_candidate.nearest_point , best_left_candidate.powlist.osm_way_id , best_right_candidate.nearest_point , best_right_candidate.powlist.osm_way_id , alternative_index = alternative_route_index ) , ( best_left_candidate_index , best_right_candidate_index ) , alternative_route_index
	
	def get_hint_geojson_dict ( self ):
		best_left_candidate_index , best_right_candidate_index , alternative_route_index , left_cost , mid_cost , right_cost = self._get_collapsed_hint_candidate_indexes ()
		best_left_candidate , best_right_candidate = self._transition_list[self._start_transition_index].left_trackpoint.calculated_candidates[best_left_candidate_index] , self._transition_list[self._end_transition_index-1].right_trackpoint.calculated_candidates[best_right_candidate_index]
		
		return best_left_candidate.geojson_dict_to ( best_right_candidate , with_alternatives = True , alternative_index = alternative_route_index )
	
	def apply_hint_osm_way_id_set ( self ):
		"""
			Applies the osm way id set of the most appropriate hint across bucket to all trackpoints in bucket's range
		"""
		osm_way_id_set , ( best_left_candidate_index , best_right_candidate_index ) , alternative_route_index = self.get_hint_osm_way_id_set_and_indexes ()
		
		logger.info ( "Applying hint with alternative_route_index = %s , best_left_candidate_index = %s , best_right_candidate_index = %s" , alternative_route_index , best_left_candidate_index , best_right_candidate_index )
		
		# penalize this so we're slightly less likely to repeat ourselves in future hints
		self.hint_transitions[alternative_route_index].already_hinted_shim_matrix[best_left_candidate_index,best_right_candidate_index] = _hint_route_already_hinted_penalty
		
		for i in xrange ( self._start_transition_index , self._end_transition_index + 1 ):
			self._transition_list.routable_track[i].apply_hint_set ( osm_way_id_set , reversed ( self._transition_list.routable_track[:i] ) )
	
	def iter_transitions_with_depth ( self , depth = 0 ):
		"""
			This is just a useful method for debugging
		"""
		try:
			left_child_iterable = self._left_child.iter_transitions_with_depth ( depth+1 )
		except AttributeError:
			left_child_iterable = ( ( self._left_child , depth ) ,)
		
		try:
			right_child_iterable = self._right_child.iter_transitions_with_depth ( depth+1 )
		except AttributeError:
			right_child_iterable = ( ( self._right_child , depth ) ,)
		
		for x in chain ( left_child_iterable , right_child_iterable ):
			yield x
	
	def iter_covered_transition ( self , from_transition_index , to_transition_index ):
		"""
			Returns a generator yielding a mixture of TransitionBuckets and Transitions to cover the
			part of the interval @from_transition_index to @to_transition_index that can be covered by
			this TransitionBucket
		"""
		if from_transition_index >= self._end_transition_index or to_transition_index <= self._start_transition_index:
			# we have nothing for you
			return
		elif from_transition_index <= self._start_transition_index and to_transition_index >= self._end_transition_index:
			# we're completely covered
			yield self
		else:
			# we're partially covered - we'll have to ask our children
			if self._left_child.position_aware:
				left_child_iterable = self._left_child.iter_covered_transition ( from_transition_index , to_transition_index )
			else:
				# this is generally only possible on a boundary condition, so check if one of the boundaries matches our left transition
				left_child_iterable = (self._left_child,) if self._start_transition_index >= from_transition_index and self._start_transition_index < to_transition_index else ()
			
			if self._right_child.position_aware:
				right_child_iterable = self._right_child.iter_covered_transition ( from_transition_index , to_transition_index )
			else:
				# this is generally only possible on a boundary condition, so check if one of the boundaries matches our right transition
				right_child_iterable = (self._right_child,) if self._end_transition_index <= to_transition_index and self._end_transition_index > from_transition_index else ()
			
			for x in chain ( left_child_iterable , right_child_iterable ):
				yield x
	
	def _collapsed_time_distance ( self , collapsed_transition_tuples , start_transition_index = None , end_transition_index = None ):
		if start_transition_index is None:
			start_transition_index = self._start_transition_index
		if end_transition_index is None:
			end_transition_index = self._end_transition_index
		
		time_sum , distance_sum = 0 , 0
		for transition , left_ci , right_ci in collapsed_transition_tuples[start_transition_index:end_transition_index]:
			time_sum += transition.raw_time_matrix[left_ci,right_ci]
			distance_sum += transition.raw_distance_matrix[left_ci,right_ci]
		return time_sum , distance_sum
	
	def visit_for_hint_exploration ( self , target_transition_index , collapsed_transition_tuples ):
		assert self._start_transition_index <= target_transition_index < self._end_transition_index
		
		# we want to keep a note of the next immediate downstream yielded value to avoid recalculating collapsed_time_distance from scratch
		last_downstream = None
		
		if hasattr ( self._left_child , "_end_transition_index" ) and self._left_child._end_transition_index > target_transition_index:
			downstream_left = True
			for t in self._left_child.visit_for_hint_exploration ( target_transition_index , collapsed_transition_tuples ):
				last_downstream = t
				yield t
		elif hasattr ( self._right_child , "_start_transition_index" ) and self._right_child._start_transition_index <= target_transition_index:
			downstream_left = False
			for t in self._right_child.visit_for_hint_exploration ( target_transition_index , collapsed_transition_tuples ):
				last_downstream = t
				yield t
		
		if last_downstream is not None:
			last_downstream_tb , last_downstream_score , last_downstream_collapsed_time , last_downstream_collapsed_distance = last_downstream
		
		# choose the best hint route to take using standard costs...
		hint_pin_left , hint_pin_right , alternative_route_index , left_cost , mid_cost , right_cost = self._get_collapsed_hint_candidate_indexes ()
		
		# ...but _comparing_ the chosen HintTransition routes and underlying Transition-based routes to see how much of a saving we make
		# can't be done simply using their standard costs as the two are calculated so differently. for a start one includes
		# intrinsic_costs for each trackpoint. also the nonlinearities in the cost functions will cause a summation of Transition
		# transition costs to not really work as desired. so we need to sum the total time , distance of the underlying Transition-based
		# route and apply HintTransition's cost function to those, so we're at least attempting to compare like with like.
		
		# if we have two child buckets, calculate the collapsed_time_distance by adding the _other_ bucket's collapsed_time_distance and adding it to last_downstream_collapsed_time/distance.
		# otherwise just calculate our own _collapsed_time_distance ().
		try:
			collapsed_time , collapsed_distance = ( self._right_child if downstream_left else self._left_child )._collapsed_time_distance ( collapsed_transition_tuples )
			
			collapsed_time += last_downstream_collapsed_time
			collapsed_distance += last_downstream_collapsed_distance
		except ( AttributeError , UnboundLocalError ):
			collapsed_time , collapsed_distance = self._collapsed_time_distance ( collapsed_transition_tuples )
		
		hint_transition = self.hint_transitions[alternative_route_index]
		t , l , r = collapsed_transition_tuples[self._start_transition_index]
		ctt_left_pow = t.left_trackpoint.calculated_candidates[l]
		t , l , r = collapsed_transition_tuples[self._end_transition_index-1]
		ctt_right_pow = t.right_trackpoint.calculated_candidates[r]
		
		inner_score = max ( hint_transition.time_transition_cost ( collapsed_time , ctt_left_pow , ctt_right_pow ) * hint_transition.distance_transition_cost ( collapsed_distance , ctt_left_pow , ctt_right_pow ) , 1.e-8 ) / max ( mid_cost , 1.e-8 )
		outer_score = left_cost + right_cost
		if outer_score:
			outer_score = max ( sum ( transition.matrix[left_ci,right_ci] for transition , left_ci , right_ci in chain ( collapsed_transition_tuples[:self._start_transition_index] , collapsed_transition_tuples[self._end_transition_index:] ) ) , 1.e-8 ) / outer_score
		else:
			outer_score = 1.0
		
		# put both scores through a sigmoid function (atan) before combining them to reduce influence
		# of extreme values (which would otherwise often happen at smaller scales, creating scores of
		# unbeatable magnitude).
		# using the logarithm so we have a nice linearized representation of the ratios we've just
		# calculated for use against the sigmoid function
		inner_score , outer_score = ( atan ( log ( inner_score ) * 0.5 ) / pi ) + 0.5 , ( atan ( log ( outer_score ) * 0.5 ) / pi ) + 0.5
		# adding rather than multiplying as they're supposedly logarithms now (albeit mangled ones)
		score = inner_score + outer_score
		
		logger.debug ( "%s score = %s ( inner score = %s , outer_score = %s)" , self , score , inner_score , outer_score )
		
		yield self , score , collapsed_time , collapsed_distance
	
	@property
	def _covers_dirty_indexes ( self ):
		candidate_index_index = bisect_left ( self._dirty_indexes , self._end_transition_index )
		return candidate_index_index != 0 and self._dirty_indexes[candidate_index_index-1] >= self._start_transition_index
	
	@property
	def piecewise_length ( self ):
		if not hasattr ( self , "_piecewise_length" ):
			self._piecewise_length = self._left_child.piecewise_length + self._right_child.piecewise_length
		return self._piecewise_length
	
	@property
	def matrix_and_ripeness ( self ):
		if ( not ( hasattr ( self , "_matrix" ) and hasattr ( self , "_ripeness" ) ) ) or self._covers_dirty_indexes:
			left_child_matrix , left_child_ripeness = self._left_child.matrix_and_ripeness
			right_child_matrix , right_child_ripeness = self._right_child.matrix_and_ripeness
			self._matrix = _matrix_pseudo_multiplication ( left_child_matrix , right_child_matrix )
			self._ripeness = left_child_ripeness + right_child_ripeness
			if isinstance ( self._left_child , Transition ):
				self._clear_dirty_index ( self._start_transition_index )
			if isinstance ( self._right_child , Transition ):
				self._clear_dirty_index ( self._end_transition_index-1 )
		return self._matrix , self._ripeness
	
	def __repr__ ( self ):
		return u"<%s[%s,%s),c=%s,r=%s,c*r=%s,bf=%s>" % ( type ( self ).__name__ , self._start_transition_index , self._end_transition_index , self.cost , self.ripeness , self.cost * self.ripeness , self.cost / ( self._left_child.cost + self._right_child.cost ) )

class AsymmetricTransitionBucket ( TransitionBucket ):
	def _get_left_span ( self ):
		span = self._end_transition_index - self._start_transition_index
		minor_span = min ( span-1 , max ( 1 , int ( ceil ( span / 3.0 ) ) ) )
		
		return minor_span if not self._odd else (span - minor_span)

# not used but potentially useful
#class RandomTransitionBucket ( TransitionBucket ):
	#def _get_left_span ( self ):
		#span = self._end_transition_index - self._start_transition_index
		#return min ( span-1 , max ( 1 , int ( self._transition_list.random.gauss ( 0.5 , 0.10 ) * span ) ) )

class Transition ( BaseTransition ):
	_matrix_updated_for_left_trackpoint_user_pinned = _matrix_updated_for_right_trackpoint_user_pinned = None
	
	def __init__ ( self , left_trackpoint , right_trackpoint ):
		self.left_trackpoint = left_trackpoint
		self.right_trackpoint = right_trackpoint
		
		if not self.left_trackpoint.calculated_candidates:
			left_pow = self.left_trackpoint.advance_next_candidate ()
		else:
			left_pow = self.left_trackpoint.calculated_candidates[0]
		
		if not self.right_trackpoint.calculated_candidates:
			right_pow = self.right_trackpoint.advance_next_candidate ()
		else:
			right_pow = self.right_trackpoint.calculated_candidates[0]
		
		# _matrix holds the main overarching cost we operate on - augmented transition costs. i.e. a transition_cost
		# added to the intrinsic_costs of its endpoints.
		self._matrix = array ( [[]] , dtype = "f4" )
		
		# _raw_matrix is a version of the matrix we maintain unmuddied with the intrinsic_costs
		self._raw_matrix = array ( [[]] , dtype = "f4" )
		# _raw_time_matrix & _raw_distance_matrix are versions of the matrix that just hold the raw times/distances from the router. mainly used
		# for hinting, but useful for debugging.
		self._raw_time_matrix = array ( [[]] , dtype = "f4" )
		self._raw_distance_matrix = array ( [[]] , dtype = "f4" )
	
	def cast_recalculation ( self , fractional_point ):
		# public entry point for _cast_recalculation - unlike a TransitionBucket will return index 0-based at
		# Transition's leftmost extreme - TODO should straighten out that behaviour between the two. (though in
		# reality it has no effect at the moment)
		return self._cast_recalculation ( fractional_point * self.ripeness )
	
	def _cast_recalculation ( self , cumulative_ripeness_point ):
		if cumulative_ripeness_point < self.left_trackpoint.ripeness:
			return 0
		else:
			return 1
	
	def visit_for_recalculations ( self ):
		return (( self , self._visit_for_recalculations_base_chain_weight ),)
	
	def collapse ( self , pin_left , pin_right , to_transition = False ):
		if to_transition:
			yield ( self , pin_left , pin_right )
		else:
			yield self.left_trackpoint.calculated_candidates[pin_left]
	
	@property
	def hint_transition ( self , *args , **kwargs ):
		raise HintingPointlessError
	
	def get_hint_osm_way_id_set ( self , *args , **kwargs ):
		raise HintingPointlessError
	
	def get_hint_geojson_dict ( self , *args , **kwargs ):
		raise HintingPointlessError
	
	def apply_hint_osm_way_id_set ( self , *args , **kwargs ):
		raise HintingPointlessError
	
	def visit_for_hint_exploration ( self , *args , **kwargs ):
		raise HintingPointlessError
	
	def iter_covered_transition ( self , from_transition_index , to_transition_index ):
		# again as in cast_recalculation, the indexing business is a bit of a vestigial
		# remainder of this method coming from TransitionBucket and most callers will think
		# they're calling it on a TransitionBucket - should sort out indexing behaviour between
		# the two.
		yield self
	
	@property
	def piecewise_length ( self ):
		if not hasattr ( self , "_piecewise_length" ):
			self._piecewise_length = self.left_trackpoint.point.distance ( self.right_trackpoint.point ) * self.left_trackpoint.local_scale_factor
		return self._piecewise_length
	
	@staticmethod
	def time_transition_cost ( expected_possible_time , left_pow , right_pow ):
		apparent_time = timedelta_total_seconds ( right_pow.powlist.trackpoint.timestamp - left_pow.powlist.trackpoint.timestamp )
		
		# note the protection against division by zero
		time_factor = expected_possible_time / max ( apparent_time , 1.e-3 )
		
		#if time_factor > 1e5:
			#print "unusually high time_factor %s, expected_possible_time = %s , apparent_time = %s" % ( time_factor , expected_possible_time , apparent_time )
		
		# possibilities for upper curve: (((time_factor+4)**2)-23)*0.5 , (((time_factor+3)**2)-14)*0.5 , (((time_factor+2)**2)-7)*0.5
		
		return (((time_factor+0.5)**2)-0.25)*0.5 if time_factor < 1.0 else ( (((time_factor+3)**2)-14)*0.5 * ( 1 + ((expected_possible_time-apparent_time)*0.5) ) )
	
	@staticmethod
	def distance_transition_cost ( expected_possible_distance , left_pow , right_pow ):
		apparent_distance = left_pow.nearest_point.distance ( right_pow.nearest_point ) * left_pow.powlist.trackpoint.local_scale_factor
		# note the protection against division by zero
		return max ( 1.0 , expected_possible_distance / max ( apparent_distance , 1.e-3 ) )**1.5
	
	@staticmethod
	def _seconds_metres_to_wrapper ( left_pow , right_pow ):
		"""
			separate to allow easy overriding
		"""
		return left_pow.seconds_metres_to ( right_pow )
	
	def _pow_pow_cost_tuple ( self , left_pow , right_pow ):
		seconds , metres = self._seconds_metres_to_wrapper ( left_pow , right_pow )
		time_transition_cost = self.time_transition_cost ( seconds , left_pow , right_pow )
		distance_transition_cost = self.distance_transition_cost ( metres , left_pow , right_pow )
		return time_transition_cost * distance_transition_cost , seconds , metres
	
	@property
	def raw_matrices ( self ):
		assert self._raw_matrix.shape == self._raw_time_matrix.shape == self._raw_distance_matrix.shape
		if self._raw_matrix.shape != ( len ( self.left_trackpoint.calculated_candidates ) , len ( self.right_trackpoint.calculated_candidates ) ):
			if len ( self.left_trackpoint.calculated_candidates ) < self._raw_time_matrix.shape[0]:
				raise ValueError
			if len ( self.right_trackpoint.calculated_candidates ) < self._raw_time_matrix.shape[1]:
				raise ValueError
			
			# calculate the missing rows
			missing_rows = pool.imap ( lambda left_pow: pool.imap ( lambda right_pow: self._pow_pow_cost_tuple ( left_pow , right_pow ) , self.right_trackpoint.calculated_candidates[:self._raw_matrix.shape[1]] ) , self.left_trackpoint.calculated_candidates[self._raw_matrix.shape[0]:] )
			
			# calculate the missing columns (including the missing "corner")
			missing_columns = pool.imap ( lambda right_pow: pool.imap ( lambda left_pow: [ self._pow_pow_cost_tuple ( left_pow , right_pow ) ] , self.left_trackpoint.calculated_candidates ) , self.right_trackpoint.calculated_candidates[self._raw_matrix.shape[1]:] )
			
			# insert the missing rows
			for missing_row in missing_rows:
				row_list = list ( missing_row )
				self._raw_matrix = numpy_append ( self._raw_matrix , [ [ tc for tc , s , m in row_list ] ] , axis = 0 )
				self._raw_time_matrix = numpy_append ( self._raw_time_matrix , [ [ s for tc , s , m in row_list ] ] , axis = 0 )
				self._raw_distance_matrix = numpy_append ( self._raw_distance_matrix , [ [ m for tc , s , m in row_list ] ] , axis = 0 )
			
			# insert the missing columns
			for missing_column in missing_columns:
				column_list = list ( missing_column )
				self._raw_matrix = numpy_append ( self._raw_matrix , [ [ tc ] for (( tc , s , m ),) in column_list ] , axis = 1 )
				self._raw_time_matrix = numpy_append ( self._raw_time_matrix , [ [ s ] for (( tc , s , m ),) in column_list ] , axis = 1 )
				self._raw_distance_matrix = numpy_append ( self._raw_distance_matrix , [ [ m ] for (( tc , s , m ),) in column_list ] , axis = 1 )
		
		# a bit of assertion overkill
		assert self._raw_matrix.shape == self._raw_time_matrix.shape == self._raw_distance_matrix.shape
		
		return self._raw_matrix , self._raw_time_matrix , self._raw_distance_matrix
	
	@property
	def matrix ( self ):
		"""
			The main matrix - really just the transition cost added to the intrinsic costs
		"""
		raw_matrix = self.raw_matrix
		if self._matrix.shape != raw_matrix.shape or self._matrix_updated_for_left_trackpoint_user_pinned != self.left_trackpoint.user_pinned or self._matrix_updated_for_right_trackpoint_user_pinned != self.right_trackpoint.user_pinned:
			if raw_matrix.shape[0] < self._matrix.shape[0]:
				raise ValueError
			if raw_matrix.shape[1] < self._matrix.shape[1]:
				raise ValueError
			
			self._matrix = raw_matrix + array ( [ [ self.left_trackpoint.calculated_candidates[left_index].intrinsic_cost + self.right_trackpoint.calculated_candidates[right_index].intrinsic_cost for right_index in xrange ( raw_matrix.shape[1] )  ] for left_index in xrange ( raw_matrix.shape[0] ) ] )
			
			self._matrix_updated_for_left_trackpoint_user_pinned = self.left_trackpoint.user_pinned
			self._matrix_updated_for_right_trackpoint_user_pinned = self.right_trackpoint.user_pinned
		
		# another bit of assertion overkill
		assert self._matrix.shape == raw_matrix.shape
		
		return self._matrix
	
	@property
	def matrix_and_ripeness ( self ):
		return self.matrix , ( self.left_trackpoint.ripeness + self.right_trackpoint.ripeness )
	
	@property
	def raw_matrix ( self ):
		_raw_matrix , _raw_time_matrix , _raw_distance_matrix = self.raw_matrices
		return _raw_matrix
	
	@property
	def raw_time_matrix ( self ):
		_raw_matrix , _raw_time_matrix , _raw_distance_matrix = self.raw_matrices
		return _raw_time_matrix
	
	@property
	def raw_distance_matrix ( self ):
		_raw_matrix , _raw_time_matrix , _raw_distance_matrix = self.raw_matrices
		return _raw_distance_matrix
	
	def __repr__ ( self ):
		return "<T,c=%s,r=%s,c*r=%s>" % ( self.cost , self.ripeness , self.cost * self.ripeness )
	
class HintTransition ( Transition ):
	def __init__ ( self , tb_piecewise_length , alternative_route_index , *args ):
		self._tb_piecewise_length = tb_piecewise_length
		self._already_hinted_shim_matrix = array ( [[]] , dtype = "f4" )
		self._alternative_route_index = alternative_route_index
		return super ( HintTransition , self ).__init__ ( *args )
	
	#
	# lru_cache here should be shared class-wide and thus get the desired behaviour
	#
	# the reason we have this lru_cache here is to share routing calls between "ordinary" hint
	# transitions and "alternative route" hint transitions. both get returned in a single routing
	# call but the way Transitions are designed it's hard to share routing calls between two of
	# them. so in this peculiar case we simply make sure the two instances are sharing a cache
	# and hope the hint transition's alternative route counterpart gets called on to calculate
	# its routes soon enough afterwards that the previous call is still in the lru_cache - it
	# really should be under normal circumstances.
	#
	@staticmethod
	@lru_cache ( maxsize = 128 )
	def _seconds_metres_to_wrapper_inner ( left_pow , right_pow ):
		return left_pow.seconds_metres_to ( right_pow , with_alternatives = True )
	
	def _seconds_metres_to_wrapper ( self , left_pow , right_pow ):
		try:
			return self._seconds_metres_to_wrapper_inner ( left_pow , right_pow )[self._alternative_route_index]
		except IndexError:
			# this alternative doesn't exist
			return 1.e15 , 1.e15
	
	def distance_transition_cost ( self , expected_possible_distance , left_pow , right_pow ):
		# penalize routes that are significantly shorter or longer than  piecewise_length of TransitionBucket
		# we're hinting across - if they're too short they're probably taking a shortcut and/or this isn't an appropriate place
		# to hint
		length_factor = max ( 1.e-3 , expected_possible_distance ) / max ( 1.e-3 , self._tb_piecewise_length )
		return ( length_factor**4 - length_factor**2 + 1 ) / length_factor**2
	
	@property
	def already_hinted_shim_matrix ( self ):
		# ensure shape matches actual matrix - fill out if not
		if self._already_hinted_shim_matrix.shape != self._matrix.shape:
			if self._already_hinted_shim_matrix.shape[0] > self._matrix.shape[0]:
				raise ValueError
			if self._already_hinted_shim_matrix.shape[1] > self._matrix.shape[1]:
				raise ValueError
			
			# add missing rows
			while self._already_hinted_shim_matrix.shape[0] < self._matrix.shape[0]:
				self._already_hinted_shim_matrix = numpy_append ( self._already_hinted_shim_matrix , ( tuple ( (0 if self.left_trackpoint.calculated_candidates[self._already_hinted_shim_matrix.shape[0]].worth_hinting_to ( self.right_trackpoint.calculated_candidates[i] ) else _hint_route_already_hinted_penalty) for i in xrange ( self._already_hinted_shim_matrix.shape[1] ) ) ,) , axis = 0 )
			
			# add missing columns
			while self._already_hinted_shim_matrix.shape[1] < self._matrix.shape[1]:
				self._already_hinted_shim_matrix = numpy_append ( self._already_hinted_shim_matrix , tuple ( (0 if self.left_trackpoint.calculated_candidates[i].worth_hinting_to ( self.right_trackpoint.calculated_candidates[self._already_hinted_shim_matrix.shape[1]] )  else _hint_route_already_hinted_penalty,) for i in xrange ( self._already_hinted_shim_matrix.shape[0] ) ) , axis = 1 )
		
		return self._already_hinted_shim_matrix
	
	@property
	def matrix_and_ripeness ( self ):
		matrix , ripeness = super ( HintTransition , self ).matrix_and_ripeness
		return matrix + self.already_hinted_shim_matrix , ripeness
