from xml.etree.cElementTree import iterparse

from models import initial_mercurial_working_version_id
from router import Router

class GPX ( list ):
	"""
	really just a list of tracks
	"""
	def __init__ ( self , gpxfile , transport , blacklists = None ):
		self.transport = transport
		self.blacklists = blacklists or {}
		self.metadata = {}
		self.initial_mercurial_working_version_id = initial_mercurial_working_version_id
		gpxiter = iter ( iterparse ( gpxfile , events = ("start","end") ) )
		super ( GPX , self ).__init__ ( self._gen_from_gpx ( gpxiter ) )
	
	def _gen_from_gpx ( self , gpxiter ):
		from track import Track
		
		track_index = 0
		
		for event , element in gpxiter:
			if event == "start" and element.tag.endswith ( "}gpx" ):
				gpxelement = element
				for event , element in gpxiter:
					if event == "start" and element.tag.endswith ( "}trk" ):
						yield Track ( element , gpxiter , Router ( self.transport ) , self.transport , blacklist = self.blacklists.get ( track_index , [] ) )
						track_index += 1
					elif event == "end" and element is gpxelement:
						element.clear ()
						break
					elif event == "start" and element.tag.endswith ( "}metadata" ):
						metadataelement = element
						for event , element in gpxiter:
							if event == "end" and element is metadataelement:
								element.clear ()
								break
							elif event == "end" and element.tag.split ( "}" )[-1] in ( "name" , "desc" , "time" , "keywords" ,):
								tagbasename = element.tag.split ( "}" )[-1]
								self.metadata[tagbasename] = element.text
			
					if event == "end":
						# clear any unhandled elements as they're ended
						element.clear ()
	
	@property
	def total_request_count ( self ):
		return sum ( ( track.router.request_count for track in self if hasattr ( track , "router" ) ) )
