from django.conf import settings
from django.utils.datastructures import SortedDict

from itertools import chain
import os.path


TSBP_COLOR_SCHEME = getattr ( settings , "TSBP_COLOR_SCHEME" , SortedDict ( (
	( "primary" , SortedDict ( (
		( "base" , "#A7EA84" ) ,
		( "dull" , "#8AAF76" ) ,
		( "dark" , "#51982B" ) ,
		( "light" , "#C0F4A4" ) ,
		( "verylight" , "#CEF4BA" ) ,
	) ) ) ,
	( "secondary_a" , SortedDict ( (
		( "base" , "#72A8C3" ) ,
		( "dull" , "#658392" ) ,
		( "dark" , "#25617F" ) ,
		( "light" , "#9BCAE1" ) ,
		( "verylight" , "#AED0E1" ) ,
	) ) ) ,
	( "secondary_b" , SortedDict ( (
		( "base" , "#FFCB90" ) ,
		( "dull" , "#BFA281" ) ,
		( "dark" , "#A66E2F" ) ,
		( "light" , "#FFD8AB" ) ,
		( "verylight" , "#FFE3C2" ) ,
	) ) ) ,
	( "complementary" , SortedDict ( (
		( "base" , "#F3899F" ) ,
		( "dull" , "#B67A87" ) ,
		( "dark" , "#9E2C45" ) ,
		( "light" , "#F9A7B9" ) ,
		( "verylight" , "#F9BDCA" ) ,
	) ) ) ,
) ) )

TSBP_COST_COLOR_NAME = getattr ( settings , "TSBP_COST_COLOR_NAME" , "complementary" )

TSBP_COST_COLOR = getattr ( settings , "TSBP_COST_COLOR" , TSBP_COLOR_SCHEME[TSBP_COST_COLOR_NAME] )

TSBP_NOCOST_COLOR_NAME = getattr ( settings , "TSBP_NOCOST_COLOR_NAME" , "nocost" )

TSBP_NOCOST_COLOR = getattr ( settings , "TSBP_NOCOST_COLOR" , TSBP_COLOR_SCHEME.get ( TSBP_NOCOST_COLOR_NAME , { "base" : "#888" } ) )

TSBP_TRACK_COLORS = getattr ( settings , "TSBP_TRACK_COLORS" , None )

if TSBP_TRACK_COLORS is None:
	TSBP_TRACK_COLORS = {
		"names" : [] ,
		"colors" : [] ,
	}
	
	_initial_list = list ( chain.from_iterable ( ( ( ( j , i , "%s_%s" % ( color_name , variant_name ) , color ) for j , ( variant_name , color ) in enumerate ( color_dict.items () ) ) for i , ( color_name , color_dict ) in enumerate ( TSBP_COLOR_SCHEME.items () ) if color_name != TSBP_COST_COLOR_NAME ) ) )
	_initial_list.sort ()
	
	for _ , _ , name , color in _initial_list:
		TSBP_TRACK_COLORS["names"].append ( name )
		TSBP_TRACK_COLORS["colors"].append ( color )

TSBP_OSMOSIS_BINARY = getattr ( settings , "TSBP_OSMOSIS_BINARY" , "osmosis" )

TSBP_OSM2PGSQL_BINARY = getattr ( settings , "TSBP_OSMOSIS_BINARY" , "osm2pgsql" )

TSBP_DAEMONIZE_BINARY = getattr ( settings , "TSBP_DAEMONIZE_BINARY" , "daemonize" )

import tsbp
TSBP_OSM2PGSQL_STYLE = os.path.join ( os.path.dirname ( tsbp.__file__ ) , "osm2pgsql" , "default.style" )

TSBP_REPLICATION_CURRENT_SEQUENCE_NAMESPACE = getattr ( settings , "TSBP_REPLICATION_CURRENT_SEQUENCE_NAMESPACE" , "" )
