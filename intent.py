"""
	An approximate mirror of intent.js
"""

INTENT = {
	"excluded" : 0 ,
	"tryhard" : 1 ,
	"try" : 2 ,
	"base" : 3 ,
	"untry" : 4 ,
	"untryhard" : 5 ,
	"ignoretryhard" : 9 ,
	"ignoretry" : 10 ,
	"ignorebase" : 11 ,
	"ignoreuntry" : 12 ,
	"ignoreuntryhard" : 13 ,
}

def intent_is_ignored ( intent ):
	return bool ( (1<<3) & intent )

def intent_to_weight ( intent ):
	intent = intent & 0x07
	if intent == 1:
		return 2
	elif intent == 2:
		return 1
	elif intent == 3:
		return 0
	elif intent == 4:
		return -1
	elif intent == 5:
		return -2
