(function ()
{

var _intent = window.tsbp.intent = {
	"excluded" : 0 ,
	"tryhard" : 1 ,
	"try_" : 2 ,
	"base" : 3 ,
	"untry" : 4 ,
	"untryhard" : 5 ,
	"ignoretryhard" : 9 ,
	"ignoretry" : 10 ,
	"ignorebase" : 11 ,
	"ignoreuntry" : 12 ,
	"ignoreuntryhard" : 13
};

var _intent_class = window.tsbp.intent_class = new Array(14);
$.each ( _intent , function ( k , v )
{
	_intent_class[v] = "_intent_" + k;
} );
_intent_class[_intent.try_] = "_intent_try";

var _intent_explanation = window.tsbp.intent_explanation = [];
_intent_explanation[_intent.excluded] = "has been ignored in this solution";
_intent_explanation[_intent.tryhard] = "will be given super extra attention when recalculated";
_intent_explanation[_intent.try_] = "will be given extra attention when recalculated";
_intent_explanation[_intent.base] = "is being treated normally";
_intent_explanation[_intent.untry] = "will be given less attention when recalculated";
_intent_explanation[_intent.untryhard] = "will be given even less attention when recalculated";
_intent_explanation[_intent.ignoretryhard] = "will be ignored in the next recalculation";
_intent_explanation[_intent.ignoretry] = _intent_explanation[_intent.ignoretryhard];
_intent_explanation[_intent.ignorebase] = _intent_explanation[_intent.ignoretryhard];
_intent_explanation[_intent.ignoreuntry] = _intent_explanation[_intent.ignoretryhard];
_intent_explanation[_intent.ignoreuntryhard] = _intent_explanation[_intent.ignoretryhard];

var _intent_explanation_plural = window.tsbp.intent_explanation_plural = window.tsbp.intent_explanation.slice ();
_intent_explanation_plural[_intent.excluded] = "have been ignored in this solution";



var intent_is_ignored = window.tsbp.intent_is_ignored = function ( intent )
{
	return !!((1<<3) & intent);
};

var transition = window.tsbp.intent_transition = {
	"try_" : 0 ,
	"untry" : 1 ,
	"ignore" : 2 ,
	"unignore" : 3
};

var tm = window.tsbp.intent_transition_matrix = new Array ( 14 );

tm[_intent.excluded] = [
	_intent.excluded ,
	_intent.excluded ,
	_intent.excluded ,
	_intent.excluded
];

tm[_intent.tryhard] = [
	_intent.tryhard ,
	_intent.try_ ,
	_intent.ignoretryhard ,
	_intent.tryhard
];

tm[_intent.try_] = [
	_intent.tryhard ,
	_intent.base ,
	_intent.ignoretry ,
	_intent.try_
];

tm[_intent.base] = [
	_intent.try_ ,
	_intent.untry ,
	_intent.ignorebase ,
	_intent.base
];

tm[_intent.untry] = [
	_intent.base ,
	_intent.untryhard ,
	_intent.ignoreuntry ,
	_intent.untry
];

tm[_intent.untryhard] = [
	_intent.untry ,
	_intent.untryhard ,
	_intent.ignoreuntryhard ,
	_intent.untryhard
];


tm[_intent.ignoretryhard] = [
	_intent.ignoretryhard ,
	_intent.ignoretry ,
	_intent.ignoretryhard ,
	_intent.tryhard
];

tm[_intent.ignoretry] = [
	_intent.ignoretryhard ,
	_intent.ignorebase ,
	_intent.ignoretry ,
	_intent.try_
];

tm[_intent.ignorebase] = [
	_intent.ignoretry ,
	_intent.ignoreuntry ,
	_intent.ignorebase ,
	_intent.base
];

tm[_intent.ignoreuntry] = [
	_intent.ignorebase ,
	_intent.ignoreuntryhard ,
	_intent.ignoreuntry ,
	_intent.untry
];

tm[_intent.ignoreuntryhard] = [
	_intent.ignoreuntry ,
	_intent.ignoreuntryhard ,
	_intent.ignoreuntryhard ,
	_intent.untryhard
];


//
// We're also going to keep a "cosmetic" version of the matrix, used for deciding which menu
// options to show etc, which will also hide some operations for clarity.
//

var ctm = window.tsbp.intent_cosmetic_transition_matrix = $.extend ( true , [] , tm );

ctm[_intent.ignoretryhard] = [
	_intent.ignoretryhard ,
	_intent.ignoretryhard ,
	_intent.ignoretryhard ,
	_intent.tryhard
];

ctm[_intent.ignoretry] = [
	_intent.ignoretry ,
	_intent.ignoretry ,
	_intent.ignoretry ,
	_intent.try_
];

ctm[_intent.ignorebase] = [
	_intent.ignorebase ,
	_intent.ignorebase ,
	_intent.ignorebase ,
	_intent.base
];

ctm[_intent.ignoreuntry] = [
	_intent.ignoreuntry ,
	_intent.ignoreuntry ,
	_intent.ignoreuntry ,
	_intent.untry
];

ctm[_intent.ignoreuntryhard] = [
	_intent.ignoreuntryhard ,
	_intent.ignoreuntryhard ,
	_intent.ignoreuntryhard ,
	_intent.untryhard
];


var intent_emphasized_color = "rgba(0,0,0,0.07)";
var intent_emphasized_hard_color = "rgba(0,0,0,0.13)";
var intent_ignored_color = $.color.parse(window.TSBP_COST_COLOR.base).scale("a",0.3).toString ();

var buildstripe_canvas = function ( color )
{
	var canvas = document.createElement ( "canvas" );
	canvas.width = canvas.height = 32;

	ctx = canvas.getContext ( "2d" );

	ctx.fillStyle = color;
	ctx.beginPath ();
		ctx.moveTo ( 0 , 0 );
		ctx.lineTo ( 8 , 0 );
		ctx.lineTo ( 32 , 24 );
		ctx.lineTo ( 32 , 32 );
		ctx.lineTo ( 24 , 32 );
		ctx.lineTo ( 0 , 8 );
		ctx.lineTo ( 0 , 0 );
		ctx.moveTo ( 24 , 0 );
		ctx.lineTo ( 32 , 0 );
		ctx.lineTo ( 32 , 8 );
		ctx.lineTo ( 24 , 0 );
		ctx.moveTo ( 0 , 24 );
		ctx.lineTo ( 8 , 32 );
		ctx.lineTo ( 0 , 32 );
		ctx.lineTo ( 0 , 24 );
	ctx.fill ();

	return canvas;
};

window.tsbp.intent_fill_style = function ()
{
	// wrapping these private canvases up in a closure
	var emphasized_stripe_canvas = buildstripe_canvas ( intent_emphasized_color );
	var emphasized_hard_stripe_canvas = buildstripe_canvas ( intent_emphasized_hard_color );
	var ignored_stripe_canvas = buildstripe_canvas ( intent_ignored_color );

	return function ( ctx , intent )
	{
		if ( intent_is_ignored ( intent ) )
			return ctx.createPattern ( ignored_stripe_canvas , "repeat" );
		else if ( intent === _intent.try_ )
			return intent_emphasized_color;
		else if ( intent === _intent.untry )
			return ctx.createPattern ( emphasized_stripe_canvas , "repeat" );
		else if ( intent === _intent.tryhard )
			return intent_emphasized_hard_color;
		else if ( intent === _intent.untryhard )
			return ctx.createPattern ( emphasized_hard_stripe_canvas , "repeat" );
	
		// else null
	};
} ();

window.tsbp.update_intent_listener = function ( target )
{
	// add new class from target's intent
	var intent_class = tsbp.intent_class[target.data ( "intent" )];
	// can't use removeClass because we want this to work with svg elements
	target.attr ( "class" , (target.attr ( "class" ) || "") + " " + intent_class );

	var intent_explanation = "This point " + tsbp.intent_explanation[target.data ( "intent" )];

	if ( target.attr ( "title" ) )
		target.attr ( "title" , intent_explanation );
	if ( target.attr ( "original-title" ) )
		target.attr ( "original-title" , intent_explanation );
};

} ) ();
