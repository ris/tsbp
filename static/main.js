(function ()
{

window.tsbp.state_classes = {
	"enabled" : "_state_enabled" ,
	"disabled" : "_state_disabled" ,
	"initial" : "_state_initial" ,
	"idle" : "_state_idle" ,
	"requesting" : "_state_requesting" ,
	"error" : "_state_error"
};

window.tsbp.hash = {
	// this is our hashchange manager. i'll try to explain how it works. an object on creation,
	// if it desires, registers itself as the "owner" of the next hash token level by pushing a
	// function (generally a context-aware closure if it knows what's good for it) onto the handler
	// stack. on a hash change the handlers on the stack are each called with their respective
	// hash token, importantly giving each a chance to create or destroy any of its children (which
	// may have their own handlers installed) if appropriate before returning and allowing the
	// manager to continue along the stack. handlers all should return augmented hash tokens which
	// are populated with any interesting information they discovered. or perhaps null if they were
	// deactivated as a result. these are collected by the manager and emmitted in an
	// augmentedhashchange event which can be listened to by any interested (but non-owning) parties.
	//
	// at least that's the idea.

	_tokenize_hash: function ( hash_string )
	{
		return hash_string.replace ( /^\#/ , "" ).split ( ":" );
	} ,
	_untokenize_hash: function ( hash_tokens )
	{
		return "#" + hash_tokens.join ( ":" );
	} ,

	set_hash_token: function ( token_index , token_content )
	{
		var tokens = window.tsbp.hash._tokenize_hash ( window.location.hash );
		
		tokens.splice ( token_index );
		
		if ( token_content != null )
			tokens.push ( token_content );
		
		window.location.hash = window.tsbp.hash._untokenize_hash ( tokens );
	} ,
	pop_hash_token: function ()
	{
		// pops a token off the page's current hash value
		var tokens = window.tsbp.hash._tokenize_hash ( window.location.hash );
		var popped = tokens.pop ();
	
		window.location.hash = window.tsbp.hash._untokenize_hash ( tokens );
	
		return popped;
	} ,

	_handler_stack: [] ,

	push_handler: function ( handler )
	{
		tsbp.hash._handler_stack.push ( handler );
	} ,
	truncate_handler: function ( handler )
	{
		// removes all handlers on stack after (and including) handler
		var i = tsbp.hash._handler_stack.indexOf ( handler );
		tsbp.hash._handler_stack.splice ( i , tsbp.hash._handler_stack.length - i );
	}
};
$(window).on ( "hashchange" , function ( event , extra_options )
{
	//
	// master hashchange manager function
	//

	var i = 0;
	var tokenized_hash = tsbp.hash._tokenize_hash ( window.location.hash );
	var augmented_token_array = [];
	
	while ( tsbp.hash._handler_stack[i] != null || tokenized_hash[i] != null )
	{
		augmented_token_array.push ( tsbp.hash._handler_stack[i] != null ? tsbp.hash._handler_stack[i] ( tokenized_hash[i] , extra_options ) : tokenized_hash[i] );
		i++;
	}

	$(window).trigger ( "augmentedhashchange" , [ augmented_token_array , extra_options ] );
} );

window.tsbp.force_reference = function ( inner )
{
	// I'm not sure if you'd call this boxing or unboxing or... iunno - either way
	// it prevents $.extend's "deep" mode from making a copy of the inner object when we
	// don't want it to by disguising it as a non-plain-object
	var BoxUnbox = function () {};
	BoxUnbox.prototype = inner;
	
	return new BoxUnbox;
};

// Our general tipsy parameters
window.tsbp.tsbp_tipsy_parameters = {
	gravity : function ()
	{
		var element_center_y = $(this).offset().top + ( $(this).height () / 2.0 ) - $(document).scrollTop ();
		var element_center_x = $(this).offset().left + ( $(this).width () / 2.0 ) - $(document).scrollLeft ();

		var result = element_center_y > $(window).height () * 0.5 ? "s" : "n" ;

		if ( element_center_x < $(window).width () * 0.25 )
			result += "w";
		else if ( element_center_x > $(window).width () * 0.75 )
			result += "e";

		return result;
	} ,
	opacity : 0.9 ,
	delayIn : 250
};

window.tsbp.SolutionMap = function ( initial_element , leaflet_map , track_overview_geojson , track_overview_base_style , detailed_track_base_style , contextmenu , solution_master_panel )
{
	var sm = initial_element.addClass ( "_solutionmap" );
	sm.data ( "intents" , { tracks : {} } );
	track_overview_base_style = track_overview_base_style || {};
	detailed_track_base_style = detailed_track_base_style || {};

	var all_directly_known = sm.add ( leaflet_map ).add ( contextmenu ).add ( solution_master_panel );

	// "._solutionmap_map" should have already been turned into a leaflet map and set up as desired by the caller
	var map = leaflet_map;
	var deselect_click_timer_id;
	var deselect_click_blocked = false;

	map.on ( "click" , function ( event )
	{
		if ( deselect_click_blocked )
			return;
		window.clearTimeout ( deselect_click_timer_id );
		// don't immediately do a deselect - wait until we know it's not part of a dblclick
		deselect_click_timer_id = window.setTimeout ( window.tsbp.hash.pop_hash_token , 500 );
	} );

	map.on ( "dblclick" , function ( event )
	{
		// any pending clicks we'll say were part of the dblclick and shouldn't be acted upon
		window.clearTimeout ( deselect_click_timer_id );
	} );

	map.on ( "boxzoomend" , function ( event )
	{
		deselect_click_blocked = true;
		// block clicks for a period after this as we will probably get a spurious click through
		window.setTimeout ( function () { deselect_click_blocked = false } , 500 );
	} );

	// a 1x1 transparent png
	var empty_icon = new L.Icon ( { iconUrl : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=" } );

	var track_overview_layer = L.geoJson_hidetrackindex ( track_overview_geojson , {
		style: function ( feature )
		{
			return $.extend ( {} , { "color" : window.TSBP_TRACK_COLORS.colors[ feature.properties.track_index % window.TSBP_TRACK_COLORS.colors.length ] } , track_overview_base_style );
		} ,
		pointToLayer: function ( geojson , latlng )
		{
			// MultiPoints will be acting as a placeholder for extents and shouldn't be visible
			return new L.Marker ( latlng , { icon : empty_icon , clickable : false } );
		}
	} ).addTo ( map ).on ( "click" , function ( event )
	{
		window.location.hash = "#track-" + event.layer.feature.properties.track_index;
	} );

	try {
		map.fitBounds ( track_overview_layer.getBounds () );
	}
	catch ( e ) {
		// hmm - perhaps we have no useful content to fit to.
		map.fitWorld ();
	}

	var cost_plot = $();

	var destroy_cost_plot = function ()
	{
		var pre_destroy_function = cost_plot.data ( "preDestroy" );
		if ( $.isFunction ( pre_destroy_function ) )
			pre_destroy_function ();

		cost_plot.remove ();
	};

	var hash_handler = function ( hash_token )
	{
		var intents = sm.data ( "intents" );

		var hash_track_re = /^track\-(\d+?)$/;
		var hash_match = hash_track_re.exec ( hash_token );
		var track_index = hash_match && parseInt ( hash_match[1] );

		if ( track_index != null && track_overview_geojson.features[track_index] != null )
		{
			track_overview_layer.hidden_track_index = track_index;
			track_overview_layer.refreshHidden ();

			if ( cost_plot.data ( "track_index" ) !== track_index )
			{
				destroy_cost_plot ();

				var intents_track = intents.tracks[track_index] = intents.tracks[track_index] == null ? {} : intents.tracks[track_index];

				cost_plot = tsbp.CostPlot ( $("<div/>").addClass ( "_" + window.TSBP_TRACK_COLORS.names[ track_index % window.TSBP_TRACK_COLORS.names.length ] ).appendTo ( sm ) , track_index , intents_track , window.TSBP_NOCOST_COLOR.base , window.TSBP_COST_COLOR.base , detailed_track_base_style );
			}

			return {
				type: "track" ,
				track_index: track_index
			};
		}

		if ( track_overview_layer.hidden_track_index != null )
		{
			track_overview_layer.hidden_track_index = null;
			track_overview_layer.refreshHidden ();

			destroy_cost_plot ();
		}

		return null;
	};
	tsbp.hash.push_handler ( hash_handler );
	// TODO unhook this on destruction

	sm.on ( "addLayer" , function ( event , layer )
	{
		map.addLayer ( layer );
	} );

	sm.on ( "removeLayer" , function ( event , layer )
	{
		map.removeLayer ( layer );
	} );

	sm.on ( "intentsUpdate" , function ( event , sub_type , track_index )
	{
		all_directly_known.addClass ( "_solutionmap_with_intents" );
		
		if ( sub_type === "intent" )
		{
			// apparently track_index's intents have been updated
			var intents = sm.data ( "intents" );

			// so let's recalculate the overview
			var track_length = intents.tracks[track_index].intentarray.length;
			var bucket_fraction = Math.min ( 1 , 128 / track_length );
			var overview_array = [];

			for ( var i = 0 ; i < track_length ; i++ )
				overview_array[Math.floor(i*bucket_fraction)] |= (1<<intents.tracks[track_index].intentarray[i]);

			solution_master_panel.find ( "._tracklistingentry[data-track-index='" + track_index + "']" ).trigger ( "intentsOverviewUpdate" , [ overview_array ] );

			all_directly_known.find ( "._intent_listener[data-track-index='" + track_index + "']" ).each ( function ( i , element )
			{
				var target = $(element);
				
				// first remove existing intent class
				var intent_class = tsbp.intent_class[target.data ( "intent" )];
				// can't use removeClass because we want this to work with svg elements
				target.attr ( "class" , $.map ( (target.attr ( "class" ) || "").split ( " " ) , function ( str ) { if ( str === intent_class ) return null; return str; } ).join ( " " ) );

				// update the intent data
				target.data ( "intent" , intents.tracks[track_index].intentarray[ target.data ( "trackpointIndex" ) ] );

				tsbp.update_intent_listener ( target );
			} );
		}
	} );

	var updatesmpposition = function ()
	{
		try {
			solution_master_panel.trigger ( "mapPositionChanged" , [ map.getCenter () , map.getZoom () , map.getBounds () ] );
		}
		catch ( e ) {
			return;
		}
	};
	map.on ( "moveend zoomend" , updatesmpposition );
	updatesmpposition ();

	return sm;
};

var _ti_pinning_lt_function = function ( a , b )
{
	// actually using a <= condition to obtain bisect_left semantics
	return a <= b[0];
}

window.tsbp.candidate_description = function ( annotation )
{
	if ( !annotation )
		return null;
	
	var way_str = annotation.id ? ( "way " + annotation.id ) : "";
	if ( way_str && ( annotation.n || annotation.r ) )
		way_str = " (" + way_str + ")";
	
	return ( annotation.n || annotation.r || "" ) + way_str;
};

var _pinning_description = function ( pinning )
{
	return "Trackpoint " + pinning[0] + " pinned to " + ( pinning[1] < 0 ? ( "none of the first " + (-pinning[1]) + " matches" ) : ( window.tsbp.candidate_description ( pinning[2] ) || "candidate point " + pinning[1] ) )
};

window.tsbp.CostPlot = function ( initial_element , track_index , supplied_intents , low_cost_color , high_cost_color , leaflet_layer_base_style )
{
	// these should probably be defined somewhere else
	var pin_svg_url = "/_static/images/pin.plain.svg";
	var embeddingmark_svg_url = "/_static/images/embeddingmark.plain.svg#embeddingmark";
	
	// supplied_intents is mandatory as caller should be keeping a reference to it
	var cp = initial_element.addClass ( "_costplot _panel" );
		var detail_plot_element = $("<div class='_costplot_detailplot'/>").appendTo ( cp );
		var shim_container_element = $("<div class='_costplot_shim_container'/>").appendTo ( cp );
			var shim_canvas_element = $("<canvas class='_costplot_shim_canvas'/>").appendTo ( shim_container_element );
			var pin_container_svg = $("<svg class='_costplot_pin_container'></svg>").appendTo ( shim_container_element );
				var pin_container_defs = $(document.createElementNS ( "http://www.w3.org/2000/svg" , "defs" )).appendTo ( pin_container_svg );
					var pin_container_defs_pin_parent = $(document.createElementNS ( "http://www.w3.org/2000/svg" , "g" )).appendTo ( pin_container_defs );
					var pin_container_embeddingmark_pair_g = $(document.createElementNS ( "http://www.w3.org/2000/svg" , "g" )).attr ( "id" , "pinembeddingmarkpair" ).appendTo ( pin_container_defs );
						var pin_container_embeddingmark_pair_g_upper_use = $(document.createElementNS ( "http://www.w3.org/2000/svg" , "use" )).appendTo ( pin_container_embeddingmark_pair_g );
						pin_container_embeddingmark_pair_g_upper_use[0].setAttributeNS ( "http://www.w3.org/1999/xlink" , "xlink:href" , embeddingmark_svg_url );
						var pin_container_embeddingmark_pair_g_lower_use = $(document.createElementNS ( "http://www.w3.org/2000/svg" , "use" )).attr ( "transform" , "scale(1,-1)" ).appendTo ( pin_container_embeddingmark_pair_g );
						pin_container_embeddingmark_pair_g_lower_use[0].setAttributeNS ( "http://www.w3.org/1999/xlink" , "xlink:href" , embeddingmark_svg_url );
				var pin_container_clip_path = $(document.createElementNS ( "http://www.w3.org/2000/svg" , "clipPath" )).attr ( "id" , "pinclippath" ).appendTo ( pin_container_svg );
					var pin_container_clip_path_upper_rect = $(document.createElementNS ( "http://www.w3.org/2000/svg" , "rect" )).appendTo ( pin_container_clip_path );
					var pin_container_clip_path_lower_rect = $(document.createElementNS ( "http://www.w3.org/2000/svg" , "rect" )).appendTo ( pin_container_clip_path );
				var pin_container_pin_g = $(document.createElementNS ( "http://www.w3.org/2000/svg" , "g" )).attr ( "class" , "_costplot_pin_container_pin_g" ).attr ( "clip-path" , "url(#pinclippath)" ).appendTo ( pin_container_svg );
				var pin_container_embeddingmark_g = $(document.createElementNS ( "http://www.w3.org/2000/svg" , "g" )).attr ( "class" , "_costplot_pin_container_embeddingmark_g" ).appendTo ( pin_container_svg );
		var overview_plot_element = $("<div class='_costplot_overviewplot'/>").appendTo ( cp );
		var end_positioning_helper_element = $("<div class='_costplot_endpositioninghelper'/>").appendTo ( cp );

	// we need the svg to be _in_ our DOM (not just <use/>d) to be able to apply css selectors etc to it, hence this slightly odd xhr here
	pin_container_defs_pin_parent.load ( pin_svg_url + " #pin" );
	
	// this is not added to document yet - will be added as needed
	var tooltip_positioning_surrogate = $("<div/>").css ( {
		visibility: "hidden" ,
		position: "absolute" ,
		"z-index": -1
	} );

	cp.data ( "track_index" , track_index );

	// initialize shim canvas
	shim_canvas_element[0].width = shim_container_element.innerWidth ();
	shim_canvas_element[0].height = shim_container_element.innerHeight ();

	var setup_stroke_gradient = function ( plot , series , ctx )
	{
		var gradient = ctx.createLinearGradient ( 0 , 0 , 0 , series.yaxis.p2c ( series.yaxis.min ) );
		gradient.addColorStop ( 0 , high_cost_color );
		gradient.addColorStop ( 1 , low_cost_color );
		ctx.strokeStyle = gradient;
	};

	var plotdata = function ( track_data , intents )
	{
		var cost_array = [];
		var i;
		for ( i = 0 ; i < track_data.visual_costprofile.length ; i++ )
			cost_array.push ( [ i , track_data.visual_costprofile[i] ] );

		var data = [
			{
				"data" : cost_array ,
				color: "#666"
			}
		];

		var boxed_intents = window.tsbp.force_reference ( intents.intentarray );

		var detail_plot = $.plot ( detail_plot_element , data , {
			xaxis: {
				show: false ,
				zoomRange: [3,data[0].data.length-1] ,
				panRange: [0,data[0].data.length-1]
			} ,
			yaxis: {
				show: false ,
				zoomRange: false
			} ,
			grid: {
				color: "#ccc" ,
				borderWidth: 1,
				labelMargin: 0 ,
				axisMargin: 0 ,
				minBorderMargin: 0 ,
				hoverable: true ,
				clickable: true ,
				autoHighlight: false
			} ,
			zoom: {
				interactive: true ,
				amount: 1.5
			} ,
			series: {
				eoshighlight: "active" ,
				eoshighlightRangeFillColor: $.color.parse(high_cost_color).scale("a",0.3).toString () ,
				eoshighlightNullMarkingColor: $.color.parse(high_cost_color).scale("a",0.3).toString () ,
				eoshighlightNavigateExpandRangeToSelection: true ,
				lines: {
					lineWidth: 3 ,
					setupDrawContext: setup_stroke_gradient
				} ,
				points: {
					radius: 5 ,
					lineWidth: 3
				} ,
				"intents": boxed_intents ,
				intentsFillStyleFunction : window.tsbp.intent_fill_style
			}
		} );
	
		var overview_plot = $.plot ( overview_plot_element , data , {
			xaxis: {
				show: false
			} ,
			yaxis: {
				show: false
			} ,
			grid: {
				color: "#ccc" ,
				borderWidth: 1,
				labelMargin: 0 ,
				axisMargin: 0 ,
				minBorderMargin: 0
			} ,
			series: {
				eoshighlight: "passive" ,
				eoshighlightRangeFillColor: $.color.parse(high_cost_color).scale("a",0.3).toString () ,
				eoshighlightNullMarkingColor: $.color.parse(high_cost_color).scale("a",0.3).toString () ,
				lines: {
					lineWidth: 3 ,
					setupDrawContext: setup_stroke_gradient
				} ,
				points: {
					radius: 5 ,
					lineWidth: 3
				} ,
				"intents": boxed_intents ,
				intentsFillStyleFunction : window.tsbp.intent_fill_style
			} ,
			draggableselection: {
				mode: "x" ,
				invertFill: true ,
				color: "#000",
				fillColor: "rgba(0,0,0,0.2)",
				edgeLineWidth: 2
			}
		} );

		return { detail : detail_plot , overview : overview_plot };
	};

	var initintents = function ( track_data )
	{
		supplied_intents.intentarray = supplied_intents.intentarray || [];
		
		for ( var i = 0 ; i < track_data.costprofile.length ; i++ )
			supplied_intents.intentarray[i] = track_data.costprofile[i] == null ? window.tsbp.intent.excluded : ( supplied_intents.intentarray[i] == null ? window.tsbp.intent.base : supplied_intents.intentarray[i] );
		
		// we don't have a clever way of merging sources of pinnings yet
		supplied_intents.pinnings = supplied_intents.pinnings || track_data.pinnings.slice () || [];

		return supplied_intents;
	};
	
	var set_pinning = function ( intents_pinnings , track_index , new_pinning , annotation )
	{
		var i = intents_pinnings.bisect ( track_index , null , null , _ti_pinning_lt_function );
		
		// separating the (possible) removal and (possible) addition of entries here as either could be
		// conditionally necessary
		var either = false;
		if ( intents_pinnings[i] && intents_pinnings[i][0] === track_index )
		{
			either = true;
			// remove old entry
			intents_pinnings.splice ( i , 1 );
		}
		if ( new_pinning != null )
		{
			either = true;
			// add new entry
			intents_pinnings.splice ( i , 0 , [ track_index , new_pinning , annotation ] );
		}
		if ( !either )
			// nothing was changed - don't let this propagate
			return;
		
		cp.trigger ( "intentsUpdate" , [ "pinning" , track_index ] );
		leaflet_layer_deferred.done ( function ( layer )
		{
			// bleh. layer is actually the geojson layer, so we need to find the sublayer we want
			layer.eachLayer ( function ( sublayer )
			{
				if ( sublayer.setPinning )
					sublayer.setPinning ( new_pinning );
			} );
		} );
	};

	var raw_summary_data_deferred = $.ajax ( {
		dataType: "json" ,
		traditional: true ,
		url: "track/" + track_index + "/detail.json"
	} )
	var summary_data_deferred = raw_summary_data_deferred.pipe ( function ( track_data )
	{
		var i , c;
		track_data.visual_costprofile = [];
		for ( i = 0 ; i < track_data.costprofile.length ; i++ )
			track_data.visual_costprofile.push ( track_data.costprofile[i] == null ? null : Math.sqrt ( track_data.costprofile[i] ) );

		for ( i = 0 ; i < track_data.costprofile.length ; i++ )
			if ( (track_data.costprofile_max || -1) < ( track_data.costprofile[i] || 0 ) )
				track_data.costprofile_max = track_data.costprofile[i];
		track_data.visual_costprofile_max = track_data.costprofile_max == null ? null : Math.sqrt ( track_data.costprofile_max );

		return track_data;
	} );
	var intents_deferred = summary_data_deferred.pipe ( initintents );
	var plots_deferred = $.when ( summary_data_deferred , intents_deferred ).pipe ( plotdata );
	var leaflet_layer_deferred = summary_data_deferred.pipe ( function ( data )
	{
		var linestring = data.linestring || { type : "LineString" , coordinates : [] };

		linestring.properties = {
			fullindexes : [] ,
			gpxpoints: data.gpxpoints
		};

		//
		// TODO tidy up this mess of contradictory ways of doing things.
		//

		// build up an array of corresponding indexes between track and routable track
		$.each ( data.costprofile , function ( i , cost )
		{
			if ( cost != null )
				linestring.properties.fullindexes.push ( i );
		} );

		var lcc = $.color.parse(low_cost_color) , hcc = $.color.parse(high_cost_color)

		return L.geoJsonM ( linestring , {
			style: function ( feature )
			{
				return $.extend ( {
					"color0" : [ lcc.r , lcc.g , lcc.b ] ,
					"color1" : [ hcc.r , hcc.g , hcc.b ] 
				} , leaflet_layer_base_style );
			}
		} );
	} ).done ( function ( layer )
	{
		cp.trigger ( "addLayer" , layer );

		layer.on ( "selectionChanged" , function ( event )
		{
			// handle selections from layer
			if ( event.trackIndexes.length )
			{
				if ( event.trackIndexes.length === 1 )
					window.tsbp.hash.set_hash_token ( 1 , "trackpoint-" + event.trackIndexes[0] );
				else
					window.tsbp.hash.set_hash_token ( 1 , "trackpoints-" + event.trackIndexes[0] + "-to-" + event.trackIndexes[1] );
			}
			else
				window.tsbp.hash.set_hash_token ( 1 , null );
		} );

		// propagate hovers from layer to plot
		layer.on ( "hoverChanged" , function ( event )
		{
			plots_deferred.done ( function ( plots ) { plots.detail.eosHover ( event.trackIndexes.length === 0 ? null : 0 , event.trackIndexes[0] , event.trackIndexes[1] ) } );
		} );

		// act on pinning from layer
		layer.on ( "pinningChanged" , function ( event )
		{
			$.when ( intents_deferred , plots_deferred ).done ( function ( intents , plots )
			{
				set_pinning ( intents.pinnings , event.trackIndex , event.pinning , event.annotation );
				draw_pinnings ( plots.detail , intents.pinnings );
			} );
		} );

		// relay this leaflet event to a DOM event
		layer.on ( "hideContextMenu" , function ( event )
		{
			cp.trigger ( "hideContextMenu" );
		} );

		// propagate hovers from plot to layer
		detail_plot_element.on ( "ploteoshovered" , function ( event , series , indexes )
		{
			// bleh. layer is actually the geojson layer, so we need to find the sublayer we want
			layer.eachLayer ( function ( sublayer )
			{
				if ( sublayer.setHover )
					sublayer.setHover ( indexes );
			} );
		} );

		cp.on ( "zoomto" , function ( event )
		{
			// bleh. layer is actually the geojson layer, so we need to find the sublayer we want
			layer.eachLayer ( function ( sublayer )
			{
				if ( sublayer.fitSelectionBounds )
					sublayer.fitSelectionBounds ( true );
			} );
		} );
	} );

	plots_deferred.done ( function ( plots )
	{
		// make absolutely sure the two plots are synced up
		detail_plot_element.trigger ( "plotrangesset" , [ plots.detail.getRanges () ] );
	} );

	var draw_shim = function ( detail_plot , overview_plot )
	{
		var range = overview_plot.getSelection ();
		var axes = overview_plot.getAxes ();
		var axis_name = range.xaxis ? "xaxis" : "x2axis";
		var from_x = axes[axis_name].p2c ( range[axis_name].from );
		var to_x = axes[axis_name].p2c ( range[axis_name].to );

		var canvas = shim_canvas_element[0];
		var ctx = canvas.getContext ( "2d" );
		ctx.clearRect ( 0 , 0 , canvas.width , canvas.height );

		ctx.lineWidth = 1;
		// a little darker than grid border color to compensate for aa
		ctx.strokeStyle = "#bbb";

		var from_handle_length = Math.min ( canvas.height , from_x );
		var to_handle_length = Math.min ( canvas.height , canvas.width - to_x );

		ctx.beginPath ();
		ctx.moveTo ( 0 , 0 );
		ctx.bezierCurveTo ( 0 , from_handle_length , from_x , canvas.height - from_handle_length , from_x , canvas.height );
		ctx.moveTo ( canvas.width , 0 );
		ctx.bezierCurveTo ( canvas.width , to_handle_length , to_x , canvas.height - to_handle_length , to_x , canvas.height );

		ctx.stroke ();
	};
	
	var draw_pinnings = function ( detail_plot , pinnings )
	{
		var pin_uses = pin_container_pin_g.children ();
		var embeddingmark_uses = pin_container_embeddingmark_g.children ();
		
		pin_uses.each ( function () { $(this).tipsy ( "hide" ) } );
		
		var ranges = detail_plot.getRanges ();
		var axes = detail_plot.getAxes ();
		var axis_name = ranges.xaxis ? "xaxis" : "x2axis";
		
		var cp_padding_top = parseInt ( window.getComputedStyle ( cp[0] ).paddingTop ) , cp_padding_left = parseInt ( window.getComputedStyle ( cp[0] ).paddingLeft );
		var cp_inner_width = cp.innerWidth () , cp_inner_height = cp.innerHeight ();
		var detail_plot_element_inner_height = detail_plot_element.innerHeight ();
		
		// the following are quite finely tuned for the particular svg we're using at time or writing
		var y_coord = detail_plot_element_inner_height + cp_padding_top - 83;
		var pin_x_offset = cp_padding_left - 4;
		var embeddingmark_x_offset = cp_padding_left - 4;
		
		// set geometry of clipping path
		pin_container_clip_path_upper_rect.attr ( {
			"x" : "-10" ,
			"y" : "-10" ,
			"width" : cp_inner_width + 20 ,
			"height" : Math.round ( detail_plot_element_inner_height / 4.0 ) + cp_padding_top + 10
		} );
		pin_container_clip_path_lower_rect.attr ( {
			"x" : "-10" ,
			"y" : Math.round ( detail_plot_element_inner_height * 3.0 / 4.0 ) + cp_padding_top ,
			"width" : cp_inner_width + 20 ,
			"height" : cp_inner_height + 10 // overkill
		} );
		
		// set positioning of embeddingmarks in the referenced pair
		pin_container_embeddingmark_pair_g_upper_use.attr ( {
			"y" : Math.round ( detail_plot_element_inner_height / 4.0 ) + cp_padding_top
		} );
		pin_container_embeddingmark_pair_g_lower_use.attr ( {
			"y" : - (Math.round ( detail_plot_element_inner_height * 3.0 / 4.0 ) + cp_padding_top)
		} );
		
		var _pin_use , pin_use , _embeddingmark_use , embeddingmark_use , x;
		
		$.each ( pinnings , function ( i , pinning )
		{
			x = Math.round ( axes[axis_name].p2c ( pinning[0] ) );
			
			_pin_use = pin_uses[i];
			
			if ( _pin_use == null )
			{
				_pin_use = document.createElementNS ( "http://www.w3.org/2000/svg" , "use" );
				_pin_use.setAttributeNS ( "http://www.w3.org/1999/xlink" , "xlink:href" , "#pin" );
				_pin_use.setAttribute ( "y" , y_coord );
				pin_container_pin_g.append ( _pin_use );
			}
			
			pin_use = $(_pin_use);
			
			pin_use.attr ( "x" , x + pin_x_offset );
			pin_use.attr ( "title" , _pinning_description ( pinning ) );
			pin_use.tipsy ( tsbp.tsbp_tipsy_parameters );
			
			
			_embeddingmark_use = embeddingmark_uses[i];
			
			if ( _embeddingmark_use == null )
			{
				_embeddingmark_use = document.createElementNS ( "http://www.w3.org/2000/svg" , "use" );
				_embeddingmark_use.setAttributeNS ( "http://www.w3.org/1999/xlink" , "xlink:href" , "#pinembeddingmarkpair" );
				pin_container_embeddingmark_g.append ( _embeddingmark_use );
			}
			
			embeddingmark_use = $(_embeddingmark_use);
			
			embeddingmark_use.attr ( "x" , x + embeddingmark_x_offset );
		} );
		
		if ( pin_use )
			// purge trailing uses
			pin_use.nextAll ().remove ();
		
		if ( embeddingmark_use )
			// purge trailing uses
			embeddingmark_use.nextAll ().remove ();
	};

	//
	// Set up feedback loop for plot positioning
	//

	detail_plot_element.on ( "plotzoom plotpan" , function ( event , plot )
	{
		var ranges = plot.getRanges ();
		var xaxis_name = ranges.xaxis ? "xaxis" : "x2axis";
		var xonly_range = {};
		xonly_range[xaxis_name] = ranges[xaxis_name];
		plots_deferred.done ( function ( plots ) { plots.overview.setSelection ( xonly_range ) } );

		cp.trigger ( "hideContextMenu" );
	} );

	detail_plot_element.on ( "plotrangesset" , function ( event , ranges )
	{
		var xaxis_name = ranges.xaxis ? "xaxis" : "x2axis";
		var xonly_range = {};
		xonly_range[xaxis_name] = ranges[xaxis_name];
		plots_deferred.done ( function ( plots ) { plots.overview.setSelection ( xonly_range ) } );

		cp.trigger ( "hideContextMenu" );
	} );

	overview_plot_element.on ( "plotselecting" , function ( event , range )
	{
		plots_deferred.done ( function ( plots )
		{
			var xaxis_name = range.xaxis ? "xaxis" : "x2axis";
			var xonly_range = {};
			xonly_range[xaxis_name] = range[xaxis_name];
			plots.detail.setRanges ( xonly_range );

			draw_shim ( plots.detail , plots.overview );
		} );
		
		$.when ( intents_deferred , plots_deferred ).done ( function ( intents , plots ) { draw_pinnings ( plots.detail , intents.pinnings ) } );

		cp.trigger ( "hideContextMenu" );
	} );

	// handle resizes for shim
	cp.on ( "resize" , function ( event )
	{
		shim_canvas_element[0].width = shim_container_element.innerWidth ();
		shim_canvas_element[0].height = shim_container_element.innerHeight ();

		plots_deferred.done ( function ( plots ) { draw_shim ( plots.detail , plots.overview ) } );
		$.when ( intents_deferred , plots_deferred ).done ( function ( intents , plots ) { draw_pinnings ( plots.detail , intents.pinnings ) } );
	} );
	
	// hopefully make it impossible to show more than one of these tooltips at a time
	pin_container_pin_g.on ( "tipsyPreShow" , function ( event )
	{
		pin_container_pin_g.children ().each ( function () { $(this).tipsy ( "hide" ) } );
	} );

	// handle intent transition request events
	cp.on ( "try_ untry ignore unignore" , function ( event )
	{
		$.when ( intents_deferred , plots_deferred ).done ( function ( intents , plots )
		{
			var selection = plots.detail.eosGetSelection ();
			
			if ( ! ( selection && selection.indexes && selection.indexes.length ) )
				return;

			for ( var i = selection.indexes[0] ; i <= ( selection.indexes[1] || selection.indexes[0] ) ; i++ )
				intents.intentarray[i] = window.tsbp.intent_transition_matrix[intents.intentarray[i]][window.tsbp.intent_transition[event.type]];

			plots.detail.draw ();
			plots.overview.draw ();

			cp.trigger ( "intentsUpdate" , [ "intent" , track_index ] )
		} );

		return false;
	} );
	
	// handle selectbefore & selectafter
	cp.on ( "selectbefore selectafter" , function ( event )
	{
		plots_deferred.done ( function ( plots )
		{
			var selection = plots.detail.eosGetSelection ();
			
			if ( ! ( selection && selection.indexes && selection.indexes.length ) )
				return;
			
			if ( event.type === "selectafter" )
			{
				// check existing selection actually has an "after"
				if ( selection.indexes[selection.indexes.length-1] < (selection.series.data.length-1) )
					plots.detail.eosSelect ( selection.series , selection.indexes[0] , selection.series.data.length-1 );
			} else if ( event.type === "selectbefore" )
			{
				// check existing selection actually has a "before"
				if ( selection.indexes[0] > 0 )
					plots.detail.eosSelect ( selection.series , 0 , selection.indexes[1] || selection.indexes[0] );
			}
		} );

		return false;
	} )

	// propagate selections to overview plot
	detail_plot_element.on ( "ploteosselected" , function ( event , eosselectedseries , eosselectedindexes )
	{
		plots_deferred.done ( function ( plots ) { plots.overview.eosSelect ( eosselectedseries , eosselectedindexes[0] , eosselectedindexes[1] ) } );
	} );

	// propagate selections to page
	detail_plot_element.on ( "ploteosselected" , function ( event , eosselectedseries , eosselectedindexes )
	{
		if ( eosselectedseries == null )
			window.tsbp.hash.set_hash_token ( 1 , null );
		else if ( eosselectedindexes.length == 1 )
			window.tsbp.hash.set_hash_token ( 1 , "trackpoint-" + eosselectedindexes[0] );
		else
			window.tsbp.hash.set_hash_token ( 1 , "trackpoints-" + eosselectedindexes[0] + "-to-" + eosselectedindexes[1] );
	} );

	// propagate hovers to overview plot
	detail_plot_element.on ( "ploteoshovered" , function ( event , eoshoveredseries , eoshoveredindexes )
	{
		plots_deferred.done ( function ( plots ) { plots.overview.eosHover ( eoshoveredseries , eoshoveredindexes[0] , eoshoveredindexes[1] ) } );
	} );
	
	// propagate selection to plots
	var hash_handler = function ( token , extra_options )
	{
		var hash_trackpoint_re = /^trackpoint(\-(\d+?)|s\-(\d+?)\-to\-(\d+?))$/;
		var match = ( token || "" ).match ( hash_trackpoint_re );
		var match_ints = ( match || [] ).slice ( 2 );
		var selectedindexes = [];
		var i, j;
		if ( match_ints[0] )
		{
			i = parseInt ( match_ints[0] );
			if ( ! isNaN ( i ) )
				selectedindexes.push ( i );
		}
		else if ( match_ints[1] )
		{
			i = parseInt ( match_ints[1] );
			j = parseInt ( match_ints[2] );
			if ( ! ( isNaN ( i ) || isNaN ( j ) ) )
			{
				selectedindexes.push ( i );
				selectedindexes.push ( j );
			}
		}
		
		plots_deferred.done ( function ( plots )
		{
			plots.detail.eosSelect ( selectedindexes.length === 0 ? null : 0 , selectedindexes[0] , selectedindexes[1] );
			leaflet_layer_deferred.done ( function ( layer )
			{
				// here is where we _would_ call the map layer's .setSelection (), but instead
				// we're letting the augmentedhashchange event do that

				if ( extra_options && extra_options.zoom_map )
					cp.trigger ( "zoomto" );
			} );
		} );

		return {
			type: "trackpoint" ,
			"selectedindexes": selectedindexes ,
			pointinfo_promise: $.when ( summary_data_deferred , intents_deferred ).pipe ( function ( track_data , intents )
			{
				var selectedindexes_length = selectedindexes.length;
				return $.map ( selectedindexes , function ( index )
				{
					var info = {
						cost: track_data.costprofile[index] ,
						relative_visual_cost: track_data.visual_costprofile[index] == null ? null : track_data.visual_costprofile[index] / track_data.visual_costprofile_max ,
						intent: intents.intentarray[index]
					};
					// hmm - better way of deciding this?
					if ( selectedindexes_length === 1 )
					{
						info.detail_promise = $.ajax ( {
							type : "GET" ,
							dataType : "json" ,
							url : "track/" + track_index + "/trackpoint/" + index + "/detail.json"
						} );
					}
					return info;
				} );
			} )
		};
	};
	tsbp.hash.push_handler ( hash_handler );
	
	var latest_pointinfo_promise , latest_detail_promise;
	// augmentedhashchange event handling for layer done here because we have to be able to set it up
	// so it works from the start (before the layer is even created). we also have access to useful
	// things like track_index from here
	var layer_aumgmentedhash_handler = function ( event , augmented_token_array , extra_options )
	{
		leaflet_layer_deferred.done ( function ( layer )
		{
			var selected = augmented_token_array[0] && augmented_token_array[0].type === "track" && augmented_token_array[0].track_index === track_index;
			
			if ( ! selected )
				return;
			
			var selectedindexes = augmented_token_array[1] && augmented_token_array[1].type === "trackpoint" ? augmented_token_array[1].selectedindexes : [];
			
			// bleh. layer is actually the geojson layer, so we need to find the sublayer we want
			layer.eachLayer ( function ( sublayer )
			{
				if ( sublayer.setSelection )
					sublayer.setSelection ( selectedindexes );
			} );
			
			if ( selectedindexes.length === 1 )
			{
				latest_pointinfo_promise = augmented_token_array[1].pointinfo_promise;
				augmented_token_array[1].pointinfo_promise.done ( function ( pointinfo )
				{
					// because promises aren't cancelable, we have to do an "are we still wanted?" check in this really stupid way
					if ( latest_pointinfo_promise !== augmented_token_array[1].pointinfo_promise )
						return;
					
					//changestate ( "requesting" );
					
					latest_detail_promise = pointinfo[0].detail_promise;
					pointinfo[0].detail_promise.done ( function ( data , textStatus , xhr )
					{
						// because promises aren't cancelable, we have to do an "are we still wanted?" check in this really stupid way
						if ( latest_detail_promise !== pointinfo[0].detail_promise )
							return;
						
						if ( data.calculated_candidates == null )
							return;
						
						//changestate ( "idle" );
				
						// bleh. layer is actually the geojson layer, so we need to find the sublayer we want
						layer.eachLayer ( function ( sublayer )
						{
							if ( sublayer.setCandidates )
								intents_deferred.done ( function ( intents )
								{
									var i = intents.pinnings.bisect ( selectedindexes[0] , null , null , _ti_pinning_lt_function );
									sublayer.setCandidates ( data.calculated_candidates , ( intents.pinnings[i] && intents.pinnings[i][0] === selectedindexes[0] ) ? intents.pinnings[i][1] : null , data.current_candidate_index );
								} );
						} );
					} ).fail ( function ()
					{
						//changestate ( "error" );
					} ).always ( function ()
					{
						latest_detail_promise = null;
					} );
				} ).always ( function ()
				{
					latest_pointinfo_promise = null;
				} );
			}
		} );
	};
	$(window).on ( "augmentedhashchange" , layer_aumgmentedhash_handler );

	$.when ( intents_deferred , plots_deferred ).done ( function ( intents , plots )
	{
		detail_plot_element.on ( "requestContextMenu" , function ( event )
		{
			// we want to add some information to this event

			var selection = plots.detail.eosGetSelection ();
			var seen_intents = {};

			if ( selection && selection.indexes && selection.indexes.length )
			{
				for ( var i = selection.indexes[0] ; i <= selection.indexes[selection.indexes.length-1] ; i++ )
				{
					seen_intents[intents.intentarray[i]] = true;
				};
			}

			var arguments_array = Array.prototype.slice.call ( arguments );
			cp.trigger ( event , arguments_array.slice ( 1 ).concat ( [ seen_intents , selection && selection.indexes , selection.series.data.length ] ) );
			return false;
		} );
	} );

	cp.data ( "preDestroy" , function ()
	{
		leaflet_layer_deferred.done ( function ( layer )
		{
			// let event bubble up to signal we want this layer removed from the map
			cp.trigger ( "removeLayer" , layer );
		} );

		plots_deferred.done ( function ( plots )
		{
			plots.detail.shutdown ();
			plots.overview.shutdown ();
		} );
		
		if ( raw_summary_data_deferred.state () === "pending" )
			raw_summary_data_deferred.abort ();
		if ( latest_detail_promise && latest_detail_promise.state () === "pending" )
			latest_detail_promise.abort ();

		tsbp.hash.truncate_handler ( hash_handler );
		
		$(window).off ( "augmentedhashchange" , layer_aumgmentedhash_handler );
	} );

	var last_tooltip_x_coords = [] , last_tooltip_intent , last_tooltip_plural;

	// handle plot tooltips
	$.when ( intents_deferred , plots_deferred ).done ( function ( intents , plots )
	{
		detail_plot_element.on ( "plothover" , function ( event , pos , item )
		{
			var new_tooltip_x_coords = [] , new_tooltip_intent = null , new_tooltip_plural , hit_track_index , lower_track_index , upper_track_index , ranges;
	
// 			if ( console && console.log )
// 				console.log ( [ event , pos , item ] );
			if ( item != null )
			{
				new_tooltip_plural = false;
				new_tooltip_x_coords[0] = item.series.xaxis.p2c ( item.datapoint[0] );
				new_tooltip_x_coords[1] = new_tooltip_x_coords[0];
				new_tooltip_intent = intents.intentarray[item.datapoint[0]];
			}
			else
			{
				hit_track_index = Math.round ( pos.x );
				new_tooltip_intent = intents.intentarray[hit_track_index];

				if ( new_tooltip_intent !== null && new_tooltip_intent !== tsbp.intent.base )
				{
					// otherwise don't bother and take a shortcut

					axis = plots.detail.getAxes ().xaxis;
	
					// scan upwards for upper limit of intent block
					// using min-1 so we catch fractions
					for ( var i = hit_track_index ; i >= 0 && i >= axis.options.min-1 && intents.intentarray[i] === new_tooltip_intent ; i-- )
						lower_track_index = i;
	
					// scan downwards for lower limit of intent block
					// using max+1 so we catch fractions
					for ( var i = hit_track_index ; i < intents.intentarray.length && i <= axis.options.max+1 && intents.intentarray[i] === new_tooltip_intent ; i++ )
						upper_track_index = i;
	
					new_tooltip_plural = lower_track_index !== upper_track_index;
					new_tooltip_x_coords[0] = axis.p2c ( Math.max ( lower_track_index-0.5 , axis.options.min ) );
					new_tooltip_x_coords[1] = axis.p2c ( Math.min ( upper_track_index+0.5 , axis.options.max ) );
				}
			}

			if ( new_tooltip_intent === null || new_tooltip_intent === tsbp.intent.base )
			{
				detail_plot_element.removeAttr ( "original-title" ).tipsy ( "hide" );
				// note not removing tooltip_positioning_surrogate from document as it causes a visible glitch on chrome

				// clear all of these
				last_tooltip_x_coords = [];
				last_tooltip_intent = null;
				last_tooltip_plural = null;
			}
			else if ( last_tooltip_x_coords[0] !== new_tooltip_x_coords[0] || last_tooltip_x_coords[1] !== new_tooltip_x_coords[1] || last_tooltip_intent !== new_tooltip_intent || last_tooltip_plural !== new_tooltip_plural )
			{
				tooltip_positioning_surrogate.css ( {
					left: plots.detail.getPlotOffset().left + new_tooltip_x_coords[0] ,
					top: 0,
					bottom: 0,
					width: new_tooltip_x_coords[1] - new_tooltip_x_coords[0]
				} ).appendTo ( detail_plot_element );
				detail_plot_element.attr ( "original-title" , new_tooltip_plural ? "These points " + tsbp.intent_explanation_plural[new_tooltip_intent] : "This point " + tsbp.intent_explanation[new_tooltip_intent] );

				// set all of these
				last_tooltip_x_coords = new_tooltip_x_coords;
				last_tooltip_intent = new_tooltip_intent;
				last_tooltip_plural = new_tooltip_plural;

				// hide any currently shown tooltip as it's now wrong
				detail_plot_element.tipsy ( "hide" );
				// trigger a fake mouseenter to cause tipsy to consider showing a new tooltip
				detail_plot_element.trigger ( "mouseenter" );
			}
		} );
	} );
	detail_plot_element.tipsy ( $.extend ( {} , tsbp.tsbp_tipsy_parameters , { positioningSurrogate: tooltip_positioning_surrogate } ) );

	return cp;
};

} ) ();
