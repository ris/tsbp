//
// Based on http://davywybiral.blogspot.co.uk/2010/09/javascript-insort-function.html
//

Array.prototype._bisect_lt_function = function ( a , b )
{
	return a < b;
};

// using a lt_function that is actually <= gives bisect_left behaviour, else behaviour is bisect_right
Array.prototype.bisect = function ( x , lo , hi , lt_function )
{
	lt_function = lt_function || this._bisect_lt_function;
	var mid;
	if ( lo == null ) lo = 0;
	if ( hi == null ) hi = this.length;
	while ( lo < hi )
	{
		mid = Math.floor ( ( lo + hi ) / 2 );
		if ( lt_function ( x , this[mid] ) ) hi = mid;
		else lo = mid + 1;
	}
	return lo;
};

Array.prototype.insort = function ( x , lo , hi , lt_function ) {
	this.splice ( this.bisect ( x , lo , hi , lt_function ) , 0 , x );
};
