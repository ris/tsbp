(function ()
{

window.tsbp.ButtonList = function ( input_element )
{
	var buttonlist = input_element.addClass ( "_buttonlist _panel" );
	
	var exclude_mouseleaves_into = ".tipsy , .tipsy *";
	
	var replaced_button = buttonlist.find ( "li > a.fakebutton , li > button" ).first ();
	var replacement_button = replaced_button.clone ().addClass ( "_buttonlist_replacement" ).insertAfter ( buttonlist );
	
	if ( replaced_button.attr ( "id" ) )
		replacement_button.attr ( "id" , replaced_button.attr ( "id" ) + "__buttonlist_replacement" );

	replacement_button.width ( replaced_button.width () );
	
	// Set up the visibility functions
	replacement_button.on ( "mouseenter focusin click" , function ( event )
	{
		// Position the buttonlist
		var replaced_button_inner_pos = replaced_button.position ();
		var replacement_button_pos = replacement_button.position ();
		buttonlist.css ( "left" , Math.floor ( replacement_button_pos.left ) - Math.floor ( replaced_button_inner_pos.left ) );
		buttonlist.css ( "top" , Math.floor ( replacement_button_pos.top ) - Math.floor ( replaced_button_inner_pos.top ) );

		buttonlist.css ( "visibility" , "visible" );
		replacement_button.css ( "visibility" , "hidden" );

		if ( event.type === "focusin" )
			// Now that we're showing the buttonlist, the element we actually want focussed is the replaced_button.
			replaced_button.focus ();
	} );
	
	buttonlist.mouseleave ( function ( e )
	{
		if ( $(e.relatedTarget).is ( exclude_mouseleaves_into ) )
			return;

		buttonlist.css ( "visibility" , "hidden" );
		replacement_button.css ( "visibility" , "visible" );
	} );
};

var _dynamicunit_unit_types = {
	"velocity" : {
		"mps" : {
			factor : 1.0 ,
			display_text : "m/s"
		} ,
		"kph" : {
			factor : 3.6 ,
			display_text : "km/h"
		} ,
		"mph" : {
			factor : 2.237 ,
			display_text : "mph"
		}
	}
};

window.tsbp.DynamicUnit = function ( input_element , storage_prefix , unit_type )
{
	var du = input_element.addClass ( "_dynamicunit" );
	if ( unit_type )
		du.data ( "unitType" , unit_type );

	storage_prefix = storage_prefix || "";
	var storage_key = storage_prefix + "dynamicunit_" + unit_type + "_unit_preference";

	var valuecontainer = $("<span class='_dynamicunit_valuecontainer'/>");
	var unitselect = $("<select/>");
	$.each ( _dynamicunit_unit_types[ du.data ( "unitType" ) ] , function ( label , obj )
	{
		unitselect.append ( $("<option/>").attr ( "value" , label ).text ( obj.display_text ) );
	} );
	du.empty ().append ( valuecontainer ).append ( unitselect );

	du.on ( "change" , "select" , function ( event )
	{
		var original_unit_value = du.data ( "unit" ) == null ? 1.0 : _dynamicunit_unit_types[ du.data ( "unitType" ) ][du.data ( "unit" ) ].factor;
		var new_unit_value = _dynamicunit_unit_types[ du.data ( "unitType" ) ][ unitselect.val () ].factor;
		valuecontainer.text ( ( du.data ( "value" ) * new_unit_value / original_unit_value ).toFixed ( 1 ) );

		if ( window.localStorage.getItem ( storage_key ) !== unitselect.val () )
		{
			window.localStorage.setItem ( storage_key , unitselect.val () );
			$("._dynamicunit[data-unit-type='" + du.data ( "unitType" ) + "']").trigger ( "unitPreferenceUpdated" , [ du.data ( "unitType" ) ] );
		}
	} );

	var updatefunction = function ( event , unit_type )
	{
		if ( unit_type !== du.data ( "unitType" ) )
			return;

		unitselect.val ( window.localStorage.getItem ( storage_key ) || du.data ( "unit" ) );
		unitselect.trigger ( "change" );
	};
	du.on ( "unitPreferenceUpdated" , updatefunction );
	updatefunction ( null , du.data ( "unitType" ) );

	return du;
};

} ) ();
