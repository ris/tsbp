(function ()
{

// A GeoJSON layer that can temporarily hide a feature with the given track_index property
// (actually works by adding/removing the sub-layer from the actual map while they all still
// remain members of this._layers)
L.GeoJSON_hidetrackindex = L.GeoJSON.extend ( {
	refreshHidden : function ()
	{
		var outer_layer = this;
		$.each ( this._layers , function ( key , layer )
		{
			if ( !outer_layer._map )
				return;

			// operation relies on removing a layer or adding a layer more than once has no effect
			if ( layer.feature && layer.feature.properties && layer.feature.properties.track_index === outer_layer.hidden_track_index )
				outer_layer._map.removeLayer ( layer );
			else
				outer_layer._map.addLayer ( layer );
		} );
		return this;
	},

	// as in L.GeoJSONM.addData. sigh.
	addData: function (geojson) {
		var features = geojson instanceof Array ? geojson : geojson.features,
		    i, len;

		if (features) {
			for (i = 0, len = features.length; i < len; i++) {
				if ( features[i].geometry != null )
					this.addData(features[i]);
			}
			return this;
		}

		var options = this.options;

		if (options.filter && !options.filter(geojson)) { return; }

		// vvv change made here vvv
		var layer = L.GeoJSON_hidetrackindex.geometryToLayer(geojson, options.pointToLayer);
		layer.feature = geojson;

		this.resetStyle(layer);

		if (options.onEachFeature) {
			options.onEachFeature(geojson, layer);
		}

		return this.addLayer(layer);
	}
} );

L.Util.extend ( L.GeoJSON_hidetrackindex , {
	// Just catch the case where geometry is a LineString and produce a PolylineWithFilter rather than a Polyline
	// ok, perhaps this means GeoJSON_hidetrackindex does more than just the hidetrackindex trick
	geometryToLayer: function (geojson, pointToLayer) {
		var geometry = geojson.type === 'Feature' ? geojson.geometry : geojson,
		    coords = geometry.coordinates,
		    latlngs,
		    m_min, m_max, i, m, c;

		if ( geometry.type === "LineString" ) {
			latlngs = this.coordsToLatLngs(coords);

			return new L.PolylineWithFilter(latlngs);
		}
		else
			return L.GeoJSON.geometryToLayer.apply ( this , arguments );
	}
} );

L.geoJson_hidetrackindex = function (geojson, options) {
	return new L.GeoJSON_hidetrackindex(geojson, options);
};

// a Polyline that will possibly add a filter= attribute to the _container if asked to by the style
L.PolylineWithFilter = L.Polyline.extend ( {
	// yes, again this will only work with the svg backend
	_updateStyle: function () {
		if (this.options.containerfilter) {
			this._container.setAttribute('filter', this.options.containerfilter);
		}
		return L.Polyline.prototype._updateStyle.apply ( this , arguments );
	}
} );

L.polylineWithFilter = function (latlngs, options) {
	return new L.PolylineWithFilter(latlngs, options);
};


// A GeoJSON layer that generates PolylineMs from LineStrings instead of Polylines
L.GeoJSONM = L.GeoJSON.extend ( {
	// sigh. this needs to be copied & overrided simply because the original refers
	// to L.GeoJSON.geometryToLayer directly. which of course stops inheritance working cleanly.
	// this seems to be their way of doing static methods.
	addData: function (geojson) {
		var features = geojson instanceof Array ? geojson : geojson.features,
		    i, len;

		if (features) {
			for (i = 0, len = features.length; i < len; i++) {
				this.addData(features[i]);
			}
			return this;
		}

		var options = this.options;

		if (options.filter && !options.filter(geojson)) { return; }

		// vvv change made here vvv
		var layer = L.GeoJSONM.geometryToLayer(geojson, options.pointToLayer);
		layer.feature = geojson;

		this.resetStyle(layer);

		if (options.onEachFeature) {
			options.onEachFeature(geojson, layer);
		}

		return this.addLayer(layer);
	},

	addLayer: function ()
	{
		if (this._layers[L.Util.stamp(arguments[0])]) {
			return this;
		}

		// don't ask me why L.FeatureGroups only bubble a blessed selection of events. god only knows.
		arguments[0].on('selectionChanged', this._propagateEvent, this);
		arguments[0].on('hoverChanged', this._propagateEvent, this);
		arguments[0].on('pinningChanged', this._propagateEvent, this);
		return L.GeoJSON.prototype.addLayer.apply ( this , arguments );
	},

	removeLayer: function ()
	{
		arguments[0].off('selectionChanged', this._propagateEvent, this);
		arguments[0].off('hoverChanged', this._propagateEvent, this);
		arguments[0].off('pinningChanged', this._propagateEvent, this);
		return L.GeoJSON.prototype.removeLayer.apply ( this , arguments );
	}
} );

L.Util.extend ( L.GeoJSONM , {
	// Just catch the case where geometry is a LineString and produce a PolylineM rather than a Polyline
	geometryToLayer: function (geojson, pointToLayer) {
		var geometry = geojson.type === 'Feature' ? geojson.geometry : geojson,
		    coords = geometry.coordinates,
		    latlngs,
		    m_min, m_max, i, m, c;

		if ( geometry.type === "LineString" ) {
			latlngs = this.coordsToLatLngs(coords);

			// normalize values of m
			// would be nice to combine passes, but the least intrusive way is to do it simply
			for ( i = 0 ; i < latlngs.length ; i++ )
			{
				m_min = m_min == null ? latlngs[i].m : Math.min ( m_min , latlngs[i].m );
				m_max = m_max == null ? latlngs[i].m : Math.max ( m_max , latlngs[i].m );
			}

			m = 1.0 / (m_max - m_min);
			c = -(m_min * m);

			for ( i = 0 ; i < latlngs.length ; i++ )
				latlngs[i].m = ( latlngs[i].m * m ) + c;

			return new L.PolylineM(latlngs);
		}
		else
			// fall through to original implementation
			return L.GeoJSON.geometryToLayer.apply ( this , arguments );
	},

	// this is called by coordsToLatLngs and it's where we need to make sure the "m" gets into the produced LatLng
	coordsToLatLng: function (coords, reverse) { // (Array, Boolean) -> LatLng
		var lat = parseFloat(coords[reverse ? 0 : 1]),
		    lng = parseFloat(coords[reverse ? 1 : 0]);

		return new L.LatLngM(lat, lng, true, parseFloat(coords[2]), parseFloat(coords[3]), coords[4]);
	}
} );

L.geoJsonM = function (geojson, options) {
	return new L.GeoJSONM(geojson, options);
};


// a LatLng that also holds an "m" (and some auxiliary stuff)
L.LatLngM = function ( rawLat , rawLng , noWrap , m , q , c )
{
	L.LatLng.apply ( this , arguments );
	this.m = parseFloat ( m );
	this.q = parseFloat ( q );
	this.c = c;
};
L.LatLngM.prototype = new L.LatLng(0,0); // < javascript is an awful language
L.LatLngM.prototype.toString = function ()
{
	return 'LatLngM(' +
		L.Util.formatNum(this.lat) + ', ' +
		L.Util.formatNum(this.lng) + ', ' +
		"false, " +
		L.Util.formatNum(this.m) + ', ' +
		L.Util.formatNum(this.q) + ', ' +
		this.c +
		')';
};


// A Point that (optionally, I suppose) holds an "m", or "measurement" value for each point (and some auxiliary stuff)
// Arithmetic behaviour is largely unchanged.
L.PointM = function (/*Number*/ x, /*Number*/ y, /*Boolean*/ round, /*Number*/ m, /*Number*/ q, /*Number*/ r, /*Bool*/ s) {
	this.x = (round ? Math.round(x) : x);
	this.y = (round ? Math.round(y) : y);
	this.m = m;
	this.q = q; // used to store (fractional) routable track trackpoint index
	this.r = r; // used to store original linestring index
	this.s = s; // used to store (bool) connectedness with previous point
};

L.PointM.prototype = {

	clone: function () {
		return new L.PointM(this.x, this.y, false, this.m, this.q, this.r, this.s);
	},

	// non-destructive, returns a new point
	add: function (point) {
		return this.clone()._add(L.point(point));
	},

	// destructive, used directly for performance in situations where it's safe to modify existing point
	_add: function (point) {
		this.x += point.x;
		this.y += point.y;
		return this;
	},

	subtract: function (point) {
		return this.clone()._subtract(L.point(point));
	},

	_subtract: function (point) {
		this.x -= point.x;
		this.y -= point.y;
		return this;
	},

	divideBy: function (num) {
		return this.clone()._divideBy(num);
	},

	_divideBy: function (num) {
		this.x /= num;
		this.y /= num;
		return this;
	},

	multiplyBy: function (num) {
		return this.clone()._multiplyBy(num);
	},

	_multiplyBy: function (num) {
		this.x *= num;
		this.y *= num;
		return this;
	},

	round: function () {
		return this.clone()._round();
	},

	_round: function () {
		this.x = Math.round(this.x);
		this.y = Math.round(this.y);
		return this;
	},

	floor: function () {
		return this.clone()._floor();
	},

	_floor: function () {
		this.x = Math.floor(this.x);
		this.y = Math.floor(this.y);
		return this;
	},

	distanceTo: function (point) {
		point = L.point(point);

		var x = point.x - this.x,
			y = point.y - this.y;

		return Math.sqrt(x * x + y * y);
	},

	toString: function () {
		return 'PointM(' +
				L.Util.formatNum(this.x) + ', ' +
				L.Util.formatNum(this.y) + ', ' +
				'false, ' +
				L.Util.formatNum(this.m) + ', ' +
				L.Util.formatNum(this.q) + ', ' +
				L.Util.formatNum(this.r) + ', ' +
				this.s +
				')';
	}
};

L.pointM = function (x, y, round, m, q, r, s) {
	if (x instanceof L.PointM) {
		return x;
	}
	if (x instanceof L.Point) {
		return new L.PointM(x.x, x.y);
	}
	if (x instanceof Array) {
		return new L.PointM(x[0], x[1], false, x[2], x[3], x[4], x[5]);
	}
	if (isNaN(x)) {
		return x;
	}
	return new L.PointM(x, y, round, m, q, r, s);
};


L.LineUtilM = $.extend ( {} , L.LineUtil , {
	// A bastardization of Douglas-Peucker simplification to cope with the m parameter and
	// split the parts at appropriate points
	_simplifyDP: function (points, sqTolerance) {

		var len = points.length,
			ArrayConstructor = typeof Uint8Array !== undefined + '' ? Uint8Array : Array,
			markers = new ArrayConstructor(len);

		markers[0] = markers[len - 1] = 1;

		// first pass like a regular pass but adding the "m" error into the euclidean distance
		this._simplifyDPStep(points, markers, sqTolerance, 0, len - 1, this._sqClosestPointOnSegmentM);

		var i,
			firstpasspoints = [];

		for (i = 0; i < len; i++) {
			if (markers[i]) {
				firstpasspoints.push(points[i]);
			}
		}

		// second pass
		len = firstpasspoints.length
		markers = new ArrayConstructor ( len );
		
		this._simplifyDPStep(firstpasspoints, markers, sqTolerance, 0, len - 1, this._sqClosestPointOnSegmentOnlyM);

		var k,
			secondpassparts = [ [] ];

		for ( i = 0 , k = 0 ; i < len ; i++ )
		{
			secondpassparts[k].push ( firstpasspoints[i] );

			if ( markers[i] && i !== 0 && i !== len-1 )
			{
				k++;

				// notice this is the same actual PointM object that's getting added to
				// both arrays - hope that doesn't cause any trouble
				secondpassparts[k] = [ firstpasspoints[i] ];
			}
		}

		return secondpassparts;
	},

	_simplifyDPStep: function (points, markers, sqTolerance, first, last, distance_function) {
		var maxSqDist = 0,
			index, i, sqDist;

		for (i = first + 1; i <= last - 1; i++) {
			if ( points[i].s != points[i+1].s ) {
				// cheat: this point _must_ be included so we may as well bail early and put the split here for this level
				index = i;
				maxSqDist = Number.POSITIVE_INFINITY;
				break;
			}

			sqDist = distance_function(points[i], points[first], points[last], true, sqTolerance * this._base_m_scale * this._base_m_scale);

			if (sqDist > maxSqDist) {
				index = i;
				maxSqDist = sqDist;
			}
		}

		if (maxSqDist > sqTolerance) {
			markers[index] = 1;

			this._simplifyDPStep(points, markers, sqTolerance, first, index, distance_function);
			this._simplifyDPStep(points, markers, sqTolerance, index, last, distance_function);
		}
	},

	// overriding to take account of m parameter - handling of which is affected by sqTolerance
	// value, so unfortunately can't _just_ override _sqDist
	_reducePoints: function (points, sqTolerance) {
		var reducedPoints = [points[0]];

		for (var i = 1, prev = 0, len = points.length; i < len; i++) {
			// we effectively fix the tolerance in the m dimension by multiplying by the sqTolerance
			// also ensure points *just before* the connectedness changes get included (yes, we're peeking forwards here)
			if ( this._sqDist(points[i], points[prev], sqTolerance * this._base_m_scale * this._base_m_scale) > sqTolerance || ( i < len-1 && points[i].s != points[i+1].s ) ) {
				reducedPoints.push(points[i]);
				prev = i;
			}
		}
		if (prev < len - 1) {
			reducedPoints.push(points[len - 1]);
		}
		return reducedPoints;
	} ,

	// really the (effectively fixed) (inverse) tolerance in the m dimension
	_base_m_scale: 8 ,

	// overriding to take account of m parameter
	_sqDist: function (p1, p2, sq_m_scale) {
		var dx = p2.x - p1.x,
			dy = p2.y - p1.y,
			dm = p2.m - p1.m;
		return dx * dx + dy * dy + dm * dm * sq_m_scale;
	},

	_sqClosestPointOnSegmentM: function (p, p1, p2, sqDist, sq_m_scale) {
		var x = p1.x,
			y = p1.y,
			m = p1.m,
			dx = p2.x - x,
			dy = p2.y - y,
			dm = p2.m - m,
			dot = dx * dx + dy * dy,
			t;

		if (dot > 0) {
			// spatial positioning of point remains the same obviously - doesn't take m into account
			t = ((p.x - x) * dx + (p.y - y) * dy) / dot;

			if (t > 1) {
				x = p2.x;
				y = p2.y;
				m = p2.m;
			} else if (t > 0) {
				x += dx * t;
				y += dy * t;
				m += dm * t;
			}
		}

		dx = p.x - x;
		dy = p.y - y;
		dm = p.m - m;

		return sqDist ? dx * dx + dy * dy + dm * dm * sq_m_scale : new L.PointM(x, y, false, m);
	},

	_sqClosestPointOnSegmentOnlyM: function (p, p1, p2, sqDist, sq_m_scale) {
		var x = p1.x,
			y = p1.y,
			m = p1.m,
			dx = p2.x - x,
			dy = p2.y - y,
			dm = p2.m - m,
			dot = dx * dx + dy * dy,
			t;

		if (dot > 0) {
			// spatial positioning of point remains the same obviously - doesn't take m into account
			t = ((p.x - x) * dx + (p.y - y) * dy) / dot;

			if (t > 1) {
				x = p2.x;
				y = p2.y;
				m = p2.m;
			} else if (t > 0) {
				x += dx * t;
				y += dy * t;
				m += dm * t;
			}
		}

		dm = p.m - m;

		return sqDist ? dm * dm * sq_m_scale : new L.PointM(x, y, false, m);
	},

	// This is called by LineUtil's clipSegment - we just have to make sure it
	// returns our PointM (with a properly interpolated M) instead of a Point
	_getEdgeIntersection: function (a, b, code, bounds) {
		var dx = b.x - a.x,
			dy = b.y - a.y,
			min = bounds.min,
			max = bounds.max,
			t;

		if (code & 8) { // top
			t = (max.y - a.y) / dy;
			return new L.PointM(a.x + dx * t, max.y, false, (a.m||0)*(1-t) + (b.m||0)*t, a.q*(1-t) + b.q*t, a.r*(1-t) + b.r*t, b.s);
		} else if (code & 4) { // bottom
			t = (min.y - a.y) / dy;
			return new L.PointM(a.x + dx * t, min.y, false, (a.m||0)*(1-t) + (b.m||0)*t, a.q*(1-t) + b.q*t, a.r*(1-t) + b.r*t, b.s);
		} else if (code & 2) { // right
			t = (max.x - a.x) / dx;
			return new L.PointM(max.x, a.y + dy * t, false, (a.m||0)*(1-t) + (b.m||0)*t, a.q*(1-t) + b.q*t, a.r*(1-t) + b.r*t, b.s);
		} else if (code & 1) { // left
			t = (min.x - a.x) / dx;
			return new L.PointM(min.x, a.y + dy * t, false, (a.m||0)*(1-t) + (b.m||0)*t, a.q*(1-t) + b.q*t, a.r*(1-t) + b.r*t, b.s);
		}
	}
} );


L.PolylineM = L.Polyline.extend ( {
	// overriding this method to re-point to our LineUtilM
	_clipPoints: function () {
		var points = this._originalPoints,
			len = points.length,
			i, k, segment;

		if (this.options.noClip) {
			this._parts = [points];
			return;
		}

		this._parts = [];

		var parts = this._parts,
			vp = this._map._pathViewport,
			lu = L.LineUtilM;

		for (i = 0, k = 0; i < len - 1; i++) {
			segment = lu.clipSegment(points[i], points[i + 1], vp, i);
			if (!segment) {
				continue;
			}

			parts[k] = parts[k] || [];
			parts[k].push(segment[0]);

			// if segment goes out of screen, or it's the last one, it's the end of the line part
			if ((segment[1] !== points[i + 1]) || (i === len - 2)) {
				parts[k].push(segment[1]);
				k++;
			}
		}
	},

	// Instead of following the Point creation all the way into the _map, we're just going to let latLngToLayerPoint do its thing
	// then convert that result to a PointM
	projectLatlngs: function () {
		var p;
		this._originalPoints = [];

		for (var i = 0, len = this._latlngs.length; i < len; i++) {
			p = this._map.latLngToLayerPoint(this._latlngs[i]);
			this._originalPoints[i] = new L.PointM ( p.x , p.y , false , this._latlngs[i].m, this._latlngs[i].q, i , this._latlngs[i].c);
		}
		
		this._originalGPXPoints = [];
		if ( this.feature.properties.gpxpoints )
			for ( var i = 0 ; i < this.feature.properties.gpxpoints.geometry.coordinates.length ; i++ )
				this._originalGPXPoints.push ( this._map.latLngToLayerPoint ( L.latLng ( this.feature.properties.gpxpoints.geometry.coordinates[i][1] , this.feature.properties.gpxpoints.geometry.coordinates[i][0] ) ) );
	},

	// Overriding this method to allow us to flatten further-split parts into this._parts
	// (and also point at our LineUtilM)
	_simplifyPoints: function () {
		var parts = this._parts,
			lu = L.LineUtilM;

		// have to iterate backwards along this as we'll be inserting items into the array
		// and so changing indexes of following items
		for (var i = parts.length-1; i >= 0; i--) {
			// splice returned parts into parts
			parts.splice.apply ( parts , [ i , 1 ].concat ( lu.simplify(parts[i], this.options.smoothFactor) ) );
		}
	},

	onAdd: function ()
	{
		this._selectedTrackIndexes = [];
		this._preSelectedTrackIndexes = [];
		this._hoveredTrackIndexes = [];
		this._candidates = [];

		var r = L.Polyline.prototype.onAdd.apply ( this , arguments );

		this._originalPointsIndexes = [];
		for ( var i = 0 , j = 0 ; i < this._originalPoints.length ; i++ )
			if ( this._originalPoints[i].q >= j )
			{
				this._originalPointsIndexes.push ( i );
				j++;
			}

		return r;
	},

	_routableIndexFromTrackIndex: function ( i )
	{
		return this.feature.properties.fullindexes.bisect ( i );
	},

	_trackIndexFromRoutableIndex: function ( i )
	{
		return this.feature.properties.fullindexes[i];
	},

	_getColor0: function ()
	{
		// sigh. yes. defining colors as [r,g,b] arrays. i need to do some interpolation and parsing
		// the various svg/css color formats is too much bother without some utility code i don't want
		// to have to depend on.
		return this.options.color0 || [ 0 , 0 , 0 ];
	},

	_getColor1: function ()
	{
		return this.options.color1 || [ 255 , 255 , 255 ];
	},

	_getInterpolatedColor: function ( f )
	{
		var color0 = this._getColor0 ();
		var color1 = this._getColor1 ();

		var stopcolor = [ (1-f) * color0[0] + f * color1[0] , (1-f) * color0[1] + f * color1[1] , (1-f) * color0[2] + f * color1[2] ];

		return this._cssColorString ( stopcolor );
	},

	_cssColorString: function ( value_array )
	{
		if ( value_array.length === 4 )
			return "rgba(" + Math.round(value_array[0]) + "," + Math.round(value_array[1]) + "," + Math.round(value_array[2]) + "," + value_array[3] + ")";
		else
			return "rgb(" + Math.round(value_array[0]) + "," + Math.round(value_array[1]) + "," + Math.round(value_array[2]) + ")";
	},

	_getCrossW: function ()
	{
		return 2;
	},

	_getGPXCircleRadius: function ()
	{
		return this.options.gpxcircleradius != null ? this.options.gpxcircleradius : 2;
	},
	
	_getCandidateCircleRadius: function ()
	{
		return this.options.candidatecircleradius != null ? this.options.candidatecircleradius : 3;
	},

	_crossPathD: function ( point )
	{
		var cross_w = this._getCrossW ();
		return "M"+(point.x-cross_w)+" "+(point.y-cross_w)+"l4 4m0 -4l-4 4";
	},

	// a gaussian curve, sigma 1.8, with a central spike
	_GPXHoverPrePostOpacityLUT: [ 0.09974088 , 0.215763 , 0.34279876 , 0.8 , 0.34279876 , 0.215763 , 0.09974088 ] ,

	_indexArrayValid: function ( index_array )
	{
		if ( index_array.length > 2 )
			return false;

		if ( index_array.length === 2 )
		{
			if ( index_array[0] === index_array[1] )
				return false;
			else if ( index_array[0] > index_array[1] )
				return false;
		}

		if ( index_array[0] != null && index_array[0] < 0 )
		{
			return false;
		}
		
		return true;
	},

	_getSelectionBounds: function ( fallback_to_all )
	{
		// Note we're using LatLngs everywhere here as (layer)Points have been rounded and lost too much precision to always give a good result
		var p , i , track_index , routable_index , bounds_accum , end_routable_index , start_routable_index;
		var selectedTrackIndexes = this._selectedTrackIndexes;
		if ( ! ( selectedTrackIndexes && selectedTrackIndexes.length ) )
		{
			if ( ! fallback_to_all )
				return null;
			
			selectedTrackIndexes = this.feature.properties.gpxpoints.geometry.coordinates.length > 1 ? [ 0 , this.feature.properties.gpxpoints.geometry.coordinates.length-1 ] : [ 0 ];
		}

		if ( selectedTrackIndexes.length === 1 )
		{
			track_index = selectedTrackIndexes[0];
			routable_index = this._routableIndexFromTrackIndex ( track_index );
			if ( this._trackIndexFromRoutableIndex ( routable_index-1 ) === track_index )
				// point is in routable track
				return new L.LatLngBounds ( [ this._latlngs[this._originalPointsIndexes[routable_index-1]] , L.latLng ( this.feature.properties.gpxpoints.geometry.coordinates[track_index][1] , this.feature.properties.gpxpoints.geometry.coordinates[track_index][0] ) ] );
			else
				// point is null point
				return new L.LatLngBounds ( [ L.latLng ( this.feature.properties.gpxpoints.geometry.coordinates[track_index][1] , this.feature.properties.gpxpoints.geometry.coordinates[track_index][0] ) ] );
		}

		// aggregate points on routable path
		start_routable_index = Math.max ( this._routableIndexFromTrackIndex ( selectedTrackIndexes[0] )-1 , 0 );
		if ( this._trackIndexFromRoutableIndex ( start_routable_index ) < selectedTrackIndexes[0] )
			start_routable_index++;
		end_routable_index = this._routableIndexFromTrackIndex ( selectedTrackIndexes[1] )-1;
		for ( i = this._originalPointsIndexes[start_routable_index] ; i < this._latlngs.length && this._latlngs[i].q <= end_routable_index ; i++ )
		{
			if ( bounds_accum == null )
				bounds_accum = new L.LatLngBounds ( [ this._latlngs[i] ] );
			else
				bounds_accum.extend ( this._latlngs[i] );
		}

		// aggregate gpx points
		for ( i = selectedTrackIndexes[0] ; i <= selectedTrackIndexes[1] ; i++ )
		{
			if ( bounds_accum == null )
				bounds_accum = new L.LatLngBounds ( [ L.latLng ( this.feature.properties.gpxpoints.geometry.coordinates[i][1] , this.feature.properties.gpxpoints.geometry.coordinates[i][0] ) ] );
			else
				bounds_accum.extend ( L.latLng ( this.feature.properties.gpxpoints.geometry.coordinates[i][1] , this.feature.properties.gpxpoints.geometry.coordinates[i][0] ) );
		}

		return bounds_accum;
	},

	fitSelectionBounds: function ( fallback_to_all )
	{
		var bounds = this._getSelectionBounds ( fallback_to_all );
		if ( bounds == null )
			return;

		this._map.fitBounds ( bounds );
	},

	expandToSelectionBounds: function ()
	{
		var bounds = this._getSelectionBounds ();
		if ( bounds == null )
			return;

		var map_bounds = this._map.getBounds ();
		if ( ! map_bounds.contains ( bounds ) )
			this._map.fitBounds ( bounds.extend ( map_bounds ) );
	},

	setSelection: function ( index_array )
	{
		if ( index_array[0] === this._selectedTrackIndexes[0] &&
			index_array[1] === this._selectedTrackIndexes[1] )
			// nothing's changed.
			return;

		// just going to bail if any error conditions are found
		if ( ! this._indexArrayValid ( index_array ) )
			return;

		this._selectedTrackIndexes = [];

		if ( index_array[0] != null )
		{
			this._selectedTrackIndexes.push ( index_array[0] );

			if ( index_array[1] != null )
			{
				this._selectedTrackIndexes.push ( index_array[1] );
			}
		}

		this._candidates = [];
		this._pinning = null;
		this._solution_candidate_index = null;

		this.fire ( "selectionChanged" , { trackIndexes : this._selectedTrackIndexes } );

		this.expandToSelectionBounds ();

		this._updateSelectionCircles ();
		this._updateHoverCircles ();
		this._updateSelectionUnderPaths ();
		
		this._updateCandidatePoints ();
	},

	_setPreSelection: function ( index_array , valid )
	{
		// @valid - whether releasing the mouse here would cause the selection to proceed

		if ( index_array[0] === this._preSelectedTrackIndexes[0] &&
			index_array[1] === this._preSelectedTrackIndexes[1] &&
				!!valid == !!this._preSelectedValid )
			// nothing's changed.
			return;
		
		// just going to bail if any error conditions are found
		if ( ! this._indexArrayValid ( index_array ) )
			return;

		this._preSelectedTrackIndexes = [];
		this._preSelectedValid = valid;

		if ( index_array[0] != null )
		{
			this._preSelectedTrackIndexes.push ( index_array[0] );

			if ( index_array[1] != null )
			{
				this._preSelectedTrackIndexes.push ( index_array[1] );
			}
		}

		this._updateSelectionCircles ();
		this._updateHoverCircles ();
		this._updatePreSelectionUnderPaths ();
	},

	setHover: function ( index_array )
	{
		if ( index_array[0] === this._hoveredTrackIndexes[0] &&
			index_array[1] === this._hoveredTrackIndexes[1] )
			// nothing's changed.
			return;

		// just going to bail if any error conditions are found
		if ( ! this._indexArrayValid ( index_array ) )
			return;

		this._hoveredTrackIndexes = [];

		if ( index_array[0] != null )
		{
			this._hoveredTrackIndexes.push ( index_array[0] );

			if ( index_array[1] != null )
			{
				this._hoveredTrackIndexes.push ( index_array[1] );
			}
		}

		this.fire ( "hoverChanged" , { trackIndexes : this._hoveredTrackIndexes } );

		this._updateHoverCircles ();
	},
	
	setCandidates: function ( candidates , pinning , solution_candidate_index )
	{
		this._candidates = candidates.slice ();
		this._pinning = pinning;
		this._solution_candidate_index = solution_candidate_index;
		
		this._updateCandidatePoints ( true );
	},
	
	setPinning: function ( pinning )
	{
		if ( pinning == this._pinning )
			// nothing changed - don't let this propagate
			return;
		
		this._pinning = pinning;
		
		var context_obj = {
			trackIndex : this._selectedTrackIndexes[0] ,
			pinning : pinning ,
		};
		if ( this._pinning >= 0 )
			context_obj.annotation = this._candidates[this._pinning];
		this.fire ( "pinningChanged" , context_obj );
		
		this._updateCandidatePoints ();
	},

	_updateSelectionCircles: function ()
	{
		//
		// actually (re-)generate the svg elements corresponding to entries in this._*TrackIndexes
		//

		var pointm , point , circle , i , routable_index , track_index , path , path_d , cross_w = this._getCrossW ();

		// get rid of any existing _selectedCircles elements
		while ( this._selectedCircles.length )
			this._selectedContainer.removeChild ( this._selectedCircles.pop () );
		this._selectedCirclesIndexes = [];

		// get rid of any existing _preSelectedCircles elements
		while ( this._preSelectedCircles.length )
			this._preSelectedContainer.removeChild ( this._preSelectedCircles.pop () );
		this._preSelectedCirclesIndexes = [];

		if ( this._preSelectedTrackIndexes && this._preSelectedTrackIndexes.length )
			for ( i = 0 ; i < this._preSelectedTrackIndexes.length ; i++ )
			{
				track_index = this._preSelectedTrackIndexes[i];
				routable_index = this._routableIndexFromTrackIndex ( track_index );
				if ( this._trackIndexFromRoutableIndex ( routable_index-1 ) === track_index )
				{
					// draw a pre-selected indicator on the path
					pointm = this._originalPoints[this._originalPointsIndexes[routable_index-1]];
					circle = this._createElement ( "circle" );
					circle.setAttribute ( "r" , "5" );
					circle.setAttribute ( "cx" , pointm.x );
					circle.setAttribute ( "cy" , pointm.y );
					circle.setAttribute ( "fill" , this._preSelectedValid ? "white" : "none" );
					circle.setAttribute ( "stroke" , this._getInterpolatedColor ( pointm.m ) );
					this._preSelectedCircles.push ( circle );
					this._preSelectedCirclesIndexes.push ( track_index );
					this._preSelectedContainer.appendChild ( circle );
				}
			}
		else
		{
			for ( i = 0 ; i < this._selectedTrackIndexes.length ; i++ )
			{
				track_index = this._selectedTrackIndexes[i];
				routable_index = this._routableIndexFromTrackIndex ( track_index );
				if ( this._trackIndexFromRoutableIndex ( routable_index-1 ) === track_index )
				{
					// draw a normal selected indicator
					pointm = this._originalPoints[this._originalPointsIndexes[routable_index-1]];
					circle = this._createElement ( "circle" );
					circle.setAttribute ( "r" , "5" );
					circle.setAttribute ( "cx" , pointm.x );
					circle.setAttribute ( "cy" , pointm.y );
					circle.setAttribute ( "fill" , "white" );
					circle.setAttribute ( "stroke" , this._getInterpolatedColor ( pointm.m ) );
					this._selectedCircles.push ( circle );
					this._selectedCirclesIndexes.push ( track_index );
					this._selectedContainer.appendChild ( circle );
				}
			}
		}
		
		this._updateSolutionCandidateIndexTooltip ();

		var last_plotted_point , last_plotted_null_point , gpx_circle_r = this._getGPXCircleRadius () , gpx_circle_r_sq = gpx_circle_r*gpx_circle_r , j , k , end_track_index , cross_w_sq = cross_w*cross_w;

		if ( this._preSelectedTrackIndexes.length )
		{
			last_plotted_point = last_plotted_null_point = null;
			end_track_index = this._preSelectedTrackIndexes[1] || this._preSelectedTrackIndexes[0];
			for ( i = this._preSelectedTrackIndexes[0] , j = 0 , k = 0 ; i <= end_track_index ; i++ )
			{
				point = this._originalGPXPoints[i];
				routable_index = this._routableIndexFromTrackIndex ( i );

				if ( ! this._map._pathViewport.contains ( point ) )
					// we can skip drawing this
					continue;

				if ( this._trackIndexFromRoutableIndex ( routable_index-1 ) === i )
				{
					// this is a non-null trackpoint
				}
				else
				{
					// this is a null trackpoint
					if ( last_plotted_null_point && L.LineUtil._sqDist ( point , last_plotted_null_point ) < cross_w_sq
						&& i !== end_track_index // this isn't the last of the selection
						&& this._trackIndexFromRoutableIndex ( routable_index-1 ) !== i-1 // this isn't the first of a run of null points
						&& this._trackIndexFromRoutableIndex ( routable_index ) !== i+1 ) // this isn't the last of a run of null points
						// we can skip drawing this
						continue;
					
					path_d = this._crossPathD ( point );

					if ( ! this._preSelectedNullCrosses[k] )
					{
						this._preSelectedNullCrosses[k] = this._createElement ( "path" );
						this._preSelectedNullPointsContainer.appendChild ( this._preSelectedNullCrosses[k] );
					}

					path = this._preSelectedNullCrosses[k];

					this._preSelectedNullCrossesIndexes[k] = i;
	
					path.setAttribute ( "d" , path_d );


					if ( ! this._preSelectedNullCrossHighlights[k] )
					{
						this._preSelectedNullCrossHighlights[k] = this._createElement ( "path" );
						this._preSelectedNullPointHighlightsContainer.appendChild ( this._preSelectedNullCrossHighlights[k] );
					}

					path = this._preSelectedNullCrossHighlights[k];

					this._preSelectedNullCrossHighlightsIndexes[k] = i;
	
					path.setAttribute ( "d" , path_d );


					last_plotted_null_point = point;
					k++;
				}
			}


			if ( k )
			{
				// remove surplus elements
				while ( this._preSelectedNullCrosses.length > k )
					this._preSelectedNullPointsContainer.removeChild ( this._preSelectedNullCrosses.pop () );
		
				// remove surplus indexes
				while ( this._preSelectedNullCrossesIndexes.length > k )
					this._preSelectedNullCrossesIndexes.pop ();


				// remove surplus elements
				while ( this._preSelectedNullCrossHighlights.length > k )
					this._preSelectedNullPointHighlightsContainer.removeChild ( this._preSelectedNullCrossHighlights.pop () );
		
				// remove surplus indexes
				while ( this._preSelectedNullCrossHighlightsIndexes.length > k )
					this._preSelectedNullCrossHighlightsIndexes.pop ();


				this._preSelectedNullPointsContainer.setAttribute ( "display" , "inline" );
				this._preSelectedNullPointHighlightsContainer.setAttribute ( "display" , "inline" );
			}
			else
			{
				this._preSelectedNullPointsContainer.setAttribute ( "display" , "none" );
				this._preSelectedNullPointHighlightsContainer.setAttribute ( "display" , "none" );
			}
		}
		else
		{
			// simply hide the containers, saving us having to delete a bunch of elements we'll just have to recreate again
			this._preSelectedNullPointsContainer.setAttribute ( "display" , "none" );
			this._preSelectedNullPointHighlightsContainer.setAttribute ( "display" , "none" );
		}

		if ( this._selectedTrackIndexes.length )
		{
			last_plotted_point = last_plotted_null_point = null;
			end_track_index = this._selectedTrackIndexes[1] || this._selectedTrackIndexes[0];
			for ( i = this._selectedTrackIndexes[0] , j = 0 , k = 0 ; i <= end_track_index ; i++ )
			{
				point = this._originalGPXPoints[i];
				routable_index = this._routableIndexFromTrackIndex ( i );

				if ( ! this._map._pathViewport.contains ( point ) )
					// we can skip drawing this
					continue;

				if ( this._trackIndexFromRoutableIndex ( routable_index-1 ) === i )
				{
					// this is a non-null trackpoint
					if ( last_plotted_point && L.LineUtil._sqDist ( point , last_plotted_point ) < gpx_circle_r_sq
						&& i !== end_track_index // this isn't the last of the selection
						&& this._trackIndexFromRoutableIndex ( routable_index-2 ) === i-1 // this isn't the first of a run of non-null points
						&& this._trackIndexFromRoutableIndex ( routable_index ) === i+1 ) // this isn't the last of a run of non-null points
						// we can skip drawing this
						continue;
					
					if ( ! this._GPXCircles[j] )
					{
						this._GPXCircles[j] = this._createElement ( "circle" );
						this._GPXPointsContainer.appendChild ( this._GPXCircles[j] );
					}
	
					circle = this._GPXCircles[j];
	
					this._GPXCirclesIndexes[j] = i;
	
					circle.setAttribute ( "r" , gpx_circle_r );
					circle.setAttribute ( "cx" , point.x );
					circle.setAttribute ( "cy" , point.y );
	
					last_plotted_point = point;
					j++;
				}
				else
				{
					// this is a null trackpoint
					if ( last_plotted_null_point && L.LineUtil._sqDist ( point , last_plotted_null_point ) < cross_w_sq
						&& i !== end_track_index // this isn't the last of the selection
						&& this._trackIndexFromRoutableIndex ( routable_index-1 ) !== i-1 // this isn't the first of a run of null points
						&& this._trackIndexFromRoutableIndex ( routable_index ) !== i+1 ) // this isn't the last of a run of null points
						// we can skip drawing this
						continue;
					
					path_d = this._crossPathD ( point );

					if ( ! this._selectedNullCrosses[k] )
					{
						this._selectedNullCrosses[k] = this._createElement ( "path" );
						this._selectedNullPointsContainer.appendChild ( this._selectedNullCrosses[k] );
					}

					path = this._selectedNullCrosses[k];

					this._selectedNullCrossesIndexes[k] = i;
	
					path.setAttribute ( "d" , path_d );


					if ( ! this._selectedNullCrossHighlights[k] )
					{
						this._selectedNullCrossHighlights[k] = this._createElement ( "path" );
						this._selectedNullPointHighlightsContainer.appendChild ( this._selectedNullCrossHighlights[k] );
					}

					path = this._selectedNullCrossHighlights[k];

					this._selectedNullCrossHighlightsIndexes[k] = i;
	
					path.setAttribute ( "d" , path_d );


					last_plotted_null_point = point;
					k++;
				}
			}

			if ( j )
			{
				// remove surplus elements
				while ( this._GPXCircles.length > j )
					this._GPXPointsContainer.removeChild ( this._GPXCircles.pop () );
		
				// remove surplus indexes
				while ( this._GPXCirclesIndexes.length > j )
					this._GPXCirclesIndexes.pop ();
	
				this._GPXPointsContainer.setAttribute ( "display" , "inline" );
			}
			else
				this._GPXPointsContainer.setAttribute ( "display" , "none" );


			if ( k )
			{
				// remove surplus elements
				while ( this._selectedNullCrosses.length > k )
					this._selectedNullPointsContainer.removeChild ( this._selectedNullCrosses.pop () );
		
				// remove surplus indexes
				while ( this._selectedNullCrossesIndexes.length > k )
					this._selectedNullCrossesIndexes.pop ();


				// remove surplus elements
				while ( this._selectedNullCrossHighlights.length > k )
					this._selectedNullPointHighlightsContainer.removeChild ( this._selectedNullCrossHighlights.pop () );
		
				// remove surplus indexes
				while ( this._selectedNullCrossHighlightsIndexes.length > k )
					this._selectedNullCrossHighlightsIndexes.pop ();


				this._selectedNullPointsContainer.setAttribute ( "display" , "inline" );
				this._selectedNullPointHighlightsContainer.setAttribute ( "display" , "inline" );
			}
			else
			{
				this._selectedNullPointsContainer.setAttribute ( "display" , "none" );
				this._selectedNullPointHighlightsContainer.setAttribute ( "display" , "none" );
			}
		}
		else
		{
			// simply hide the container, saving us having to delete a bunch of elements we'll just have to recreate again
			this._GPXPointsContainer.setAttribute ( "display" , "none" );

			this._selectedNullPointsContainer.setAttribute ( "display" , "none" );
			this._selectedNullPointHighlightsContainer.setAttribute ( "display" , "none" );
		}
	},

	_updateHoverCircles: function ()
	{
		//
		// As above but for hover indicators
		//
		var i , j , lut_offset , track_index , inner_track_index , routable_index , pointm , point , circle , path_d , path , gpx_circle_r = this._getGPXCircleRadius ();

		// get rid of any existing _hoveredCircles elements
		while ( this._hoveredCircles.length )
			this._hoveredContainer.removeChild ( this._hoveredCircles.pop () );
		this._hoveredCirclesIndexes = [];

		// get rid of any existing _hoveredGPXCircles elements
		while ( this._hoveredGPXCircles.length )
			this._hoveredGPXCirclesContainer.removeChild ( this._hoveredGPXCircles.pop () );
		this._hoveredGPXCirclesIndexes = [];

		// get rid of any existing _hoveredNullCrosses elements
		while ( this._hoveredNullCrosses.length )
			this._hoveredNullPointsContainer.removeChild ( this._hoveredNullCrosses.pop () );
		this._hoveredNullCrossesIndexes = [];

		// get rid of any existing _hoveredNullCrossHighlights elements
		while ( this._hoveredNullCrossHighlights.length )
			this._hoveredNullPointHighlightsContainer.removeChild ( this._hoveredNullCrossHighlights.pop () );
		this._hoveredNullCrossHighlightsIndexes = [];

		for ( i = 0 ; i < this._hoveredTrackIndexes.length ; i++ )
		{
			track_index = this._hoveredTrackIndexes[i];
			routable_index = this._routableIndexFromTrackIndex ( track_index );
			if ( this._trackIndexFromRoutableIndex ( routable_index-1 ) === track_index )
			{
				// draw a short string of gpx points before & after hovered point
				lut_offset = this._GPXHoverPrePostOpacityLUT.length>>1;
				// iterate through entries in lookup table
				for ( j = 0 ; j < this._GPXHoverPrePostOpacityLUT.length ; j++ )
				{
					inner_track_index = track_index + j - lut_offset;
					if ( inner_track_index >= 0 && inner_track_index < this._originalGPXPoints.length && this._trackIndexFromRoutableIndex ( (routable_index+j)-(lut_offset+1) ) === inner_track_index )
					{
						// ok, it seems valid, let's draw it
						point = this._originalGPXPoints[inner_track_index];
						circle = this._createElement ( "circle" );
						circle.setAttribute ( "r" , gpx_circle_r );
						circle.setAttribute ( "cx" , point.x );
						circle.setAttribute ( "cy" , point.y );
						circle.setAttribute ( "opacity" , this._GPXHoverPrePostOpacityLUT[j] );
						this._hoveredGPXCircles.push ( circle );
						this._hoveredGPXCirclesIndexes.push ( inner_track_index );
						this._hoveredGPXCirclesContainer.appendChild ( circle );
					}
				}

				if ( this._selectedTrackIndexes.indexOf ( track_index ) === -1 )
				{
					// draw a normal hover circle indicator
					pointm = this._originalPoints[this._originalPointsIndexes[routable_index-1]];
					circle = this._createElement ( "circle" );
					circle.setAttribute ( "r" , "5" );
					circle.setAttribute ( "cx" , pointm.x );
					circle.setAttribute ( "cy" , pointm.y );
					circle.setAttribute ( "fill" , this._getInterpolatedColor ( pointm.m ) );
					circle.setAttribute ( "stroke" , "none" );
					this._hoveredCircles.push ( circle );
					this._hoveredCirclesIndexes.push ( track_index );
					this._hoveredContainer.appendChild ( circle );
				}
				// else let's not conceal the selected circle on this point
			}
			else
			{
				point = this._originalGPXPoints[track_index];
				path_d = this._crossPathD ( point );

				path = this._createElement ( "path" );
				path.setAttribute ( "d" , path_d );
				this._hoveredNullCrosses.push ( path );
				this._hoveredNullCrossesIndexes.push ( track_index );
				this._hoveredNullPointsContainer.appendChild ( path );

				path = this._createElement ( "path" );
				path.setAttribute ( "d" , path_d );
				this._hoveredNullCrossHighlights.push ( path );
				this._hoveredNullCrossHighlightsIndexes.push ( track_index );
				this._hoveredNullPointHighlightsContainer.appendChild ( path );
			}
		}
	},
	
	_updateSolutionCandidateIndexTooltip: function ()
	{
		// wherever we're called from, this should add a tooltip containing the relevant text whenever it's appropriate (passes the following test):
		if ( this._selectedCircles[0] && this._solution_candidate_index != null && this._candidates[this._solution_candidate_index] )
		{
			this._selectedCircles[0].setAttribute ( "title" , "Solution picked " + ( window.tsbp.candidate_description ( this._candidates[this._solution_candidate_index] ) || ( "candidate point " + this._solution_candidate_index ) ) + " here" );
			// normal caveat about using jquery & tipsy from leaflet
			$(this._selectedCircles[0]).tipsy ( tsbp.tsbp_tipsy_parameters );
		}
	},
	
	_updateCandidatePoints: function ( set_initial )
	{
		// get rid of any existing _candidatePoints elements
		while ( this._candidatePoints.length )
			this._candidatePointsContainer.removeChild ( this._candidatePoints.pop () );
		
		for ( i = 0 , len = this._candidates.length ; i < len ; i++ )
			// would like to keep jquery-using stuff out of leaflet-y stuff but here
			// other solutions seem impractical
			$(this._candidateCircles[i]).tipsy ( "hide" );
		
		// also flush this
		this._candidateCircles = [];
		
		var i , len , circle , g , point , line , dx , dy , factor , hypot , candidate_circle_r = this._getCandidateCircleRadius ();
		
		var gpx_point = this._selectedTrackIndexes[0] != null && this._originalGPXPoints[this._selectedTrackIndexes[0]];
		
		// give css transforms a little help
		this._candidatePointsContainer.style.transformOrigin = this._candidatePointsContainer.style.webkitTransformOrigin = gpx_point.x + "px " + gpx_point.y + "px";
		
		for ( i = 0 , len = this._candidates.length ; i < len ; i++ )
		{
			point = this._map.latLngToLayerPoint ( L.latLng ( this._candidates[i].c[1] , this._candidates[i].c[0] ) );
			g = this._createElement ( "g" );
			circle = this._createElement ( "circle" );
			circle.setAttribute ( "r" , candidate_circle_r );
			circle.setAttribute ( "cx" , point.x );
			circle.setAttribute ( "cy" , point.y );
			circle.setAttribute ( "pointer-events" , "visible" );
			// css transforms seem to need a little more help on firefox at least
			circle.style.transformOrigin = circle.style.webkitTransformOrigin = point.x + "px " + point.y + "px";
			g.appendChild ( circle );
			
			if ( gpx_point )
			{
				dx = point.x - gpx_point.x;
				dy = point.y - gpx_point.y;
				hypot = Math.sqrt ( dx*dx + dy*dy );
				factor = 1.0 - ( hypot ? ( candidate_circle_r / hypot ) : 0 );
				
				line = this._createElement ( "line" );
				line.setAttribute ( "x1" , gpx_point.x );
				line.setAttribute ( "y1" , gpx_point.y );
				line.setAttribute ( "x2" , gpx_point.x + dx * factor );
				line.setAttribute ( "y2" , gpx_point.y + dy * factor );
				line.setAttribute ( "pointer-events" , "none" );
				g.appendChild ( line );
			}
			
			circle.setAttribute ( "title" , ( i === this._pinning ? "Unpin from " : "Pin to " ) + ( window.tsbp.candidate_description ( this._candidates[i] ) || ( "candidate point " + i ) ) );
			
			if ( i === this._pinning )
				g.setAttribute ( "class" , "pinned" );
			
			this._candidatePoints.push ( g );
			this._candidateCircles.push ( circle );
			this._candidatePointsContainer.appendChild ( g );
			
			// would like to keep jquery-using stuff out of leaflet-y stuff but here
			// other solutions seem impractical
			$(circle).tipsy ( tsbp.tsbp_tipsy_parameters );
		}
		
		this._updateSolutionCandidateIndexTooltip ();
		
		if ( set_initial )
		{
			//
			// now some trickery to allow a css transition to be applied to the "appearance" of the candidates
			//
			
			// add "initial" class
			var previous_class = this._candidatePointsContainer.getAttribute ( "class" );
			this._candidatePointsContainer.setAttribute ( "class" , previous_class + " initial" );
			
			// trick layout into being recalculated with new "initial" conditions
			window.getComputedStyle ( this._candidatePointsContainer ).opacity;
			
			// remove "initial" class
			this._candidatePointsContainer.setAttribute ( "class" , previous_class );
		}
	},

	_nearestSegmentIndex: function ( point_array , target_point )
	{
		var i , len , dist , min_dist , min_dist_i = 0;
		for ( i = 0 , len = point_array.length ; i < len-1 ; i++ )
		{
			dist = L.LineUtil.pointToSegmentDistance ( target_point , point_array[i] , point_array[i+1] );

			if ( min_dist == null || dist < min_dist )
			{
				min_dist = dist;
				min_dist_i = i;
			}
		}
		return min_dist_i;
	},

	_updateNullCrosses: function ()
	{
		var i , j , k , path , point , last_plotted_point , cross_w = 2 , cross_w_sq = cross_w*cross_w;

		// note we allow the condition when i == array length to catch trailing null points
		for ( i = 0 , j = 0 , k = 0 ; i <= this.feature.properties.fullindexes.length ; i++ , j++ )
		{
			for ( ; this._trackIndexFromRoutableIndex ( i ) !== j && j < this._originalGPXPoints.length ; j++ )
			{
				point = this._originalGPXPoints[j];

				if ( ! this._map._pathViewport.contains ( point ) )
					// we can skip drawing this
					continue;

				if ( last_plotted_point && L.LineUtil._sqDist ( last_plotted_point , point ) < cross_w_sq
					&& this._trackIndexFromRoutableIndex ( i-1 ) !== j-1 // last point is not null
					&& this._trackIndexFromRoutableIndex ( i ) !== j+1 // next point is not null
					&& j+1 !== this._originalGPXPoints.length ) // this isn't the last point
					// we can skip drawing this
					continue;

				if ( this._nullCrosses[k] == null )
				{
					path = this._createElement( "path" );
					this._nullPointsContainer.appendChild ( path );
					this._nullCrosses[k] = path;
				}
				else
					path = this._nullCrosses[k];

				path.setAttribute ( "d" , this._crossPathD ( point ) );

				last_plotted_point = point;
				this._nullCrossesIndexes[k] = j;
				k++;
			}
		}

		// remove surplus elements
		while ( this._nullCrosses.length > k )
			this._nullPointsContainer.removeChild ( this._nullCrosses.pop () );

		// remove surplus indexes
		while ( this._nullCrossesIndexes.length > k )
			this._nullCrossesIndexes.pop ();
	},

	_commonUpdateSelectionUnderPaths: function ( interval_track_indexes , under_paths , under_paths_container , force_no_selection )
	{
		while ( under_paths.length )
			under_paths_container.removeChild ( under_paths.pop () );

		if ( interval_track_indexes.length <= 1 || !!force_no_selection )
			return;

		var start_routable_index = Math.max ( this._routableIndexFromTrackIndex ( interval_track_indexes[0] )-1 , 0 );
		if ( this._trackIndexFromRoutableIndex ( start_routable_index ) < interval_track_indexes[0] )
			start_routable_index++;
		var end_routable_index = this._routableIndexFromTrackIndex ( interval_track_indexes[1] )-1;

		var pointm_lt_function = function (a,b) { return a.q < b.q };
		var new_path , new_part;
		var part_start_index , part_end_index;

		// note the very deliberate use of exclusive comparisons here - an _equal_ routable index will
		// be represented by a full next (or previous) part. or not be present at all, in which case it's
		// off screen and doesn't matter
		for ( var i = 0 ; i < this._parts.length && this._parts[i][0].q < end_routable_index ; i++ )
		{
			if ( this._parts[i][this._parts[i].length-1].q > start_routable_index )
			{
				// this part somehow overlaps with our routable index interval so we need to consider it

				new_part = this._parts[i];

				if ( new_part[0].q < start_routable_index )
				{
					// we have to trim the start of this part
					part_start_index = Math.max ( new_part.bisect ( { "q" : start_routable_index } , null , null , pointm_lt_function )-1 , 0 );
					if ( new_part[part_start_index].q !== start_routable_index )
						new_part = [ this._originalPoints[this._originalPointsIndexes[start_routable_index]] ].concat ( new_part.slice ( part_start_index+1 ) );
					else
						new_part = new_part.slice ( part_start_index );
				}

				if ( new_part[new_part.length-1].q > end_routable_index )
				{
					// we have to trim the end of this part
					part_end_index = new_part.bisect ( { "q" : end_routable_index } , null , null , function (a,b) { return a.q < b.q } )-1;
					new_part = new_part.slice ( 0 , part_end_index+1 );
					
					if ( new_part[new_part.length-1].q !== end_routable_index )
						new_part = new_part.concat ( this._originalPoints[this._originalPointsIndexes[end_routable_index]] );
				}

				new_path = this._createElement ( "path" );
				new_path.setAttribute ( "d" , "" + this._getPathPartStr ( new_part ) );
				
				under_paths.push ( new_path );
				under_paths_container.appendChild ( new_path );
			}
		}
	},

	_updateSelectionUnderPaths: function ()
	{
		return this._commonUpdateSelectionUnderPaths ( this._selectedTrackIndexes , this._selectedUnderPaths , this._selectedUnderPathsContainer );
	},

	_updatePreSelectionUnderPaths: function ()
	{
		return this._commonUpdateSelectionUnderPaths ( this._preSelectedTrackIndexes , this._preSelectedUnderPaths , this._preSelectedUnderPathsContainer , !this._preSelectedValid );
	},

	//
	// Overrides from Path.SVG
	//

	_initPath: function () {
		this._container = this._createElement('g');
		this._defs = this._createElement ( "defs" );
		this._container.appendChild ( this._defs );

		this._plainUnderContainer = this._createElement('g');
		this._container.appendChild ( this._plainUnderContainer );

		this._shadowedContainer = this._createElement('g');
		this._container.appendChild ( this._shadowedContainer );

		this._plainContainer = this._createElement('g');
		this._container.appendChild ( this._plainContainer );

// 		this._filter = this._createElement( "filter" );
// 		this._filter.setAttribute ( "id" , "polylinem-" + L.Util.stamp ( this ) + "-lighten-filter" );
// 		this._feblend = this._createElement( "feBlend" );
// 		this._feblend.setAttribute ( "mode" , "lighten" );
// 		this._feblend.setAttribute ( "in2" , "BackgroundImage" );
// 		this._feblend.setAttribute ( "in" , "SourceGraphic" );
// 		this._filter.appendChild ( this._feblend );
// 		this._defs.appendChild ( this._filter );

		this._commonSelectedUnderPathsContainer = this._createElement ( "g" );
		this._shadowedContainer.appendChild ( this._commonSelectedUnderPathsContainer );

		this._selectedUnderPaths = [];
		this._selectedUnderPathsContainer = this._createElement ( "g" );
		this._commonSelectedUnderPathsContainer.appendChild ( this._selectedUnderPathsContainer );

		this._preSelectedUnderPaths = [];
		this._preSelectedUnderPathsContainer = this._createElement ( "g" );
		this._commonSelectedUnderPathsContainer.appendChild ( this._preSelectedUnderPathsContainer );

		this._GPXPointsContainer = this._createElement ( "g" );
		this._shadowedContainer.appendChild ( this._GPXPointsContainer );

		this._GPXCircles = [];
		this._GPXCirclesIndexes = [];


		this._nullPointsContainer = this._createElement ( "g" );
		this._shadowedContainer.appendChild ( this._nullPointsContainer );

		this._nullCrosses = [];
		this._nullCrossesIndexes = [];


		this._selectedNullPointHighlightsContainer = this._createElement ( "g" );
		this._shadowedContainer.appendChild ( this._selectedNullPointHighlightsContainer );

		this._selectedNullCrossHighlights = [];
		this._selectedNullCrossHighlightsIndexes = [];


		this._selectedNullPointsContainer = this._createElement ( "g" );
		this._shadowedContainer.appendChild ( this._selectedNullPointsContainer );

		this._selectedNullCrosses = [];
		this._selectedNullCrossesIndexes = [];


		this._preSelectedNullPointHighlightsContainer = this._createElement ( "g" );
		this._shadowedContainer.appendChild ( this._preSelectedNullPointHighlightsContainer );

		this._preSelectedNullCrossHighlights = [];
		this._preSelectedNullCrossHighlightsIndexes = [];


		this._preSelectedNullPointsContainer = this._createElement ( "g" );
		this._shadowedContainer.appendChild ( this._preSelectedNullPointsContainer );

		this._preSelectedNullCrosses = [];
		this._preSelectedNullCrossesIndexes = [];


		this._candidatePointsContainer = this._createElement ( "g" );
		this._plainUnderContainer.appendChild ( this._candidatePointsContainer );

		this._candidatePoints = [];
		// need to keep these to test against mouse event targets
		this._candidateCircles = [];


		// yeah - we don't actually init the path element here anymore
		this._underPaths = [];
		this._underPathsContainer = this._createElement('g');
		this._shadowedContainer.appendChild ( this._underPathsContainer );

		this._paths = [];
		this._pathsContainer = this._createElement('g');
		//this._pathsContainer.setAttribute ( "enable-background" , "new" );
		this._shadowedContainer.appendChild ( this._pathsContainer );

		this._lingrads = [];
		this._stop0s = [];
		this._stop1s = [];


		this._selectedContainer = this._createElement ( "g" );
		this._shadowedContainer.appendChild ( this._selectedContainer );

		this._selectedCircles = [];
		this._selectedCirclesIndexes = [];


		this._preSelectedContainer = this._createElement ( "g" );
		this._shadowedContainer.appendChild ( this._preSelectedContainer );

		this._preSelectedCircles = [];
		this._preSelectedCirclesIndexes = [];


		this._hoveredNullPointHighlightsContainer = this._createElement ( "g" );
		this._plainContainer.appendChild ( this._hoveredNullPointHighlightsContainer );

		this._hoveredNullCrossHighlights = [];
		this._hoveredNullCrossHighlightsIndexes = [];


		this._hoveredNullPointsContainer = this._createElement ( "g" );
		this._plainContainer.appendChild ( this._hoveredNullPointsContainer );

		this._hoveredNullCrosses = [];
		this._hoveredNullCrossesIndexes = [];


		this._hoveredGPXCirclesContainer = this._createElement ( "g" );
		this._plainContainer.appendChild ( this._hoveredGPXCirclesContainer );

		this._hoveredGPXCircles = [];
		this._hoveredGPXCirclesIndexes = [];


		this._hoveredContainer = this._createElement ( "g" );
		this._plainContainer.appendChild ( this._hoveredContainer );

		this._hoveredCircles = [];
		this._hoveredCirclesIndexes = [];
	},

	_initStyleInner: function ( element )
	{
		if (this.options.stroke) {
			element.setAttribute('stroke-linejoin', 'round');
			element.setAttribute('stroke-linecap', 'round');
		}
		if (this.options.fill) {
			element.setAttribute('fill-rule', 'evenodd');
		}
		if (this.options.clickable) {
			if (L.Browser.svg || !L.Browser.vml) {
				element.setAttribute('class', 'leaflet-clickable');
			}
		}
	},

	_initStyle: function () {
		for ( var i = 0 ; i < this._paths.length ; i++ )
			this._initStyleInner ( this._paths[i] );

		for ( var i = 0 ; i < this._underPaths.length ; i++ )
			this._initStyleInner ( this._underPaths[i] );

		this._updateStyle();
	},

	_updateStyleInner: function ( element )
	{
		if (this.options.stroke) {
			element.setAttribute('stroke-opacity', this.options.opacity);
			element.setAttribute('stroke-width', this.options.weight);
			// this conflicts with our use of stroke-dasharray
// 			if (this.options.dashArray) {
// 				element.setAttribute('stroke-dasharray', this.options.dashArray);
// 			} else {
// 				element.removeAttribute('stroke-dasharray');
// 			}
		} else {
			element.setAttribute('stroke', 'none');
		}
		if (this.options.fill) {
			element.setAttribute('fill', this.options.fillColor || this.options.color);
			element.setAttribute('fill-opacity', this.options.fillOpacity);
		} else {
			element.setAttribute('fill', 'none');
		}
	},

	_updateStyle: function () {
		for ( var i = 0 ; i < this._paths.length ; i++ )
			this._updateStyleInner ( this._paths[i] );
		for ( var i = 0 ; i < this._underPaths.length ; i++ )
			this._updateStyleInner ( this._underPaths[i] );

		this._commonSelectedUnderPathsContainer.setAttribute ( "fill" , "none" );
		this._commonSelectedUnderPathsContainer.setAttribute ( "stroke" , "white" );
		this._commonSelectedUnderPathsContainer.setAttribute ( "stroke-linecap" , "round" );
		this._commonSelectedUnderPathsContainer.setAttribute ( "stroke-linejoin" , "round" );

		this._selectedUnderPathsContainer.setAttribute ( "stroke-width" , this.options.weight + 3 );
		this._preSelectedUnderPathsContainer.setAttribute ( "stroke-width" , this.options.weight + 1 );

		if ( this.options.shadowfilter )
			this._shadowedContainer.setAttribute ( "filter" , this.options.shadowfilter );

		if ( this.options.selectedfilter )
			this._commonSelectedUnderPathsContainer.setAttribute ( "filter" , this.options.selectedfilter );

		var gpxcirclecolor = this.options.gpxcirclecolor || "#666";

		this._GPXPointsContainer.setAttribute ( "fill" , gpxcirclecolor );
		this._GPXPointsContainer.setAttribute ( "opacity" , "0.5" );
		if ( this.options.clickable )
			this._GPXPointsContainer.setAttribute ( "class" , "leaflet-clickable" );

		this._hoveredGPXCirclesContainer.setAttribute ( "fill" , gpxcirclecolor );
		if ( this.options.clickable )
			this._hoveredGPXCirclesContainer.setAttribute ( "class" , "leaflet-clickable" );

		this._nullPointsContainer.setAttribute ( "stroke-linecap" , "round" );
		this._nullPointsContainer.setAttribute ( "stroke-width" , "2" );
		this._nullPointsContainer.setAttribute ( "fill" , "none" );
		this._nullPointsContainer.setAttribute ( "stroke" , this._cssColorString ( this._getColor1 () ) );
		if ( this.options.clickable )
			this._nullPointsContainer.setAttribute ( "class" , "leaflet-clickable" );

		this._selectedNullPointHighlightsContainer.setAttribute ( "stroke-linecap" , "round" );
		this._selectedNullPointHighlightsContainer.setAttribute ( "stroke-width" , "5" );
		this._selectedNullPointHighlightsContainer.setAttribute ( "fill" , "none" );
		this._selectedNullPointHighlightsContainer.setAttribute ( "stroke" , "white" );
		if ( this.options.clickable )
			this._selectedNullPointHighlightsContainer.setAttribute ( "class" , "leaflet-clickable" );

		this._selectedNullPointsContainer.setAttribute ( "stroke-linecap" , "round" );
		this._selectedNullPointsContainer.setAttribute ( "stroke-width" , "2" );
		this._selectedNullPointsContainer.setAttribute ( "fill" , "none" );
		this._selectedNullPointsContainer.setAttribute ( "stroke" , this._cssColorString ( this._getColor1 () ) );
		if ( this.options.clickable )
			this._selectedNullPointsContainer.setAttribute ( "class" , "leaflet-clickable" );

		this._hoveredNullPointHighlightsContainer.setAttribute ( "stroke-linecap" , "round" );
		this._hoveredNullPointHighlightsContainer.setAttribute ( "stroke-width" , "5" );
		this._hoveredNullPointHighlightsContainer.setAttribute ( "fill" , "none" );
		this._hoveredNullPointHighlightsContainer.setAttribute ( "stroke" , "white" );
		this._hoveredNullPointHighlightsContainer.setAttribute ( "opacity" , "0.5" );
		if ( this.options.clickable )
			this._hoveredNullPointHighlightsContainer.setAttribute ( "class" , "leaflet-clickable" );

		this._hoveredNullPointsContainer.setAttribute ( "stroke-linecap" , "round" );
		this._hoveredNullPointsContainer.setAttribute ( "stroke-width" , "2" );
		this._hoveredNullPointsContainer.setAttribute ( "fill" , "none" );
		this._hoveredNullPointsContainer.setAttribute ( "stroke" , this._cssColorString ( this._getColor1 () ) );
		if ( this.options.clickable )
			this._hoveredNullPointsContainer.setAttribute ( "class" , "leaflet-clickable" );

		this._preSelectedNullPointHighlightsContainer.setAttribute ( "stroke-linecap" , "round" );
		this._preSelectedNullPointHighlightsContainer.setAttribute ( "stroke-width" , "5" );
		this._preSelectedNullPointHighlightsContainer.setAttribute ( "fill" , "none" );
		this._preSelectedNullPointHighlightsContainer.setAttribute ( "stroke" , "white" );
		this._preSelectedNullPointHighlightsContainer.setAttribute ( "opacity" , "0.5" );
		if ( this.options.clickable )
			this._preSelectedNullPointHighlightsContainer.setAttribute ( "class" , "leaflet-clickable" );

		this._preSelectedNullPointsContainer.setAttribute ( "stroke-linecap" , "round" );
		this._preSelectedNullPointsContainer.setAttribute ( "stroke-width" , "2" );
		this._preSelectedNullPointsContainer.setAttribute ( "fill" , "none" );
		this._preSelectedNullPointsContainer.setAttribute ( "stroke" , this._cssColorString ( this._getColor1 () ) );
		if ( this.options.clickable )
			this._preSelectedNullPointsContainer.setAttribute ( "class" , "leaflet-clickable" );
		
		this._candidatePointsContainer.setAttribute ( "stroke-width" , "1" );
		this._candidatePointsContainer.setAttribute ( "stroke" , "black" );
		this._candidatePointsContainer.setAttribute ( "fill" , "none" );
		this._candidatePointsContainer.setAttribute ( "class" , this.options.clickable ? "leaflet-clickable candidatepoints" : "candidatepoints" );
		

		this._preSelectedContainer.setAttribute ( "stroke-width" , this.options.weight );
		if ( this.options.clickable )
			this._preSelectedContainer.setAttribute ( "class" , "leaflet-clickable" );

		this._selectedContainer.setAttribute ( "stroke-width" , this.options.weight );
		if ( this.options.clickable )
			this._selectedContainer.setAttribute ( "class" , "leaflet-clickable" );

		this._hoveredContainer.setAttribute ( "stroke" , "none" );
		if ( this.options.clickable )
			this._hoveredContainer.setAttribute ( "class" , "leaflet-clickable" );
	},

	_updatePath: function () {
		//
		// from Polyline
		//
		if (!this._map) { return; }

		this._clipPoints();
		this._simplifyPoints();

		//
		// from Path.SVG
		//
		var strs = [];
		for (var i = 0, len = this._parts.length; i < len; i++) {
			strs.push ( "" + this._getPathPartStr(this._parts[i]) );
		}
		
		var stop ,
			lingrad;

		var color0 = this._getColor0 () , color1 = this._getColor1 ();

		for ( var i = 0 ; i < strs.length ; i++ )
		{
			var id_prefix = "polylinem-" + L.Util.stamp ( this ) + "-";
			if ( ! this._paths[i] )
			{
				// we need to add a new path element
				this._paths.push ( this._createElement ( "path" ) );
				this._pathsContainer.appendChild ( this._paths[i] );
				this._paths[i].setAttribute ( "stroke" , "url(#" + id_prefix + "lg-" + i + ")" );
//				this._paths[i].setAttribute ( "filter" , "url(#" + id_prefix + "lighten-filter)" );
				this._paths[i].setAttribute ( "id" , id_prefix + "path-" + i );
			}
// 			if ( ! this._underPaths[i] )
// 			{
// 				// we need to add a new (under) path element
// 				this._underPaths.push ( this._createElement ( "path" ) );
// 				this._underPathsContainer.appendChild ( this._underPaths[i] );
// 				this._underPaths[i].setAttribute ( "stroke" , this._cssColorString ( color0 ) );
// 				this._underPaths[i].setAttribute ( "id" , id_prefix + "underpath-" + i );
// 			}
			if ( ! this._lingrads[i] )
			{
				// we need to add a new linear gradient and its associated stops
				lingrad = this._createElement ( "linearGradient" );
				lingrad.setAttribute ( "id" , id_prefix + "lg-" + i );
				lingrad.setAttribute ( "gradientUnits" , "userSpaceOnUse" );
				lingrad.setAttribute ( "spreadMethod" , "pad" );

				stop = this._createElement ( "stop" );
				stop.setAttribute ( "offset" , 0 );
				this._stop0s.push ( stop );
				lingrad.appendChild ( stop );

				stop = this._createElement ( "stop" );
				stop.setAttribute ( "offset" , 1 );
				this._stop1s.push ( stop );
				lingrad.appendChild ( stop );

				this._lingrads.push ( lingrad );
				this._defs.appendChild ( lingrad );
			}

			var str = strs[i];
			if (!str) {
				// fix webkit empty string parsing bug
				str = 'M0 0';
			}
			this._paths[i].setAttribute('d', str);
// 			this._underPaths[i].setAttribute('d', str);

			// if second (first "destination") pointm of part is unconnected, assume this whole part should be drawn dotted
			if ( this._parts[i][1] && this._parts[i][1].s === false )
				this._paths[i].setAttribute ( "stroke-dasharray" , "3px,6px" );
			else
				this._paths[i].removeAttribute ( "stroke-dasharray" );

			this._lingrads[i].setAttribute ( "x1" , this._parts[i][0].x );
			this._lingrads[i].setAttribute ( "y1" , this._parts[i][0].y );
			this._lingrads[i].setAttribute ( "x2" , this._parts[i][this._parts[i].length-1].x );
			this._lingrads[i].setAttribute ( "y2" , this._parts[i][this._parts[i].length-1].y );

			this._stop0s[i].setAttribute ( "stop-color" , this._getInterpolatedColor ( this._parts[i][0].m ) );
			this._stop1s[i].setAttribute ( "stop-color" , this._getInterpolatedColor ( this._parts[i][this._parts[i].length-1].m ) );
		}

		while ( this._paths.length > strs.length )
		{
			// get rid of surplus elements
			this._pathsContainer.removeChild ( this._paths.pop () );

			// keep lingrads and stops around for future use - save us regenerating them
		}

		while ( this._underPaths.length > strs.length )
		{
			// get rid of surplus elements
			this._underPathsContainer.removeChild ( this._underPaths.pop () );
		}

		this._updateSelectionCircles ();
		this._updateHoverCircles ();
		this._updateNullCrosses ();
		this._updateCandidatePoints ();

		this._updateSelectionUnderPaths ();
		this._updatePreSelectionUnderPaths ();

		this._initStyle ();
	},

	_initEvents: function () {
		//
		// really just taking all the event handling internal to the class
		//

		if (this.options.clickable) {
			L.DomEvent.on ( this._container , "click" , this._onMouseClick , this );
			L.DomEvent.on ( this._container , "mousemove" , this._hoverOnMouseMove , this );
			L.DomEvent.on ( this._container , "mouseleave" , this._onMouseLeave , this );
			L.DomEvent.on ( this._container , L.Draggable.START , this._onMouseDown , this );
		}
	},

	_extractArrayMinMax: function ( a )
	{
		var min , max;
		for ( var i = 0 ; i < a.length ; i++ )
		{
			min = min == null ? a[i] : Math.min ( min , a[i] );
			max = max == null ? a[i] : Math.max ( max , a[i] );
		}
		if ( min == max )
			return [ min ];
		else
			return [ min , max ];
	},

	_onMouseDown: function ( e )
	{
		this.fire ( "hideContextMenu" );

		if ((!L.Browser.touch && e.shiftKey) || ((e.which !== 1) && (e.button !== 1) && !e.touches)) {
			return;
		}

		this._simulateClick = true;

		if (e.touches && e.touches.length > 1) {
			this._simulateClick = false;
			return;
		}

		var first = (e.touches && e.touches.length === 1 ? e.touches[0] : e),
			el = first.target;

		L.DomEvent.stop(e);

		this._dragMoved = false;
		if (this._dragMoving) {
			return;
		}

		// we don't want the panning hand shown while we're doing this
		// (technique nabbed from BoxZoom)
		this._map._container.style.cursor = 'default';

		this._dragStartPoint = new L.Point(first.clientX, first.clientY);
		this._dragStartPointTrackIndexes = this._getTrackIndexesForEvent ( e , this._map.containerPointToLayerPoint ( this._map.mouseEventToContainerPoint(e) ) );

		// temporarily install these event handlers while the drag's still in action
		L.DomEvent.on(document, L.Draggable.MOVE[e.type], this._dragOnMouseMove, this);
		L.DomEvent.on(document, L.Draggable.END[e.type], this._onMouseUp, this);

		// there's no need for this while we're dragging and it interferes with things
		L.DomEvent.off ( this._container , "mousemove", this._hoverOnMouseMove);
	} ,

	_dragOnMouseMove: function (e)
	{
		if (e.touches && e.touches.length > 1) { return; }

		var first = (e.touches && e.touches.length === 1 ? e.touches[0] : e);
		this._dragNewPoint = new L.Point(first.clientX, first.clientY);

		this._dragMoving = true;

		if (!this._dragMoved) {
			this._dragMoved = true;

			if (!L.Browser.touch) {
				L.DomUtil.disableTextSelection();
			}

			//
			// this is where we'd react to equivalent of a "dragstart"
			//
		}
		else
		{
			//
			// this is where we'd react to equivalent of a "drag"
			//
		}

		// but we want to react pretty much the same for either
		if ( this._dragStartPoint && this._dragStartPointTrackIndexes && this._dragStartPointTrackIndexes.length )
		{
			var new_track_indexes = this._getTrackIndexesForEvent ( e , this._map.containerPointToLayerPoint ( this._map.mouseEventToContainerPoint(e) ) );
			if ( new_track_indexes.length )
				this._setPreSelection ( this._extractArrayMinMax ( new_track_indexes.concat ( this._dragStartPointTrackIndexes ) ) , true );
			else
				this._setPreSelection ( this._dragStartPointTrackIndexes , false );
		}

		this._updateSelectionCircles ();
		this._updateHoverCircles ();
	},

	_onMouseUp: function (e) {
		if (this._simulateClick && e.changedTouches) {
			var first = e.changedTouches[0],
				el = first.target,
				dist = (this._dragNewPoint && this._dragNewPoint.distanceTo(this._dragStartPoint)) || 0;

			if (dist < L.Draggable.TAP_TOLERANCE) {
				L.Draggable._simulateEvent('click', first);
			}
		}

		if (!L.Browser.touch) {
			L.DomUtil.enableTextSelection();
		}


		// reset map cursor
		this._map._container.style.cursor = '';

		// we're done with these event handlers for now
		for (var i in L.Draggable.MOVE) {
			if (L.Draggable.MOVE.hasOwnProperty(i)) {
				L.DomEvent.off(document, L.Draggable.MOVE[i], this._dragOnMouseMove);
				L.DomEvent.off(document, L.Draggable.END[i], this._onMouseUp);
			}
		}

		// enable this again
		L.DomEvent.on ( this._container , "mousemove" , this._hoverOnMouseMove , this );

		if (this._dragMoved) {
			// we don't want a spurious click being emitted and deselecting everything, so block clicks
			// that share the same timestamp
			this._clickBlockedTimeStamp = e.timeStamp;

			//
			// react to equivalent of "dragend"
			//

			if ( this._preSelectedTrackIndexes && this._preSelectedTrackIndexes.length && this._preSelectedValid )
				// pre-selection should become actual selection
				this.setSelection ( this._preSelectedTrackIndexes );

			this._dragStartPoint = null;
			this._dragStartPointTrackIndexes = null;
	
			this._setPreSelection ( [] , false );
		}
		this._dragMoving = false;
	},

	_onMouseClick: function (e) {
		// hmm - disabling this for now
		//if (this._map.dragging && this._map.dragging.moved()) {
		//	return;
		//}

		L.DomEvent.stop (e);

		if ( this._clickBlockedTimeStamp && this._clickBlockedTimeStamp >= e.timeStamp )
			// too soon after a dragend
			return;
		
		if ( this._selectedTrackIndexes.length == 1 )
		{
			var target_element_index = this._candidateCircles.indexOf ( e.target );
			if ( target_element_index >= 0 )
			{
				this.setPinning ( target_element_index === this._pinning ? null : target_element_index );
				return;
			}
		}

		var containerPoint = this._map.mouseEventToContainerPoint(e),
			layerPoint = this._map.containerPointToLayerPoint(containerPoint),
			track_indexes = this._getTrackIndexesForEvent ( e , layerPoint );

		this.setSelection ( track_indexes );
	},

	_hoverOnMouseMove: function (e) {
		var containerPoint = this._map.mouseEventToContainerPoint(e),
			layerPoint = this._map.containerPointToLayerPoint(containerPoint),
			track_indexes = this._getTrackIndexesForEvent ( e , layerPoint );

		this.setHover ( track_indexes );
	},

	_onMouseLeave: function (e) {
		this.setHover ( [] );
	},

	_getTrackIndexesForEvent: function ( e , layerPoint )
	{
		//
		// figure out what point(s) event is "over"
		//

		var target_element_index , originalpoints_index , routable_indexes;
		if ( e.target )
		{
			// really going to have to port this to jquery at some point to do things sanely, but for now
			// this is actually quite fast
			
			target_element_index = this._paths.indexOf ( e.target );
			if ( target_element_index >= 0 )
			{
				// we're over a path element

				// remember a PointM's "r" could be fractional as a result of segment clipping. we therefore have to
				// appropriately floor/ceil it at each end
				originalpoints_index = Math.floor ( this._parts[target_element_index][0].r ) + this._nearestSegmentIndex ( this._originalPoints.slice ( Math.floor ( this._parts[target_element_index][0].r ) , Math.ceil ( this._parts[target_element_index][this._parts[target_element_index].length-1].r ) + 1 ) , layerPoint );

				routable_indexes = [ Math.floor ( this._originalPoints[originalpoints_index].q ) , Math.ceil ( this._originalPoints[originalpoints_index].q ) ];
				if ( routable_indexes[0] === routable_indexes[1] )
					// "q" was exact, but indicated PointM will have been from "lower" end of segment, so the "upper" bound will be the next one
					routable_indexes[1]++;

				return [ this._trackIndexFromRoutableIndex ( routable_indexes[0] ) , this._trackIndexFromRoutableIndex ( routable_indexes[1] ) ];
			}

			target_element_index = this._selectedCircles.indexOf ( e.target );
			if ( target_element_index >= 0 )
				// we're over a selected circle element
				return [ this._selectedCirclesIndexes[target_element_index] ];

			target_element_index = this._preSelectedCircles.indexOf ( e.target );
			if ( target_element_index >= 0 )
				// we're over a preselected circle element
				return [ this._preSelectedCirclesIndexes[target_element_index] ];

			target_element_index = this._hoveredCircles.indexOf ( e.target );
			if ( target_element_index >= 0 )
				// we're over a hovered circle element
				return [ this._hoveredCirclesIndexes[target_element_index] ];

			target_element_index = this._GPXCircles.indexOf ( e.target );
			if ( target_element_index >= 0 )
				// we're over a gpx circle element
				return [ this._GPXCirclesIndexes[target_element_index] ];

			target_element_index = this._hoveredGPXCircles.indexOf ( e.target );
			if ( target_element_index >= 0 )
				// we're over a hovered gpx circle element
				return [ this._hoveredGPXCirclesIndexes[target_element_index] ];

			target_element_index = this._nullCrosses.indexOf ( e.target );
			if ( target_element_index >= 0 )
				// we're over a null cross element
				return [ this._nullCrossesIndexes[target_element_index] ];

			target_element_index = this._selectedNullCrosses.indexOf ( e.target );
			if ( target_element_index >= 0 )
				// we're over a selected null cross element
				return [ this._selectedNullCrossesIndexes[target_element_index] ];

			target_element_index = this._selectedNullCrossHighlights.indexOf ( e.target );
			if ( target_element_index >= 0 )
				// we're over a selected null cross element
				return [ this._selectedNullCrossHighlightsIndexes[target_element_index] ];

			target_element_index = this._hoveredNullCrosses.indexOf ( e.target );
			if ( target_element_index >= 0 )
				// we're over a hovered null cross element
				return [ this._hoveredNullCrossesIndexes[target_element_index] ];

			target_element_index = this._hoveredNullCrossHighlights.indexOf ( e.target );
			if ( target_element_index >= 0 )
				// we're over a hovered null cross element
				return [ this._hoveredNullCrossHighlightsIndexes[target_element_index] ];

			target_element_index = this._preSelectedNullCrosses.indexOf ( e.target );
			if ( target_element_index >= 0 )
				// we're over a preselected null cross element
				return [ this._preSelectedNullCrossesIndexes[target_element_index] ];

			target_element_index = this._preSelectedNullCrossHighlights.indexOf ( e.target );
			if ( target_element_index >= 0 )
				// we're over a preselected null cross element
				return [ this._hoveredNullCrossHighlightsIndexes[target_element_index] ];
		}

		return [];
	},
} );

L.polylineM = function (latlngs, options) {
	return new L.PolylineM(latlngs, options);
};


} ) ();
