(function ()
{

window.tsbp.SolutionMasterPanel = function ( initial_element )
{
	var smp = initial_element.addClass ( "_solutionmasterpanel _panel" );
	var map_llbounds;

	smp.find ( "._solutionmasterpanel_tracklisting" ).eq(0).children ().each ( function ( i , element )
	{
		window.tsbp.TrackListingEntry ( $(element) );
	} );

	smp.on ( "mapPositionChanged" , function ( event , center , zoom , llbounds )
	{
		map_llbounds = llbounds;

		smp.find ( "a._solutionmasterpanel_editlink" ).attr ( "href" , "http://www.openstreetmap.org/edit?lat=" + center.lat + "&lon=" + center.lng + "&zoom=" + zoom );
	} );

	smp.on ( "click" , "button[name='editremotecontrol']" , function ( event )
	{
		$.ajax ( {
			type : "GET" ,
			traditional : true ,
			url : "http://127.0.0.1:8111/load_and_zoom" ,
			data : {
				left : map_llbounds.getSouthWest ().lng ,
				bottom : map_llbounds.getSouthWest ().lat ,
				right : map_llbounds.getNorthEast ().lng ,
				top : map_llbounds.getNorthEast ().lat
			} ,
			dataType : "jsonp"
		} );

		return false;
	} );

	smp.removeClass ( "initially_undisplayed" );
	return smp;
};

var _determine_auto_weight = function ( intents , track_index )
{
	if ( $.isNumeric ( track_index ) )
	{
		// now see if we can find any non-base intent in tracks intentarray if it has one
		var intentarray = intents.tracks && intents.tracks[track_index] && intents.tracks[track_index].intentarray;

		if ( intentarray && intentarray.length )
		{
			var any_non_base = false;
			$.each ( intentarray , function ( i , item )
			{
				if ( item !== window.tsbp.intent.excluded && item !== window.tsbp.intent.base )
				{
					any_non_base = true;
					return false;
				}
			} );

			if ( any_non_base )
				return "1";
		}
	}

	return "0";
};

window.tsbp.RecalculateForm = function ( initial_form )
{
	var rf = initial_form.addClass ( "_recalculateform _modaldialog" );

	$(window).on ( "hashchange" , function ( event )
	{
		// RecalculateForm rather intentionally lives outside the hashchange manager as it's
		// designed more to be a mimic of the simple native html behaviour we use with css to get
		// RecalculateForm to display itself at appropriate times
		if ( window.location.hash !== "#" + rf[0].id )
			return;

		//
		// setup form contents according to intents
		//

		var intents = rf.closest ( "._solutionmap" ).data ( "intents" );
		rf[0].intents.value = JSON.stringify ( intents );

		var any_not_auto_0 = false;
		rf.find ( "select[name^='tracks__'][name$='__weight']" ).each ( function ( i , element )
		{
			// let's try and extract a track index out of the name
			var track_index = parseInt ( this.name.slice ( "tracks__".length , -"__weight".length ) );
			var last_auto_id = this.name + "_last_auto";
			var last_auto_input = $("#"+last_auto_id);
			if ( ! last_auto_input.length )
				last_auto_input = $("<input type='hidden'/>").attr ( "id" , last_auto_id ).insertAfter ( this );

			var new_auto_weight = _determine_auto_weight ( intents , track_index );

			if ( last_auto_input[0].value === this.value || ! last_auto_input[0].value )
			{
				// select remained its auto value from last time, so lets change it to its new auto value
				this.value = new_auto_weight;

				if ( new_auto_weight !== "0" )
					any_not_auto_0 = true;
			}
			else
				any_not_auto_0 = true;

			last_auto_input[0].value = new_auto_weight;
		} );

		if ( ! any_not_auto_0 )
		{
			// seems they were all auto-set to 0. that's not much use. let's auto-set them all to 1?
			rf.find ( "select[name^='tracks__'][name$='__weight']" ).each ( function () { $("#"+this.name+"_last_auto")[0].value = this.value = "1" } );
		}
	} );
	
	rf.removeClass ( "initially_undisplayed" );
	return rf;
};

window.tsbp.TrackListingEntry = function ( initial_element )
{
	var tle = initial_element.addClass ( "_tracklistingentry" );
	var expanded = tle.hasClass ( "_expanded" );
	var detailheading = tle.find ( "._tracklistingentry_detailheading" );
	var content = tle.find ( "._tracklistingentry_detailcontent" );

	var state = "initial";
	var changestate = function ( newstate )
	{
		tle.removeClass ( tsbp.state_classes[state] );
		tle.addClass ( tsbp.state_classes[newstate] );

		var oldstate = state;
		state = newstate;
	};
	changestate ( "idle" );
	var latest_detail_promise;
	var latest_pointinfo_promise;

	var _track_index = tle.data ( "trackIndex" );
	if ( _track_index != null )
		tle.addClass ( "_" + TSBP_TRACK_COLORS.names[ _track_index % TSBP_TRACK_COLORS.names.length ] );

	var cost_overview = tle.find ( "svg._tracklistingentry_costoverview" ).eq(0);
	if ( cost_overview.length )
		cost_overview = window.tsbp.CostOverview ( cost_overview );

	tle.on ( "intentsOverviewUpdate" , function ( event , overview_array )
	{
		var anytry_array = [];
		var tryhard_array = [];
		var anyuntry_array = [];
		var untryhard_array = [];
		var anyignore_array = [];

		for ( var i = 0 ; i < overview_array.length ; i++ )
		{
			anytry_array[i] = !!( ( (1<<window.tsbp.intent.try_) | (1<<window.tsbp.intent.tryhard) ) & overview_array[i] );
			tryhard_array[i] = !!( (1<<window.tsbp.intent.tryhard) & overview_array[i] );
			anyuntry_array[i] = !!( ( (1<<window.tsbp.intent.untry) | (1<<window.tsbp.intent.untryhard) ) & overview_array[i] );
			untryhard_array[i] = !!( (1<<window.tsbp.intent.untryhard) & overview_array[i] );
			anyignore_array[i] = !!( (~0xff) & overview_array[i] );
		}

		cost_overview.trigger ( "newData" , [ anytry_array , "anytry" ] );
		cost_overview.trigger ( "newData" , [ tryhard_array , "tryhard" ] );
		cost_overview.trigger ( "newData" , [ anyuntry_array , "anyuntry" ] );
		cost_overview.trigger ( "newData" , [ untryhard_array , "untryhard" ] );
		cost_overview.trigger ( "newData" , [ anyignore_array , "anyignore" ] );
	} );

	tle.on ( "toggleExpanded" , function ()
	{
		expanded = !expanded;
		tle.toggleClass ( "_expanded" , expanded );

		return false;
	} );

	tle.on ( "setExpanded" , function ( event , sense )
	{
		expanded = !!sense;
		tle.toggleClass ( "_expanded" , expanded );

		return false;
	} );

	var emptycontent = function ()
	{
		content.find ( "[title]" ).tipsy ( "hide" );

		// give currently present elements a chance to clear up
		content.children().trigger ( "aboutToDelete" );

		content.empty ();
	};

	var augmentedhashchangefunction = function ( event , augmented_token_array , extra_options )
	{
		// note the importance of casting selected to a bool to prevent a null e.g. making a toggleClass function call
		// look like a single-argument one
		var selected = !!(augmented_token_array[0] && augmented_token_array[0].type === "track" && augmented_token_array[0].track_index === tle.data ( "trackIndex" ));
		var selectedindexes;
		tle.toggleClass ( "_selected" , selected );

		// TODO implement equivalent of this in CostPlot hash handler
// 		if ( state === "requesting" && latest_xhr )
// 			latest_xhr.abort ();

		latest_pointinfo_promise = null;

		detailheading.find ( "[title]" ).tipsy ( "hide" );
		detailheading.empty ();

		if ( selected )
		{
			selectedindexes = augmented_token_array[1] && augmented_token_array[1].type === "trackpoint" ? augmented_token_array[1].selectedindexes : [];
			if ( ! selectedindexes.length )
			{
				emptycontent ();

				detailheading.text ( "No selection" );
			}
			else
			{
				tle.trigger ( "setExpanded" , true );
				detailheading.text ( "Trackpoint" + ( selectedindexes.length > 1 ? "s" : "" ) + " " + selectedindexes[0] + ( selectedindexes.length > 1 ? " to " + selectedindexes[1] : "" ) );
				
				if ( selectedindexes.length === 1 )
				{
					latest_pointinfo_promise = augmented_token_array[1].pointinfo_promise;
					augmented_token_array[1].pointinfo_promise.done ( function ( pointinfo )
					{
						// because promises aren't cancelable, we have to do an "are we still wanted?" check in this really stupid way
						if ( latest_pointinfo_promise !== augmented_token_array[1].pointinfo_promise )
							return;

						if ( pointinfo[0].cost != null )
						{
							detailheading.append ( $("<svg width='16' height='16' class='_tracklistingentry_detailpoint'><circle cx='8' cy='6' r='4' stroke-width='2.8' fill='white' class='_circle_nocost'/><circle cx='8' cy='6' r='4' stroke-width='3' fill='none' opacity='" + pointinfo[0].relative_visual_cost + "' class='_circle_cost'/></svg>") );
						}

						var new_svg = $("<svg width='18' height='12' class='_tracklistingentry_detailintentpatch'><rect x='-1px' y='-1px' width='20px' height='14px' class='_tracklistingentry_detailintentrect _intent_listener' data-track-index='" + tle.data ( "trackIndex" ) + "' data-trackpoint-index='" + selectedindexes[0] + "' data-intent='" + pointinfo[0].intent + "' original-title='placeholder'/></svg>");

						tsbp.update_intent_listener ( new_svg.children ().eq(0) );

						detailheading.prepend ( new_svg );

						detailheading.find ( "[original-title]" ).parent ().tipsy ( $.extend ( { title : function ()
						{
							return $(this).find ( "[original-title]" ).eq(0).attr ( "original-title" );
						} } , tsbp.tsbp_tipsy_parameters ) );
						
						changestate ( "requesting" );
						latest_detail_promise = pointinfo[0].detail_promise;
						pointinfo[0].detail_promise.done ( function ( data , textStatus , xhr )
						{
							// because promises aren't cancelable, we have to do an "are we still wanted?" check in this really stupid way
							if ( latest_detail_promise !== pointinfo[0].detail_promise )
								return;
							
							if ( data.panel_inner_html == null )
							{
								changestate ( "error" );
								return;
							}
							
							emptycontent ();

							content.append ( data.panel_inner_html );

							// don't really like this - requires too much cooperation between contents & fetcher
							content.find ( "[title]" ).tipsy ( tsbp.tsbp_tipsy_parameters );
							content.find ( "._dynamicunit" ).each ( function ( i , element )
							{
								tsbp.DynamicUnit ( $(element) , "tsbp_" );
							} );

							changestate ( "idle" );
						} ).fail ( function ()
						{
							// because promises aren't cancelable, we have to do an "are we still wanted?" check in this really stupid way
							if ( latest_detail_promise !== pointinfo[0].detail_promise )
								return;
							
							changestate ( "error" );
						} ).always ( function ()
						{
							latest_detail_promise = null;
						} );
					} ).always ( function ()
					{
						latest_pointinfo_promise = null;
					} );
				}
				else
				{
					emptycontent ();

					changestate ( "idle" );
				}
			}
		}
		else
		{
			tle.trigger ( "setExpanded" , false );
			emptycontent ();

			detailheading.text ( "Not selected" );
		}
	};
	$(window).on ( "augmentedhashchange" , augmentedhashchangefunction );

	return tle;
};

window.tsbp.CostOverview = function ( initial_element )
{
	var co = initial_element.addClass ( "_costoverview _panel" );

	// functions that generate d parameter contents for a path element
	var d_functions = {
		"line" : function ( data_src )
		{
			var x_factor = co.data ( "fitX" ) / (data_src.length-1);
			var max_y = Math.max.apply ( null , data_src );
			var y_factor = co.data ( "fitY" ) / max_y;

			var d = "M";
			for ( var j = 0 ; j < data_src.length ; j++ )
			{
				if ( data_src[j] != null )
					d += ( j * x_factor ) + " " + ( data_src[j] * y_factor ) + " ";
			}

			return d;
		} ,
		"block" : function ( data_src )
		{
			var x_factor = co.data ( "fitX" ) / (data_src.length-1);
			var y_factor = co.data ( "fitY" );

			var d = "";
			var path_open = false;
			for ( var j = 0 ; j < data_src.length ; j++ )
			{
				if ( data_src[j] && !path_open )
				{
					d += "M" + ( j * x_factor ) + " 0V" + y_factor;
					path_open = true;
				}
				else if ( path_open && !data_src[j] )
				{
					d += "H" + ( j * x_factor ) + "V0Z";
					path_open = false;
				}
			}
			
			if ( path_open )
				// note finishing on length, not length-1
				d += "H" + ( data_src.length * x_factor ) + "V0Z";

			return d;
		}
	};

	co.find ( "._costoverview_series" ).each ( function ( i , element )
	{
		var elementjq = $(element);
		var data_src = elementjq.data ( "src" );
		var data_series_type = elementjq.data ( "seriesType" );
		if ( data_src && data_src.length && data_series_type )
			elementjq.attr ( "d" , d_functions[data_series_type] ( data_src ) );
	} );

	co.on ( "newData" , function ( event , data_src , series_label )
	{
		co.find ( "._costoverview_series[data-label='" + series_label + "']" ).each ( function ( i , element )
		{
			var elementjq = $(element);
			var data_series_type = elementjq.data ( "seriesType" );
			elementjq.data ( "src" , data_src );
			if ( data_src && data_src.length && data_series_type )
				elementjq.attr ( "d" , d_functions[data_series_type] ( data_src ) );
		} );
	} );

	return co;
};

} ) ();
