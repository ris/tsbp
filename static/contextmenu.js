(function ()
{

window.tsbp.ContextMenu = function ( initial_element , extra_listeners )
{
	var cm = initial_element.addClass ( "_contextmenu" );
	var beard_length = 32;
	var base_listeners = cm;
	if ( extra_listeners )
		base_listeners = base_listeners.add ( extra_listeners );
	var current_target;
	var opened_timestamp;

	// first we check what css had set the desired margin to so we can later respect it
	var original_margin = parseInt ( cm.css ( "margin-left" ) );
	// then we clear it so it doesn't interfere with our positioning
	cm.css ( "margin" , 0 );
	
	var state = "initial";
	var changestate = function ( new_state )
	{
		if ( state === new_state )
			return;

		cm.removeClass ( window.tsbp.state_classes[state] );
		state = new_state;
		cm.addClass ( window.tsbp.state_classes[state] );
	};
	changestate ( "disabled" );

	var hidecontextmenufunction = function ()
	{
		current_target = opened_timestamp = null;
		changestate ( "disabled" );
	};

	cm.on ( "mousedown" , false );

	cm.on ( "mousemove mouseover" , "button" , function ( event )
	{
		if ( $.isFunction ( event.target.focus ) )
			event.target.focus ();
	} );

	cm.on ( "click mouseup" , "button" , function ( event )
	{
		if ( ! event.target.value )
			return;

		if ( state !== "enabled" )
			return;

		var listeners = base_listeners;
		if ( current_target != null )
			listeners = listeners.add ( current_target );

		// use the target's value as the event name
		listeners.trigger ( event.target.value );

		hidecontextmenufunction ();
	} );

	$(document).on ( "hideContextMenu" , hidecontextmenufunction );
	$(document).on ( "mousedown click" , function ( event )
	{
		// wish i could use real event timestamps for this, but firefox has a critical
		// bug that gives us broken event timestamps.
		//
		// rule here is menu can't be closed through these events unless it's already been
		// open for 200ms - otherwise this could be the same event that caused the requestContextMenu
		// having bubbled up
		if ( (new Date ()).getTime () - opened_timestamp > 200 )
			hidecontextmenufunction ();
	} );

	$(document).on ( "requestContextMenu" , function ( event , pos_in_target , seen_intents , selection_indexes , series_length )
	{
		current_target = event.target;
		opened_timestamp = (new Date ()).getTime ();

		// figure out which menu options should be shown
		cm.find ( "button" ).css ( "display" , function ()
		{
			var current_element = this;
			if ( current_element.name === "intent_transition" )
			{
				var should_appear = false;
				$.each ( seen_intents , function ( k , v )
				{
					if ( window.tsbp.intent_cosmetic_transition_matrix[k][window.tsbp.intent_transition[current_element.value]] != k )
					{
						should_appear = true;
						return false;
					}
				} );
				return should_appear ? "" : "none";
			} else if ( current_element.value === "selectbefore" )
			{
				if ( selection_indexes && selection_indexes.length && selection_indexes[0] )
					return "";
				else
					return "none";
			} else if ( current_element.value === "selectafter" )
			{
				if ( selection_indexes && selection_indexes.length && selection_indexes[selection_indexes.length-1] < (series_length-1) )
					return "";
				else
					return "none";
			}
		} );

		if ( selection_indexes && selection_indexes.length > 1 )
		{
			cm.find ( "._contextmenu_only_plural" ).css ( "display" , "" );
			cm.find ( "._contextmenu_only_singular" ).css ( "display" , "none" );
		}
		else
		{
			cm.find ( "._contextmenu_only_plural" ).css ( "display" , "none" );
			cm.find ( "._contextmenu_only_singular" ).css ( "display" , "" );
		}

		changestate ( "enabled" );

		var pos = $(current_target).offset ();

		pos.left += pos_in_target.left;
		pos.top += pos_in_target.top;

		// assuming window size > menu size for now
		
		var css_obj = {
			"top" : Math.max ( original_margin , pos.top - ( cm.height () + beard_length ) )
		};
		if ( pos.left + cm.width () / 2 > cm.offsetParent().width () - ( cm.width () + original_margin ) )
		{
			css_obj.left = pos.left - cm.width ();
			cm.attr ( "data-contextmenu-alignment" , "se" );
		} else if ( pos.left - cm.width () / 2 < original_margin )
		{
			css_obj.left = pos.left;
			cm.attr ( "data-contextmenu-alignment" , "sw" );
		} else
		{
			css_obj.left = pos.left - cm.width () / 2;
			cm.attr ( "data-contextmenu-alignment" , "s" );
		}

		cm.css ( css_obj );

		if ( $.isFunction ( cm[0].focus ) )
			cm[0].focus ();
	} );

	// for browsers that support it
	cm.on ( "focusout" , "*" , function ( event )
	{
		if ( ! $(event.target).has ( event.relatedTarget ) )
			hidecontextmenufunction ();
	} );

	cm.removeClass ( "initially_undisplayed" );

	return cm;
};

window.tsbp.PinningContextMenu = function ( initial_element , extra_listeners )
{
	var pcm = ContextMenu ( initial_element , extra_listeners ).addClass ( "_pinningcontextmenu" );
	
	var content_state = "initial";
	var changecontentstate = function ( new_state )
	{
		if ( content_state === new_state )
			return;

		pcm.removeClass ( "_content" + window.tsbp.state_classes[content_state] );
		content_state = new_state;
		pcm.addClass ( "_content" + window.tsbp.state_classes[content_state] );
	};
	changecontentstate ( "idle" );
	
	var latest_pointinfo_promise , latest_detail_promise;
	
	var augmentedhashchangefunction = function ( event , augmented_token_array , extra_options )
	{
		var selectedindexes = ( augmented_token_array[0] && augmented_token_array[0].type === "track" && augmented_token_array[1] && augmented_token_array[1].type === "trackpoint" && augmented_token_array[1].selectedindexes ) || [];
		pcm.empty ();
		
		if ( selectedindexes.length === 1 )
		{
			latest_pointinfo_promise = augmented_token_array[1].pointinfo_promise;
			changecontentstate ( "requesting" );
			
			augmented_token_array[1].pointinfo_promise.done ( function ( pointinfo )
			{
				// because promises aren't cancelable, we have to do an "are we still wanted?" check in this really stupid way
				if ( latest_pointinfo_promise !== augmented_token_array[1].pointinfo_promise )
					return;
				
				latest_detail_promise = pointinfo[0].detail_promise;
				pointinfo[0].detail_promise.done ( function ( data , textStatus , xhr )
				{
					// because promises aren't cancelable, we have to do an "are we still wanted?" check in this really stupid way
					if ( latest_detail_promise !== pointinfo[0].detail_promise )
						return;
					
					
				} );
			} ).always ( function ()
			{
				latest_pointinfo_promise = null;
			} );
		}
		else
		{
			latest_pointinfo_promise = latest_detail_promise = null;
			changecontentstate ( "disabled" );
		}
	};
	
	
	pcm.removeClass ( "initially_undisplayed" );

	return pcm;
};

} ) ();
