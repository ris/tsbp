# -*- coding: utf-8 -*-

def decode_line(encoded):
	"""
		Decodes a polyline that was encoded using the Google Maps method.
	
		See http://code.google.com/apis/maps/documentation/polylinealgorithm.html
		
		This is a straightforward Python port of Mark McClure's JavaScript polyline decoder
		(http://facstaff.unca.edu/mcmcclur/GoogleMaps/EncodePolyline/decode.js)
		and Peter Chng's PHP polyline decode
		(http://unitstep.net/blog/2008/08/02/decoding-google-maps-encoded-polylines-using-php/)
	
	Author (See Wah Cheng http://seewah.blogspot.co.uk/2009/11/gpolyline-decoding-in-python.html) seems to imply no copyright.
	
	Adapted by ris to work as a generator and return the coordinates in a sane order.
	"""

	encoded_len = len(encoded)
	index = 0
	lat = 0
	lng = 0

	while index < encoded_len:

		b = 0
		shift = 0
		result = 0

		while True:
			b = ord(encoded[index]) - 63
			index = index + 1
			result |= (b & 0x1f) << shift
			shift += 5
			if b < 0x20:
				break

		dlat = ~(result >> 1) if result & 1 else result >> 1
		lat += dlat

		shift = 0
		result = 0

		while True:
			b = ord(encoded[index]) - 63
			index = index + 1
			result |= (b & 0x1f) << shift
			shift += 5
			if b < 0x20:
				break

		dlng = ~(result >> 1) if result & 1 else result >> 1
		lng += dlng

		yield ( lng * 1e-6 , lat * 1e-6 )

def encoded_polyline_to_geojson_dict ( encoded ):
	return {
		"type" : "Feature" ,
		"geometry" : {
			"type" : "LineString" ,
			"coordinates" : list ( decode_line ( encoded ) ) ,
		} ,
		"properties" : {} ,
	}



def _rel_from_abs ( pfrom , pto ):
	return pto[0] - pfrom[0] , pto[1] - pfrom[1]

def _dbl_rel_from_abs ( pfrom , pto ):
	rfa = _rel_from_abs ( pfrom , pto )
	return rfa , rfa

def _dp_unpack ( a , x ):
	b , c = x
	return a[0] * b[0] + a[1] * b[1] , c

def iter_distance_minima_points ( linestring , point ):
	"""
		returns an iterator yielding points of distance minima between linestring and point
	"""
	from math import hypot
	from itertools import chain , repeat , izip , tee , imap , starmap
	from tools import iterwindow , iterrepeateach , iterdedup
	from django.contrib.gis.geos import Point
	
	if linestring.srid != point.srid:
		raise ValueError ( "Both geometrys' SRIDs must match" )
	
	target_srid = linestring.srid
	
	if len ( linestring.coords ) < 2:
		raise ValueError ( "Fewer than two points in a linestring?" )
	
	# using dot products to find places where point is on the normal of the linestring. when the dot product goes negative we know we've switched from going
	# towards point to going away from it.
	
	# we're lucky in that we're only interested in the _sign_ of the dot product, so we don't have to worry about or doing acos()s multiplying by hypots unless we've
	# actually found a minima.
	
	distance_running_sum = 0
	
	# FIXME materializing this means we're no longer actually a zero-copy algorithm, but to fix that otherwise we'd have to fix our use
	# of chain() and the linestring ends down below, which is something i'm not going to do right now.
	dd_coords = tuple ( iterdedup ( linestring ) )
	
	# iterate alternately along real segments and imaginary, zero-length segments that exist at corners. this is because the logic for dealing with both is quite similar.
	# end points have an imaginary segment added going back on themselves, so that the resulting "sweep" made by the dot products captures any minima in endpoints. only thing
	# is that direction vectors have to be fiddled with there, hence the slightly fiddly use of chain ().
	# what a beautiful iterator chain.
	it0 , it1 , it2 = tee ( dd_coords , 3 )
	for ( segpoint_a , ( dot_product_a , direction_vector_a ) ) , ( segpoint_b , ( dot_product_b , direction_vector_b ) ) in iterwindow ( izip ( iterrepeateach ( it0 ) , imap ( _dp_unpack , iterrepeateach ( imap ( _rel_from_abs , it1 , repeat ( point ) ) ) , chain ( ( (_rel_from_abs ( dd_coords[1] , dd_coords[0] ),_rel_from_abs ( dd_coords[0] , dd_coords[1] )) ,) , iterrepeateach ( starmap ( _dbl_rel_from_abs , iterwindow ( it2 , 2 ) ) ) , ( (_rel_from_abs ( dd_coords[-1] , dd_coords[-2] ),_rel_from_abs ( dd_coords[-2] , dd_coords[-1] )) ,) ) ) ) , 2 ):
		
		# have to keep track of total length to be able to report back
		if segpoint_a is segpoint_b:
			# "segment" is zero-length - a corner
			seg_hypot = 0
		else:
			# on segments that aren't zero-length direction_vector_a will = direction_vector_b = the segment vector
			seg_hypot = hypot ( *direction_vector_a )
		
		if dot_product_a > 0 and not dot_product_b > 0:
			# we have a minima on this segment
			if seg_hypot:
				# calculate nearest point on this segment
				distance_along_seg = dot_product_a / seg_hypot
				proportion_along_seg = distance_along_seg / seg_hypot
				vector_from_segpoint_a = direction_vector_a[0] * proportion_along_seg , direction_vector_a[1] * proportion_along_seg
				near_point = segpoint_a[0] + vector_from_segpoint_a[0] , segpoint_a[1] + vector_from_segpoint_a[1]
				near_point_direction_unit_vector = direction_vector_a[0] / seg_hypot , direction_vector_a[1] / seg_hypot
			else:
				# "segment" is zero-length - a corner - we don't have to find the position on the line
				near_point = segpoint_a
				# but we do have to calculate the direction vector from two neighbouring real segments
				direction_vector_hypot_a = hypot ( *direction_vector_a )
				direction_unit_vector_a = direction_vector_a[0] / direction_vector_hypot_a , direction_vector_a[1] / direction_vector_hypot_a
				direction_vector_hypot_b = hypot ( *direction_vector_b )
				direction_unit_vector_b = direction_vector_b[0] / direction_vector_hypot_b , direction_vector_b[1] / direction_vector_hypot_b
				combined_direction_vector = direction_unit_vector_a[0] + direction_unit_vector_b[0] , direction_unit_vector_a[1] + direction_unit_vector_b[1]
				combined_direction_vector_hypot = hypot ( *combined_direction_vector )
				
				if combined_direction_vector_hypot:
					near_point_direction_unit_vector = combined_direction_vector[0] / combined_direction_vector_hypot , combined_direction_vector[1] / combined_direction_vector_hypot
				else:
					# combined_direction_vector_hypot could be zero as a result of the previous two segments going back on each other and
					# cancelling each other out. there is no real right answer here - I'd call it a degenerate case, so just grab the
					# normalized direction_vector_a as it shouldn't be possible for it to be 0 due to our iterdedup filtering and our start &
					# end point mangling.
					near_point_direction_unit_vector = direction_unit_vector_a
				
				distance_along_seg = 0
			
			yield Point ( near_point , srid = target_srid ) , near_point_direction_unit_vector , distance_running_sum + distance_along_seg
	
		distance_running_sum += seg_hypot

def parse_poly_file ( poly_file ):
	"""
		Parse an Osmosis polygon filter file.

		Accept a sequence of lines from a polygon file, return a django.contrib.gis.geos.MultiPolygon object.

		http://wiki.openstreetmap.org/wiki/Osmosis/Polygon_Filter_File_Format
		
		Adapted from http://wiki.openstreetmap.org/wiki/Osmosis/Polygon_Filter_File_Python_Parsing
	"""
	from django.contrib.gis.geos import MultiPolygon , Polygon
	in_ring = False
	coords = []
	
	for index , line in enumerate ( poly_file ):
		if index == 0:
			# first line is junk.
			continue
		
		#elif index == 1:
			## second line is the first polygon ring.
			#coords.append ( [ [] , [] ] )
			#ring = coords[-1][0]
			#in_ring = True
		
		elif in_ring and line.strip () == 'END':
			# we are at the end of a ring, perhaps with more to come.
			in_ring = False
	
		elif in_ring:
			# we are in a ring and picking up new coordinates.
			ring.append ( map ( float , line.split () ) )
	
		elif not in_ring and line.strip () == 'END':
			# we are at the end of the whole polygon.
			break
	
		elif not in_ring and line.startswith ( '!' ):
			# we are at the start of a polygon part hole.
			coords[-1].append ( [] )
			ring = coords[-1][-1]
			in_ring = True
	
		elif not in_ring:
			# we are at the start of a polygon part.
			coords.append ( [[]] )
			ring = coords[-1][0]
			in_ring = True
	
	return MultiPolygon ( *(Polygon ( *polycoords ) for polycoords in coords) )
