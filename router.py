import simplejson as json
from django.contrib.gis.geos import Point
from django.conf import settings

from eventlet.green.httplib import HTTPConnection , BadStatusLine
import logging
from itertools import chain
from collections import OrderedDict

from functools32 import lru_cache

from access import transports_by_label

logger = logging.getLogger ( "tsbp.router" )
_hint_lru_cache_max_size = 4096

class Router ( object ):
	def __init__ ( self , transport ):
		self.transport = transport
		self.request_count = 0
		self._hint_lru_cache = OrderedDict ()
	
	# this is a stupid flag for a stupid old debug message design
	debug = False
	
	def get_connection ( self ):
		return HTTPConnection ( settings.TSBP_AVAILABLE_TRANSPORTS[self.transport["label"]]["OSRM_HOST"] )
	
	def get_route ( self , point_from , osm_way_id_from , point_to , osm_way_id_to , instructions = True , geometry = True , with_alternatives = False ):
		point_from_4326 = point_from.transform ( 4326 , clone = True )
		point_to_4326 = point_to.transform ( 4326 , clone = True )
		
		# it would be nice to use e.g. @lru_cache here, but we need a lookaside cache, not a write-through cache, so
		# we need to implement it mostly from scratch.
		# pop out any existing hints. if they exist they will get replaced at the top of the cache once we're done.
		cache_key_from = ( osm_way_id_from , point_from_4326.coords[0] , point_from_4326.coords[1] )
		hint_from = self._hint_lru_cache.pop ( cache_key_from , None )
		cache_key_to = ( osm_way_id_to , point_to_4326.coords[0] , point_to_4326.coords[1] )
		hint_to = self._hint_lru_cache.pop ( cache_key_to , None )
		
		if hint_from and hint_to and hint_from[1] != hint_to[1]:
			# our checksums don't match - best not to use either
			hint_from = hint_to = None
		
		checksum = ( hint_from and hint_from[1] ) or ( hint_to and hint_to[1] )
		
		connection = self.get_connection ()
		query_string = "/viaroute?loc=%s,%s%s&osmwayid=%i&loc=%s,%s%s&osmwayid=%i&instructions=%s&geometry=%s&output=json&alt=%s&z=18%s" % (
			point_from_4326.coords[1] ,
			point_from_4326.coords[0] ,
			( "&hint=%s" % hint_from[0] ) if hint_from is not None else "" ,
			osm_way_id_from ,
			point_to_4326.coords[1] ,
			point_to_4326.coords[0] ,
			( "&hint=%s" % hint_to[0] ) if hint_to is not None else "" ,
			osm_way_id_to ,
			str(instructions).lower () ,
			str(geometry).lower () ,
			str(with_alternatives).lower () ,
			( "&checksum=%s" % checksum ) if checksum is not None else "" ,
		)
		logger.debug ( "sending routing request to router %s" , query_string )
		connection.request ( "GET" , query_string )
		
		try:
			response = connection.getresponse ()
		except BadStatusLine , e:
			logger.error ( "Caught %s: request was: %s" , e , query_string )
			raise e
		
		logger.debug ( "got response %s" , response.status )
		if response.status != 200:
			raise EnvironmentError
		
		self.request_count += 1
		
		response_str = response.read ()
		logger.debug ( "got response content %s" , response_str )
		response_json = json.loads ( response_str )
		
		# write back cache entries
		self._hint_lru_cache[cache_key_from] = response_json["hint_data"]["locations"][0] , response_json["hint_data"]["checksum"]
		self._hint_lru_cache[cache_key_to] = response_json["hint_data"]["locations"][1] , response_json["hint_data"]["checksum"]
		
		# enforce cache size limit - not tremendously efficient for large removals but we should only
		# really ever have to evict max 2 at a time.
		while len ( self._hint_lru_cache ) > _hint_lru_cache_max_size:
			evicted_item = self._hint_lru_cache.popitem ( last = False )
			logger.debug ( "Evicted item %s from _hint_lru_cache" , evicted_item )
		
		return response_json
	
	# actual float inf isn't reliable enough to use
	seconds_metres_pseudo_inf = ( 1.e8 , 1.e8 )
	
	def seconds_metres_from_to ( self , point_from , osm_way_id_from , point_to , osm_way_id_to , with_alternatives = False ):
		route_data = self.get_route ( point_from , osm_way_id_from , point_to , osm_way_id_to , instructions = False , geometry = False , with_alternatives = with_alternatives )
		result_pairs = []
		summary_iter = iter ( (route_data["route_summary"],) )
		if with_alternatives and route_data.get ( "alternative_summaries" ):
			summary_iter = chain ( summary_iter , route_data["alternative_summaries"] )
		
		for summary in summary_iter:
			if route_data["status"] != 0 and not summary["total_time"]:
				logger.debug ( "no route found? route_data: %s" , route_data )
				result_pairs.append ( self.seconds_metres_pseudo_inf )
			else:
				result_pairs.append ( ( float ( summary["total_time"] ) , float ( summary["total_distance"] ) ) )
		
		return result_pairs if with_alternatives else result_pairs[0]
	
	def seconds_from_to ( self , *args , **kwargs ):
		result = self.seconds_metres_from_to ( *args , **kwargs )
		return [ r[0] for r in result ] if kwargs.get ( "with_alternatives" ) else result[0]
	
	def metres_from_to ( self , *args , **kwargs ):
		result = self.seconds_metres_from_to ( *args , **kwargs )
		return [ r[1] for r in result ] if kwargs.get ( "with_alternatives" ) else result[1]
	
	def way_id_set_from_to ( self , point_from , osm_way_id_from , point_to , osm_way_id_to , alternative_index = 0 ):
		route_data = self.get_route ( point_from , osm_way_id_from , point_to , osm_way_id_to , with_alternatives = bool ( alternative_index ) )
		try:
			route_instructions = route_data["route_instructions"] if not alternative_index else route_data["alternative_instructions"][alternative_index]
		except IndexError:
			route_instructions = ()
		# newer OSRMs have a tendency to output a trailing, name-less (id-less in our case) instruction, so we have to be able to cope with an empty field.
		return frozenset ( chain ( ( int ( instruction[1] ) for instruction in route_instructions if instruction[1] ) , ( osm_way_id_from , osm_way_id_to ) ) )
	
	# adding the lru_cache decorator directly to the method means the cache will belong
	# to the class, but that should be ok as it'll also be keyed by the self argument
	# meaning we should get the right results back
	@lru_cache ( maxsize = 1024 )
	def _allowed_on_way_is_oneway ( self , osmway ):
		"""
			@point is needed to help OSRM in looking way up as it's not really designed for this
		"""
		point_4326 = Point ( osmway.way.coords[0] , srid = osmway.way.srid ).transform ( 4326 , clone = True )
		
		connection = self.get_connection ()
		query_string = "/nearest?loc=%s,%s&osmwayid=%i" % ( point_4326.coords[1] , point_4326.coords[0] , osmway.osm_id )
		logger.debug ( "sending presence request to router %s" , query_string )
		connection.request ( "GET" , query_string )
		
		response = connection.getresponse ()
		logger.debug ( "got response status %s" , response.status )
		if response.status != 200:
			raise EnvironmentError
		
		response_str = response.read ()
		
		logger.debug ( "got response content %s" , response_str )
		
		response_json = json.loads ( response_str )
		
		return bool ( response_json["mapped_coordinate"] ) , response_json["oneway"]
	
	def allowed_on_way ( self , osmway ):
		return self._allowed_on_way_is_oneway ( osmway )[0]
	
	def is_oneway ( self , osmway ):
		"""
			Determining oneway from the router currently has the potential to be tricked by ways that go back on themselves
			but we'll sort that out sometime in the future
		"""
		return self._allowed_on_way_is_oneway ( osmway )[1]

auxiliary_routers = dict ( ( ( label , Router ( transports_by_label[label] ) ) for label in settings.TSBP_AVAILABLE_TRANSPORTS.iterkeys () ) )
