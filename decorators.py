# -*- coding: utf-8 -*-
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404 , HttpResponseForbidden , HttpResponseGone

from functools import wraps

from models import Solution

def solution_view ( func ):
	"""
	Decorator to wrap views that deal with solutions.
	"""
	@wraps ( func )
	def innerfunc ( request , *args , **kwargs ):
		try:
			solution = Solution.objects.get ( id = int ( kwargs.pop ( "solution_id" ) ) )
		except ( ValueError , ObjectDoesNotExist ):
			raise Http404
		
		kwargs["solution"] = solution
		
		return func ( request , *args , **kwargs )
	
	return innerfunc

def solution_view_by_trace_chain ( func ):
	"""
	Decorator to wrap views that deal with solutions.
	"""
	@wraps ( func )
	def innerfunc ( request , *args , **kwargs ):
		try:
			solution = Solution.objects.filter ( trace_activation__trace = int ( kwargs.pop ( "trace_id" ) ) ).filter ( trace_activation__index_by_trace = int ( kwargs.pop ( "trace_activation_index" ) ) ).get ( index_by_trace_activation = int ( kwargs.pop ( "solution_index" ) ) )
		except ( ValueError , ObjectDoesNotExist ):
			raise Http404
		
		kwargs["solution"] = solution
		
		return func ( request , *args , **kwargs )
	
	return innerfunc

def solutiontrack_view ( func ):
	"""
	Decorator to wrap views that deal with solutiontracks.
	"""
	@wraps ( func )
	def innerfunc ( request , *args , **kwargs ):
		try:
			solutiontrack = kwargs["solution"].solutiontrack_set.get ( track_index = int ( kwargs.pop ( "track_index" ) ) )
		except ( ValueError , ObjectDoesNotExist ):
			raise Http404
		
		kwargs["solutiontrack"] = solutiontrack
		
		return func ( request , *args , **kwargs )
	
	return innerfunc

def solutiontrackpoint_view ( func ):
	"""
	Decorator to wrap views that deal with solutiontrackpoints.
	"""
	@wraps ( func )
	def innerfunc ( request , *args , **kwargs ):
		try:
			solutiontrackpoint = kwargs["solutiontrack"].solutiontrackpoint_set.get ( point_index = int ( kwargs.pop ( "point_index" ) ) )
		except ( ValueError , ObjectDoesNotExist ):
			raise Http404
		
		kwargs["solutiontrackpoint"] = solutiontrackpoint
		
		return func ( request , *args , **kwargs )
	
	return innerfunc

def passinlineexceptions ( func ):
	"""
	checks for exceptions being passed to @func and returns them straight. also catches exceptions raised in calling @func and returns them inline
	
	intended to help with handling exceptions in eventlet greenthreads
	"""
	@wraps ( func )
	def innerfunc ( x , *args , **kwargs ):
		if isinstance ( x , Exception ):
			return x
		else:
			try:
				return func ( x , *args , **kwargs )
			except Exception , e:
				return e
	
	return innerfunc
