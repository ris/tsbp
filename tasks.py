# -*- coding: utf-8 -*-
from django.contrib.gis.geos import Point , LineString
from django.db import transaction
from django.utils import simplejson as json
from django.core.files.base import ContentFile
from django.conf import settings

from celery import task
from celery.exceptions import RetryTaskError

from itertools import chain , izip , izip_longest , imap
from tools import iterpad , iterwindow , iterfractionalsplit
from math import sqrt , floor , ceil
import cPickle as pickle
from sys import setrecursionlimit , getrecursionlimit
import logging
from datetime import datetime
from sys import exc_info
from traceback import format_exception

from models import Solution , SolutionTrack , SolutionTrackPoint , Trace , TraceActivation , PlanetRevision
from track import UnsolvableRouteError , ZeroRipenessError
from gpx import GPX
from log import StackScrapingLogFilter , AnnotationShowingLogFormatter
from defaults import TSBP_REPLICATION_CURRENT_SEQUENCE_NAMESPACE
from access import transports_by_label


logger = logging.getLogger ( "tsbp" )

_pickle_recursion_limit = 40000

_budget_multiplier_initial = 9
_budget_multiplier_following = 4

_gpx_max_total_points = getattr ( settings , "TSBP_GPX_MAX_TOTAL_POINTS" , 10e3 )

_namestartchar = ur"A-Z_a-z\u00c0-\u00d6\u00d8-\u00f6\u00f8-\u02ff\u0370-\u037d\u037f-\u1fff\u200c-\u200d\u2070-\u218f\u2c00-\u2fef\u3001-\uD7ff\uf900-\ufdcf\ufdf0-\ufffd\U00010000-\U000effff"
_namechar = ur"%s\-.0-9\u00b7\u0300-\u036f\u0203f-\u2040" % _namestartchar

class PublicError ( Exception ):
	pass

def _interpolate_cost ( d , costs ):
	floor_d = floor ( d )
	if floor_d == d:
		return costs[int(d)]
	ceil_d = ceil ( d )
	return costs[ int ( floor ( d ) ) ] * ( ceil ( d ) - d ) + costs[ int ( ceil ( d ) ) ] * ( d - floor ( d ) )

def _interpret_solved_track ( track , solution , track_index , solution_exhausted = False ):
	solution_track = SolutionTrack ( solution = solution , track_index = track_index )
	if solution_exhausted:
		solution_track.outcome = SolutionTrack.OUTCOME_CHOICES_INVERSE["exhausted"]
	solution_track.save ()
	
	x_min = x_max = y_min = y_max = None
	
	try:
		global_iter = enumerate ( chain.from_iterable ( track ) )
		
		transitions_iter = ( ( t.raw_matrix[left_index,right_index] , left_index , right_index ) for t , left_index , right_index in track.routable_track.transitions.collapse_to_transitions () )
		routable_iter = enumerate ( izip ( track.routable_track , iterwindow ( iterpad ( transitions_iter , 1 ) , 2 ) ) )
	
		visual_costs = []
		
		# TODO combine loops, make less wasteful etc.
		
		for j , ( routable_trackpoint , ( left_transition_pows , right_transition_pows ) ) in routable_iter:
			left_transition_cost = 0
			right_transition_cost = 0
			if left_transition_pows is not None:
				left_transition_cost , left_neighbour_pow_index , self_pow_index = left_transition_pows
				point_intrinsic_cost = routable_trackpoint.calculated_candidates[self_pow_index].intrinsic_cost
				if right_transition_pows is None:
					left_transition_cost *= 2
			if right_transition_pows is not None:
				right_transition_cost , self_pow_index , right_neighbour_pow_index = right_transition_pows
				point_intrinsic_cost = routable_trackpoint.calculated_candidates[self_pow_index].intrinsic_cost
				if left_transition_pows is None:
					right_transition_cost *= 2
			
			cost = point_intrinsic_cost + left_transition_cost + right_transition_cost
			
			for i , global_trackpoint in global_iter:
				gpx_point = global_trackpoint.point.transform ( 4326 , clone = True )
				gpx_point_x = gpx_point.get_x ()
				gpx_point_y = gpx_point.get_y ()
				if x_min is None or gpx_point_x < x_min:
					x_min = gpx_point_x
				if x_max is None or gpx_point_x > x_max:
					x_max = gpx_point_x
				if y_min is None or gpx_point_y < y_min:
					y_min = gpx_point_y
				if y_max is None or gpx_point_y > y_max:
					y_max = gpx_point_y
				stp = SolutionTrackPoint ( solution_track = solution_track , point_index = i , seg_index = 0 , routable = global_trackpoint is routable_trackpoint , gpx_point = gpx_point , cost = cost if global_trackpoint is routable_trackpoint else None , current_candidate_index = self_pow_index if global_trackpoint is routable_trackpoint else None , trackpoint = global_trackpoint )
				stp.save ()
				
				visual_costs.append ( stp.visual_cost )
				
				if global_trackpoint is routable_trackpoint:
					# need to grab a new trackpoint from routable_iter
					break
		
	except UnsolvableRouteError:
		solution_track.outcome = SolutionTrack.OUTCOME_CHOICES_INVERSE["unsolvable"]
		# other than that, that's fine, we'll just continue...
	
	# emit trailing members of global_iter
	for i , global_trackpoint in global_iter:
		gpx_point = global_trackpoint.point.transform ( 4326 , clone = True )
		gpx_point_x = gpx_point.get_x ()
		gpx_point_y = gpx_point.get_y ()
		if x_min is None or gpx_point_x < x_min:
			x_min = gpx_point_x
		if x_max is None or gpx_point_x > x_max:
			x_max = gpx_point_x
		if y_min is None or gpx_point_y < y_min:
			y_min = gpx_point_y
		if y_max is None or gpx_point_y > y_max:
			y_max = gpx_point_y
		stp = SolutionTrackPoint ( solution_track = solution_track , point_index = i , seg_index = 0 , routable = False , gpx_point = gpx_point , cost = None , current_candidate_index = None , trackpoint = global_trackpoint )
		stp.save ()
		
		visual_costs.append ( stp.visual_cost )
	
	try:
		lsct = list ( track.routable_track.transitions.collapse_to_linestring_coord_quadruples ( simplify_precision = 2.0 ) )
		linestring = LineString ( [ ( x , y ) for x , y , d , c in lsct ] , srid = 900913 )
		
		# simplify to whatever's approx 5m accuracy at this latitude
		simplified_linestring = linestring.simplify ( tolerance = 5 / track.routable_track[0].local_scale_factor )
		simplified_linestring.transform ( 4326 )
		solution_track.simplified_linestring = simplified_linestring
		
		visual_routable_costs = [ c for c in visual_costs if c is not None ]
		
		linestring.transform ( 4326 )
		solution_track.costed_linestring_geojson = json.dumps ( {
			"type" : "LineString" ,
			"coordinates" : [ ( x , y , _interpolate_cost ( d , visual_routable_costs ) , d , c ) for ( x , y ) , ( x0 , y0 , d , c ) in izip ( linestring.coords , lsct ) ] ,
		} )
		
		# generate arrays for cost overview
		cost_overview_cost = []
		cost_overview_nulls = []
		for costs in iterfractionalsplit ( visual_costs , min ( 1 , 128 / float ( len ( visual_costs ) ) ) ):
			max_cost = None
			has_nulls = False
			for cost in costs:
				if cost is None:
					has_nulls = True
				else:
					max_cost = max ( cost , max_cost or 0 )
			cost_overview_cost.append ( max_cost )
			cost_overview_nulls.append ( has_nulls )
		solution_track.cost_overview_cost_json = json.dumps ( cost_overview_cost )
		solution_track.cost_overview_nulls_json = json.dumps ( cost_overview_nulls )
		
		solution_track.annotated_pinnings_json = json.dumps ( track.annotated_user_pinnings )
		
	except UnsolvableRouteError:
		# may as well make sure this is set
		solution_track.outcome = SolutionTrack.OUTCOME_CHOICES_INVERSE["unsolvable"]
	
	if x_min is not None:
		from django.contrib.gis.geos import MultiPoint , Point
		solution_track.all_points_bbox_extents = MultiPoint ( Point ( x_min , y_min ) , Point ( x_max , y_max ) , srid = 4326 )
	
	if hasattr ( track , "metadata" ):
		solution_track.metadata_json = json.dumps ( track.metadata )
	
	solution_track.save ()

def _pickle_to_solution ( gpx , solution ):
	previous_recursion_limit = getrecursionlimit ()
	setrecursionlimit ( _pickle_recursion_limit )
	
	logger.info ( "Pickling gpx instance" )
	solution.pickled_gpx.save ( "%i_%i_%i.pickle" % ( solution.trace_activation.trace.id , solution.trace_activation.index_by_trace , solution.index_by_trace_activation ) , ContentFile ( pickle.dumps ( gpx , -1 ) ) )
	
	setrecursionlimit ( previous_recursion_limit )

def _setup_log_handler ( log_file , sentinel_local , gpx_localname ):
	solution_log_handler = logging.StreamHandler ( log_file )
	solution_log_handler.setFormatter ( AnnotationShowingLogFormatter ( "[%(asctime)s]:%(levelname)s:%(name)s:%(message)s" ) )
	solution_log_handler.addFilter ( StackScrapingLogFilter ( sentinel_local , gpx_localname ) )
	
	solution_logger = logging.getLogger ( "tsbp" )
	solution_logger.addHandler ( solution_log_handler )
	solution_logger.setLevel ( logging.INFO )
	
	return solution_logger , solution_log_handler

def _get_track_intent_params ( intents , track_index ):
	tracks_dict = intents.get ( "tracks" , {} )
	track_intent_dict = tracks_dict.get ( track_index , tracks_dict.get ( str ( track_index ) , {} ) )
	logger.debug ( "track_intent_dict %s" , track_intent_dict )
	try:
		track_intent_weight = int ( track_intent_dict.get ( "weight" ) )
	except ValueError:
		track_intent_weight = 0
	logger.debug ( "track_intent_weight %s" , track_intent_weight )
	track_intentarray = track_intent_dict.get ( "intentarray" , () )
	track_pinnings = track_intent_dict.get ( "pinnings" )
	
	return track_intent_weight , track_intentarray , track_pinnings

@task ()
def solve_gpx ( solution_id ):
	
	with transaction.commit_on_success ():
		try:
			solution = Solution.objects.select_for_update ().get ( id = solution_id )
		except Solution.DoesNotExist , e:
			# solution may not have been committed yet, try again in a few seconds
			raise solve_gpx.retry ( exc = e , countdown = 10 )
		
		if not solution.log_file:
			solution.log_file.save ( "%i_%i_%i.log" % ( solution.trace_activation.trace.id , solution.trace_activation.index_by_trace , solution.index_by_trace_activation ) , ContentFile ( "" ) )
		
		if not solution.planet_revision:
			try:
				solution.planet_revision = PlanetRevision.objects.filter ( active = True ).filter ( sequence_namespace = TSBP_REPLICATION_CURRENT_SEQUENCE_NAMESPACE ).order_by ( "-state_sequence_number" )[0]
			except IndexError:
				# seems we don't have an active planet revision right now
				logger.error ( "Couldn't find an active PlanetRevision" )
				solve_gpx.retry ( countdown = 120 )
		
		solution.state = Solution.STATE_CHOICES_INVERSE["processing"]
		solution.full_clean ()
		solution.save ()
	
	# set up logger
	# grab our own code object (yes, this is horrible)
	try:
		raise Exception
	except:
		this_code_object = exc_info ()[2].tb_frame.f_code
	
	# this is kinda stupid but necessary to open file in a writable mode
	solution.log_file.file.close ()
	solution.log_file.open ( mode = "a" )
	log_file = solution.log_file.file
	
	solution_logger , solution_log_handler = _setup_log_handler ( log_file , (this_code_object,"log_file",log_file) , "gpx" )
	
	try:
		if not solution.planet_revision.active:
			raise PublicError ( "This planet revision is no longer active" )
	
		if not solution.parent_solution:
			# we may have to fetch some data from osm
			from xml.etree.cElementTree import fromstring
			from httplib2 import Http
			httpclient = Http ()
			httpclient.add_credentials ( settings.TSBP_OSM_UNPRIVILEGED_USERNAME , settings.TSBP_OSM_UNPRIVILEGED_PASSWORD )
	
			if not solution.trace_activation.fetched:
				# we're going to have to fetch these trace details
				with transaction.commit_on_success ():
					# first lock the row
					trace_activation = TraceActivation.objects.select_for_update ().get ( pk = solution.trace_activation.pk )
					# check it's still not fetched
					if not trace_activation.fetched:
						now = datetime.now ()
						solution_logger.info ( "Fetching details for trace %i" , trace_activation.trace.id )
						response , content = httpclient.request ( "%sapi/0.6/gpx/%i/details" % ( settings.TSBP_OSM_API_LOCATION , trace_activation.trace.id ) )
						
						if response.status != 200:
							solution_logger.warning ( "Failed to fetch details for trace %i: HTTP %s" , trace_activation.trace.id , response.status )
							exc = PublicError ( "Failed to fetch details for trace %i from OSM: received HTTP %i %s" % ( trace_activation.trace.id , response.status , response.reason ) )
							
							if response.status in (500,):
								solve_gpx.retry ( exc = exc , countdown = 120 )
							
							raise exc
						
						trace_activation.raw_details_xml = content
						
						rootelement = fromstring ( content )
						gpxfileelement = rootelement.find ( "gpx_file" )
						trace_activation.visibility = gpxfileelement.get ( "visibility" )
						trace_activation.name = gpxfileelement.get ( "name" )
						descriptionelement = rootelement.find ( "description" )
						if descriptionelement:
							trace_activation.description = descriptionelement.text
						trace_activation.fetched = now
						
						trace_activation.full_clean ()
						trace_activation.save ()
					
					# make sure we've got an updated version of this
					solution = Solution.objects.get ( id = solution_id )
			
			if not solution.trace_activation.trace.fetched:
				# we're going to have to fetch this trace data
				with transaction.commit_on_success ():
					# first lock the row
					trace = Trace.objects.select_for_update ().get ( pk = solution.trace_activation.trace.pk )
					# check it's still not fetched
					if not trace.fetched:
						now = datetime.now ()
						solution_logger.info ( "Fetching data for trace %i" , trace_activation.trace.id )
						response , content = httpclient.request ( "%sapi/0.6/gpx/%i/data" % ( settings.TSBP_OSM_API_LOCATION , trace_activation.trace.id ) )
						
						if response.status != 200:
							solution_logger.warning ( "Failed to fetch data for trace %i: HTTP %s" , trace.id , response.status )
							exc = PublicError ( "Failed to fetch data for trace %i from OSM: received HTTP %i %s" % ( trace.id , response.status , response.reason ) )
							
							if response.status in (500,):
								solve_gpx.retry ( exc = exc , countdown = 120 )
							
							raise exc
						
						trace.data.save ( "%i.tracedata" % trace.id , ContentFile ( content ) )
						trace.content_type = response["content-type"]
						trace.fetched = now
						
						trace.full_clean ()
						trace.save ()
					
					# make sure we've got an updated version of this
					solution = Solution.objects.get ( id = solution_id )
		
		with transaction.commit_on_success ():
			if not solution.parent_solution:
				# we have to parse the gpx from scratch
				
				solution.trace_activation.trace.data.open ( mode = "rb" )
				gpxfile = solution.trace_activation.trace.data
				if solution.trace_activation.trace.content_type == "application/x-gzip":
					from gzip import GzipFile
					gpxfile = GzipFile ( fileobj = gpxfile , mode = "rb" )
				elif solution.trace_activation.trace.content_type == "application/x-bzip2":
					# using backported bz2file so we can open generic file-like objects
					from bz2file import BZ2File
					gpxfile = BZ2File ( filename = gpxfile , mode = "rb" )
				elif solution.trace_activation.trace.content_type == "application/zip":
					raise PublicError ( "Zip files not (yet) supported" )
				
				# do crude point count
				solution_logger.info ( "Performing crude point count" )
				import re
				trkpt_re = re.compile ( ur"</(?:[%s][%s]*?\:)?trkpt>" % ( _namestartchar , _namechar ) )
				tag_count = 0
				# TODO could process this in chunks if files are too big
				for match in re.finditer ( trkpt_re , gpxfile.read () ):
					tag_count += 1
				solution_logger.info ( "%i points found using crude point count" % tag_count )
				if tag_count > _gpx_max_total_points:
					raise PublicError ( "Trace too big to process. Counted %i trackpoints in trace, limit is %i" % ( tag_count , _gpx_max_total_points ) )
				# reset gpxfile
				gpxfile.seek ( 0 )
				
				solution_logger.info ( "Parsing gpx" )
				gpx = GPX ( gpxfile , transports_by_label[solution.trace_activation.transport_type] )
			
			else:
				# we have to unpickle the gpx and apply any intents modifications to it
				
				from intent import intent_is_ignored , intent_to_weight
				
				gpx = pickle.load ( solution.parent_solution.pickled_gpx )
				
				solution_logger.info ( "Applying any new blacklists" )
				
				for i , track_intent_dict in solution.intents.get ( "tracks" , {} ).items ():
					try:
						i = int ( i ) # in case it came from un-sanitized json
					except ValueError:
						continue
					
					if i < 0 or i >= len ( gpx ):
						continue
					
					intentarray = track_intent_dict.get ( "intentarray" , () )
					
					# in case intentarray is not present or just not long enough we need to do this to graft in the
					# previous blacklist from thereon. lack of intentarray shouldn't just un-blacklist everything.
					from operator import add
					original_blacklist_tail = ()
					if len ( intentarray ) < reduce ( add , imap ( len , gpx[i] ) ):
						from bisect import bisect_left
						original_blacklist_tail = gpx[i].blacklist[bisect_left(gpx[i].blacklist,len(intentarray)):]
					
					# using izip to limit length of iteration to track length
					gpx[i].blacklist = chain ( ( j for j , intent in izip ( xrange ( sum ( ( len ( ts ) for ts in gpx[i] ) ) ) , intentarray ) if intent == 0 or intent_is_ignored ( intent ) ) , original_blacklist_tail )
					
				for i , track in enumerate ( gpx ):
					# doing this partly to make sure any new matrix entries get calculated
					try:
						total_transition = track.routable_track.transitions.total_transition ()
					except UnsolvableRouteError:
						total_transition = "(unsolvable!)"
					solution_logger.info ( "track %s total_transition cost %s after blacklist patchups" , i , total_transition )
					solution_logger.info ( "track %s request_count %s after blacklist patchups" , i , track.router.request_count )
				
				solution_logger.info ( "total_request_count %s after blacklist patchups" , gpx.total_request_count )
				
				im_min = im_max = None
				lowest_intent_negative = False
				
				modified_intentarrays = []
				
				# first pass
				# do an initial scan of our intents to see if/how much we need to offset them for
				# the "lowest" intent weight (provided it is negative) to end up giving a ripeness
				# multiplier of 0
				for i , track in enumerate ( gpx ):
					track_intent_weight , track_intentarray , track_pinnings = _get_track_intent_params ( solution.intents , i )
					
					modified_intentarray = []
					
					if track_intent_weight and track_intent_weight < 3:
						# using izip to limit length of iteration to track length
						for j , intent in izip ( xrange ( sum ( ( len ( ts ) for ts in gpx[i] ) ) ) , track_intentarray ):
							intent_weight = intent_to_weight ( intent )
							
							if intent_weight is not None and intent_weight < 0:
								lowest_intent_negative = True
							
							modified_intent_weight = ( intent_weight + track_intent_weight - 1 ) if intent_weight is not None else None
							
							modified_intentarray.append ( modified_intent_weight )
							
							if modified_intent_weight is not None:
								if im_min is None or modified_intent_weight < im_min:
									im_min = modified_intent_weight
								
								if im_max is None or modified_intent_weight > im_max:
									im_max = modified_intent_weight
					
					# while we're here let's set any pinnings that are requested
					if track_pinnings is not None:
						solution_logger.debug ( "Applying user pinnings to track %i" , i )
						# (also trimming any extra annotation related crap that might be on the end of the pinning tuples)
						track.user_pinnings = ( pinning[:2] for pinning in track_pinnings )
					
					modified_intentarrays.append ( modified_intentarray )
				
				intent_weight_offset = -im_min if lowest_intent_negative else 1
				
				# second pass
				# now let's set those intents as ripeness_multipliers
				for track , modified_intentarray in izip ( gpx , modified_intentarrays ):
					for trackpoint , intent_weight in izip_longest ( chain.from_iterable ( track ) , modified_intentarray ):
						trackpoint.ripeness_multiplier = ( 4**(intent_weight+intent_weight_offset) ) - 1 if intent_weight is not None else 1
						
				solution_logger.debug ( "intent_weight_offset %s" , intent_weight_offset )
			
			for i , track in enumerate ( gpx ):
				track_intent_weight , track_intentarray , track_pinnings = _get_track_intent_params ( solution.intents , i ) if solution.parent_solution else ( None , None , None ,)
				
				solution_exhausted = False
				
				if ( not solution.parent_solution ) or ( track_intent_weight and track_intent_weight < 3 ) or track_pinnings is not None:
					if not solution.parent_solution:
						budget = len ( track.routable_track ) * _budget_multiplier_initial
					else:
						non_zero_rm_routables = reduce ( lambda x,y : x+1 , ( True for tp in track.routable_track if tp.ripeness_multiplier > 0 ) , 0 )
						routable_len = len ( track.routable_track )
						
						solution_logger.debug ( "non_zero_rm_routables %s , routable_len %s" , non_zero_rm_routables , routable_len )
						
						# make budget root-proportional to proportion of routable trackpoints that have nonzero ripeness_multipliers
						budget = int ( ( ( non_zero_rm_routables * routable_len ) ** 0.5 ) * track_intent_weight * _budget_multiplier_following )
					
					solution_logger.info ( "Solving track %s with budget of %s" , i , budget )
					
					try:
						track.routable_track.solve ( budget )
					except UnsolvableRouteError:
						solution_logger.info ( "Caught UnsolvableRouteError" )
						# we can move on as we should also get this when we try and interpret this track, where we can make a note of it
					except ZeroRipenessError:
						solution_logger.info ( "Caught ZeroRipenessError: track seems to be exhausted" )
						solution_exhausted = True
				
					solution_logger.info ( "Interpreting track %s" , i )
					_interpret_solved_track ( track , solution , i , solution_exhausted = solution_exhausted )
				else:
					solution_logger.info ( "Cloning interpretation for track %s" , i )
					# there are probably faster ways of doing this but it's not massively important
					
					# clone SolutionTrack
					solution_track = solution.parent_solution.solutiontrack_set.get ( track_index = i )
					parent_solution_track_pk = solution_track.pk
					solution_track.pk = None
					solution_track.solution = solution
					solution_track.save ()
					
					# clone SolutionTrackPoints
					for solution_track_point in SolutionTrackPoint.objects.filter ( solution_track = parent_solution_track_pk ):
						solution_track_point.pk = None
						solution_track_point.solution_track = solution_track
						solution_track_point.save ()
			
			_pickle_to_solution ( gpx , solution )
			
			if hasattr ( gpx , "metadata" ):
				solution.metadata_json = json.dumps ( gpx.metadata )
			
			solution.state = Solution.STATE_CHOICES_INVERSE["success"]
			solution.full_clean ()
			solution.save ()
	except RetryTaskError , e:
		solution_logger.error ( "Setting solution state before we go back for a retry" )
		# TODO in theory it's possible we've lost our lock on the row. worry about that?
		with transaction.commit_on_success ():
			# grab solution afresh
			solution = Solution.objects.select_for_update ().get ( id = solution_id )
			solution.state = Solution.STATE_CHOICES_INVERSE["retry"]
			
			solution.full_clean ()
			solution.save ()
			
			# make sure logger gets tidied up before we leave
			solution_logger.removeHandler ( solution_log_handler )
			solution_log_handler.flush ()
			del solution_log_handler
			log_file.close ()
		
		# allow exception to propagate
		raise e
	except Exception , e:
		solution_logger.error ( "Caught exception and trying to tidy up:\n%s" % "".join ( format_exception ( *(exc_info()) ) ) )
		# TODO in theory it's possible we've lost our lock on the row. worry about that?
		with transaction.commit_on_success ():
			# grab solution afresh
			solution = Solution.objects.select_for_update ().get ( id = solution_id )
			solution.state = Solution.STATE_CHOICES_INVERSE["error"]
			try:
				solution.reason = e.args[0] if isinstance ( e , PublicError ) else ""
			except IndexError:
				solution.reason = ""
			
			try:
				if gpx:
					try:
						solution_logger.info ( "Attempting to pickle troublesome gpx" )
						_pickle_to_solution ( gpx , solution )
					except:
						# ah forget it
						solution_logger.warning ( "Pickling troublesome gpx failed - giving up" )
			except UnboundLocalError:
				solution_logger.warning ( "No gpx to attempt to pickle" )
			
			solution.full_clean ()
			solution.save ()
			
			# make sure logger gets tidied up before we leave
			solution_logger.removeHandler ( solution_log_handler )
			solution_log_handler.flush ()
			del solution_log_handler
			log_file.close ()
		
		# allow exception to propagate
		raise e
	
	# tidy up logger
	solution_logger.removeHandler ( solution_log_handler )
	solution_log_handler.flush ()
	del solution_log_handler
	log_file.close ()
