
_transports = (
	{
		"label" : "motorcar" ,
		"directional_factor_rolloff_velocity" : 30.0 , # in ms-1
	} ,
	{
		"label" : "bicycle" ,
		"directional_factor_rolloff_velocity" : 10.0 ,
	} ,
)

transports_by_label = dict ( ( ( t["label"] , t ) for t in _transports ) )
